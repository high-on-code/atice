function TrieNode()  {
    this.children = new Array(26).fill(null)
    this.isEndOfWord = false,
    this.count = 0
}
function insertIntoTrie(trie, word) {
    let curr = trie
    for (let i = 0; i < word.length; i++) {
        let ch = word[i]
        if (!curr.children[ch.charCodeAt(0) - 'a'.charCodeAt(0)]) {
            curr.children[ch.charCodeAt(0) - 'a'.charCodeAt(0)] = new TrieNode()
            // Object.setPrototypeOf(curr.children[ch.charCodeAt(0) - 'a'.charCodeAt(0)], TrieNode)
        }
        curr = curr.children[ch.charCodeAt(0) - 'a'.charCodeAt(0)]
        curr.count++
    }
    curr.isEndOfWord = true
}

function findPrefixCount(trie, pref) {
    let curr = trie
    for (let i = 0; i < pref.length; i++) {
        let ch = pref[i]
        if (!curr.children[ch.charCodeAt(0) - 'a'.charCodeAt(0)]) {
            return 0
        }
        curr = curr.children[ch.charCodeAt(0) - 'a'.charCodeAt(0)]
    }
    return curr.count
}
var prefixCount = function(words, pref) {
    let trie = new TrieNode()
    // Object.setPrototypeOf(trie, TrieNode)
    for (aWord of words) {
        insertIntoTrie(trie, aWord)
    }
    return findPrefixCount(trie, pref)
};

const words =["pay","attention","practice","attend"]
const pref = "at"
console.log(prefixCount(words, pref))
