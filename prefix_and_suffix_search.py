from collections import defaultdict

Trie = lambda: defaultdict(Trie)

WEIGHT = "weight"


class WordFilter:
    def __init__(self, words):
        self.trie = Trie()
        for idx2, aw in enumerate(words):
            word_len = len(aw)
            weight = idx2
            for idx in range(0, word_len + 1):
                cur = self.trie
                word_to_insert = aw[idx:] + "#" + aw
                for ch in word_to_insert:
                    cur = cur[ch]
                    cur[WEIGHT] = weight

    def f(self, pre, suff):
        node = self.trie
        search = suff + "#" + pre
        for ch in suff + "#" + pre:
            if ch not in node:
                return -1
            node = node[ch]
        return node[WEIGHT]


words = ["apple"]
presuff = ["a", "e"]
# ["WordFilter","f","f","f","f","f","f","f","f","f","f"]
words = [
    "cabaabaaaa",
    "ccbcababac",
    "bacaabccba",
    "bcbbcbacaa",
    "abcaccbcaa",
    "accabaccaa",
    "cabcbbbcca",
    "ababccabcb",
    "caccbbcbab",
    "bccbacbcba",
]
[
    [],
    ["bccbacbcba", "a"],
    ["ab", "abcaccbcaa"],
    ["a", "aa"],
    ["cabaaba", "abaaaa"],
    ["cacc", "accbbcbab"],
    ["ccbcab", "bac"],
    ["bac", "cba"],
    ["ac", "accabaccaa"],
    ["bcbb", "aa"],
    ["ccbca", "cbcababac"],
]
words = ["abcaccbcaa"]
presuff = ["ab", "abcaccbcaa"]  # should be 4
wf = WordFilter(words)
print(wf.f(*presuff))
