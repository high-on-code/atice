# storing number of elements currently in queue could have drastically reduced the complexity
class MyCircularQueue:
    def __init__(self, size):
        self.qu = [None] * size
        self.head = -1
        self.tail = -1
        self.size = size

    def enQueue(self, val):
        if self.isFull():
            return False
        else:
            if self.head == -1:
                self.head = 0
            self.tail = (self.tail + 1) % self.size
            self.qu[self.tail] = val
            return True

    def deQueue(self):
        if self.isEmpty():
            return False
        else:
            val = self.qu[self.head]
            self.head = (self.head + 1) % self.size
            if (self.head - 1) % self.size == self.tail:
                self.head, self.tail = -1, -1
            # return val
            return True

    def isEmpty(self):
        if self.head == -1:
            return True
        else:
            return False

    def isFull(self):
        if (self.tail + 1) % self.size == self.head:
            return True
        else:
            return False

    def Front(self):
        if self.isEmpty():
            return -1
        return self.qu[self.head]

    def Rear(self):
        if self.isEmpty():
            return -1
        return self.qu[self.tail]


# qu = MyCircularQueue(3)
# print(qu.enQueue(1))
# print(qu.enQueue(2))
# print(qu.enQueue(3))
# print(qu.enQueue(4))
# print(qu.Rear())
# print(qu.isFull())
# print(qu.deQueue())


qu = MyCircularQueue(6)
print(qu.enQueue(6))
print(qu.Rear())
print(qu.Rear())
print(qu.deQueue())
print(qu.enQueue(5))
print(qu.Rear())
print(qu.deQueue())
print(qu.Front())
print(qu.deQueue())
print(qu.deQueue())
print(qu.deQueue())
