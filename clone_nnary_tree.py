def clone_nnary_tree_dfs(root):
    node = Node(root.val)
    children = []
    for ch in children:
        new_ch = clone_nnary_tree_dfs(ch)
        children.append(new_ch)
    node.children = children
    return node


def clone_nnary_tree(root):
    return clone_nnary_tree_dfs(root)
