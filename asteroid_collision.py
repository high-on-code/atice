def find_remaining_collisons(asteroids):
    stack = []
    for val in asteroids:
        store = True
        while stack and stack[-1] > 0 and val < 0:
            if stack[-1] > abs(val):
                store = False
                break
            elif stack[-1] == abs(val):
                store = False
                stack.pop()
                break
            elif stack[-1] < abs(val):
                stack.pop()
        if store:
            stack.append(val)
    return stack

asteroids = [5,10,-5]
asteroids = [8,-8]
asteroids = [10,2,-5]
print(find_remaining_collisons(asteroids))