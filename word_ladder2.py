from collections import defaultdict, deque


def ladder_length(begin_word, end_word, word_list):
    adjacency_list = defaultdict(set)
    word_len = len(begin_word)
    for a_word in word_list:
        for idx in range(word_len):
            adjacency_list[a_word[:idx] + "*" + a_word[idx + 1 :]].add(a_word)
    qu = deque()
    qu.append(begin_word)
    dist = 1
    visited = {begin_word}
    while qu:

        wall_length = len(qu)
        for _ in range(wall_length):
            node = qu.popleft()
            for idx in range(word_len):
                int_rep = node[:idx] + "*" + node[idx + 1 :]
                for adj_node in adjacency_list[int_rep]:
                    if adj_node == end_word:
                        return dist + 1
                    if adj_node not in visited:

                        visited.add(adj_node)
                        qu.append(adj_node)
        dist += 1
    return 0
    print(adjacency_list)


beginWord = "hit"
endWord = "cog"
wordList = ["hot", "dot", "dog", "lot", "log", "cog"]
print(ladder_length(beginWord, endWord, wordList))
