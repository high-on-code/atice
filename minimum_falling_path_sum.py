# https://leetcode.com/problems/minimum-falling-path-sum/
import sys

INFI = sys.maxsize


def minimum_falling_path_sum_helper(matrix, ridx, cidx, cache):
    if cache[ridx][cidx]:
        return cache[ridx][cidx]
    if ridx == len(matrix) - 1:
        return matrix[ridx][cidx]

    res = INFI
    rows = len(matrix)
    cols = len(matrix[0])
    for nrid, ncid in (
        (ridx + 1, cidx - 1),
        (ridx + 1, cidx),
        (ridx + 1, cidx + 1),
    ):
        if 0 <= nrid < rows and 0 <= ncid < cols:
            res = min(
                matrix[ridx][cidx]
                + minimum_falling_path_sum_helper(matrix, nrid, ncid, cache),
                res,
            )
    cache[ridx][cidx] = res
    return cache[ridx][cidx]


def minimum_falling_path_sum(matrix):
    res = INFI
    rows = len(matrix)
    cols = len(matrix[0])
    cache = [[None] * cols for _ in range(rows)]
    for idx, val in enumerate(matrix[0]):
        res = min(minimum_falling_path_sum_helper(matrix, 0, idx, cache), res)
    print(cache)
    return res


matrix = [[2, 1, 3], [6, 5, 4], [7, 8, 9]]
matrix = [[-19, 57], [-40, -5]]
print(minimum_falling_path_sum(matrix))
