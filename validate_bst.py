def validate_bst_recur(root, lt, gt):
    if not root:
        return True
    if lt < root.val < gt:
        if not validate_bst_recur(root.left, lt, root.val):
            return False
        if not validate_bst_recur(root.left, root.val, gt):
            return False
    return True


def validate_bst(root):
    return validate_bst_recur(root, -float("inf"), float("inf"))
