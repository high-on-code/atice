def split_list_in_parts(head, parts):
    list_len = 0
    curr = head
    while curr:
        list_len += 1
        curr = curr.next
one_len = list_len // parts
    extra_node = list_len % parts
    res = []
    curr = head
    for idx in range(parts):
        clen = 0
        if not curr:
            res.append(curr)
            continue
        first = True
        while clen < one_len:
            if first:
                res.append(curr)
                first = False
            curr = curr.next
            clen += 1
        if idx < extra_node:
            if clen == 0:
                res.append(curr)
            else:
                curr = curr.next
        if not curr:
            continue
        next_curr = curr.next
        curr.next = None
        curr = next_curr
    return res
