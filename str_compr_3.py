def get_count_str(count, val):
    res = []
    reps = count // 9
    for _ in range(reps):
        res.append('9' + val)
    res.append(str(count % 9) + val)
    return res
class Solution:
    def compressedString(self, word: str) -> str:
        # edge cases
        # empty string, one string, chars other can alphas
        if len(word) == 1:
            return '1' + word[0]
        currVal = word[0]
        str_len = len(word)
        res = []
        curr_len = 1
        for idx in range(str_len - 2):
            if currVal == word[idx + 1]:
                curr_len += 1
            else:
                # curr_len = curr_len if curr_len <= 9 else 9
                # res.append(str(curr_len))
                # res.append(currVal)
                res += (get_count_str(curr_len, currVal))
                curr_len = 1
                currVal = word[idx + 1]
        if currVal == word[-1]:
            curr_len += 1
            # curr_len = curr_len if curr_len <= 9 else 9
            # res.append(str(curr_len))
            # res.append(currVal)
            res += (get_count_str(curr_len, currVal))
        else:
            # res.append(str(curr_len))
            # res.append(currVal)
            res += (get_count_str(curr_len, currVal))
            res.append('1' + word[-1])
        return ''.join(res)
    
print(Solution().compressedString("abc"))
inp = "aaaaaaaaaaaaaabb"
print(Solution().compressedString(inp))