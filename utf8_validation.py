def validate_utf8(data):
    byte_len = 0
    for byte in data:
        print(bin(byte))
        bin_byte = format(byte, '#010b')[-8:]
        print(bin_byte)
        # print(bin_byte[2:][::-1])
        if byte_len == 0:
            for ch in bin_byte:
                if ch == "1":
                    byte_len += 1
                else:
                    break
            if byte_len == 1 or byte_len > 4:
                return False
            elif byte_len:
                byte_len -= 1
        else:
            if bin_byte[0] == "1" and bin_byte[1] == "0":
                byte_len -= 1
            else:
                return False

    return byte_len == 0


data = [197, 130, 1]
print(validate_utf8(data))
