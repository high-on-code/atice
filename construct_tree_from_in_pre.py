def construct_tree_from_in_pre_helper(preorder, inorder_map, leb, rib):
    if leb > rib:
        return None
    root_val = preorder.pop()
    root = TreeNode(root_val)
    root.left = construct_tree_from_in_pre_helper(
        preorder, inorder_map, leb, inorder_map[root_val] - 1
    )
    root.right = construct_tree_from_in_pre_helper(
        preorder, inorder_map, inorder_map[root_val] + 1, rib
    )
    return root


def construct_tree_from_in_pre(preorder, inorder):
    inorder_map = {val: idx for idx, val in enumerate(inorder)}
    return construct_tree_from_in_pre_helper(preorder[::-1], inorder_map, 0, len(inorder) - 1)
