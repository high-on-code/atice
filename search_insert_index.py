def searchInsert(nums, target):
    low = 0
    hi = len(nums) - 1
    while low <= hi:
        mid = (low + hi) // 2
        if mid == len(nums):
            return mid
        if target == nums[mid]:
            return mid
        elif target < nums[mid]:
            hi = mid - 1
        else:
            low = mid + 1
    return low


nums = [1, 3]
target = 2
nums = [1, 3, 5, 6]
target = 2
target = 7
# nums = [1,3,5,6]
# target =5
print(searchInsert(nums, target))
