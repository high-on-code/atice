def format_sol(nn, curr_sol):
    sol = []
    for idx in curr_sol:
        curr_row = ["."] * nn
        curr_row[idx] = "Q"
        sol.append(curr_row)
    return sol


def nqueens_recur(nn, row, cols, dia, andia, sol, curr_sol):
    if row == nn:
        sol.append(format_sol(nn, curr_sol))
        # sol.append(curr_sol)
    for idx in range(nn):
        if idx in cols:
            continue
        if row - idx in dia:
            continue
        if row + idx in andia:
            continue
        nqueens_recur(
            nn,
            row + 1,
            cols | {idx},
            dia | {row - idx},
            andia | {row + idx},
            sol,
            curr_sol + [idx],
        )
    return sol


def nqueens(nn):
    sol = []
    return nqueens_recur(nn, 0, set(), set(), set(), sol, [])


n = 4
print(nqueens(4))
