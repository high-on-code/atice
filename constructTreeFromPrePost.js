
function TreeNode(val, left, right) {
    this.val = (val === undefined ? 0 : val)
    this.left = (left === undefined ? null : left)
    this.right = (right === undefined ? null : right)
}

/**
 * @param {number[]} preorder
 * @param {number[]} postorder
 * @return {TreeNode}
 */
var constructFromPrePost = function (preorder, postorder) {
    return constructTree(0, preorder.length - 1, 0, preorder, postorder)
};

function constructTree(prestart, preend, poststart, preorder, postorder) {
    if (prestart > preend) return null;
    if (prestart === preend) return new TreeNode(preorder[prestart]);
    let left = preorder[prestart + 1];
    let numNodes = 1
    while (left != postorder[poststart + numNodes - 1]) {
        numNodes += 1
    }
    const root = new TreeNode(preorder[prestart])
    root.left = constructTree(prestart + 1, prestart + numNodes, poststart, preorder, postorder)
    root.right = constructTree(prestart + numNodes + 1,  preend, poststart + numNodes, preorder, postorder)
    return root;
}
const preorder = [1,2,4,5,3,6,7]
const postorder = [4,5,2,6,7,3,1]
console.log(constructFromPrePost(preorder, postorder))