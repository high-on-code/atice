INFI = float("inf")


def find_cost_to_paint_house_recur(
    houses,
    cost,
    prev_color,
    current_house,
    target,
    neigh_count,
    cache,
):
    if current_house == len(houses):
        if neigh_count == target:
            return 0
        else:
            return INFI
    if neigh_count > target:
        return INFI
    if cache[current_house][prev_color][neigh_count] is not None:
        return cache[current_house][prev_color][neigh_count]

    min_cost = INFI
    if houses[current_house]:
        # neigh_count += 0 if prev_color == houses[current_house] else 1
        min_cost = find_cost_to_paint_house_recur(
            houses,
            cost,
            houses[current_house],
            current_house + 1,
            target,
            neigh_count + (0 if prev_color == houses[current_house] else 1),
            cache,
        )

    else:
        for colr, colr_cost in enumerate(cost[current_house]):
            # colr -= 1
            # neigh_count += 0 if colr == prev_color else 1
            paint_cost = find_cost_to_paint_house_recur(
                houses,
                cost,
                colr + 1,
                current_house + 1,
                target,
                neigh_count + (0 if prev_color == colr + 1 else 1),
                cache,
            )
            min_cost = min(min_cost, colr_cost + paint_cost)
    cache[current_house][prev_color][neigh_count] = min_cost
    return min_cost


def find_cost_to_paint_house(houses_ray, cost, houses, paints, target):
    # cache[color][house_idx][neighborhoods]
    cache = []
    for hidx in range(houses):
        hray = []
        cache.append(hray)
        for pidx in range(paints + 1):
            pray = [None] * (target + 1)
            hray.append(pray)
            # pray.append([0] * target)
    # print(cache)
    res = find_cost_to_paint_house_recur(houses_ray, cost, 0, 0, target, 0, cache)
    return -1 if res == INFI else res


"""

houses = [3, 1, 2, 3]
cost = [[1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1]]
m = 4
n = 3
target = 3


houses = [0]
cost = [[3, 1]]
m = 1
n = 2
target = 1

houses = [0, 0, 0, 0, 0]
cost = [[1, 10], [10, 1], [10, 1], [1, 10], [5, 1]]
m = 5
n = 2
target = 3
"""

houses = [0, 2, 1, 2, 0]
cost = [[1, 10], [10, 1], [10, 1], [1, 10], [5, 1]]
m = 5
n = 2
target = 3

print(find_cost_to_paint_house(houses, cost, m, n, target))
