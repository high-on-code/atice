# https://leetcode.com/problems/partition-labels/
def partition_labels(strn):
    last = [0] * 26
    for idx, val in enumerate(strn):
        last[ord(val) - ord("a")] = idx
    anchor = 0
    border = 0
    res = []
    for idx, val in enumerate(strn):
        # (Notice) in this case we are changing the border values at the first step of loop
        border = max(border, last[ord(val) - ord("a")])
        if border == idx:
            res.append(border - anchor + 1)
            anchor = border + 1
    return res


strn = "ababcbacadefegdehijhklij"
print(partition_labels(strn))
