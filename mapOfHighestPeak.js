var highestPeak = function(isWater) {
    // initialize a queue to store coordinates of water
    const queue = [];
    const rows = isWater.length;
    const cols = isWater[0].length;
    // add water coordinates to queue
    const result = new Array(rows);
    for (let row = 0; row < rows; row++) {
        result[row] = new Array(cols).fill(-1);
    }
    for (let row = 0; row < rows; row++) {
        for (let col = 0; col < cols; col++) {
            if (isWater[row][col] === 1) {
                queue.push([row, col]);
                result[row][col] = 0;
            }
        }
    }
    // initialize result grid of size rows x cols

    // console.log('ironman result', JSON.stringify(result));
    // bfs to find highest peak
    const dirs = [[0, 1], [0, -1], [1, 0], [-1, 0]];
    while (queue.length) {
        const [row, col] = queue.shift();
        // result[row][col] = 0;
        // console.log('ironman result', JSON.stringify(result));
        for (const [dr, dc] of dirs) {
            const r = row + dr;
            const c = col + dc;
            if (r >= 0 && r < rows && c >= 0 && c < cols && result[r][c] === -1 && isWater[r][c] === 0) {
                result[r][c] = result[row][col] + 1;
                queue.push([r, c]);
            }
        }
    }
    return result;
};

let isWater = [[0,1],[0,0]];
console.log('ironman highestPeak(isWater)', JSON.stringify(highestPeak(isWater)));