def find_min_cost_for_tickets(days, costs):
    cache = {}

    def dfs(idx):
        if idx >= len(days):
            return 0
        if idx in cache:
            return cache[idx]
        cache[idx] = float('inf')
        for num_days, curr_cost in zip([1, 7, 30], costs):
            idx2 = idx
            while idx2 < len(days) and days[idx2] < days[idx] + num_days:
                idx2 += 1
            cache[idx] = min(cache[idx], curr_cost + dfs(idx2))
        return cache[idx]

    return dfs(0)


days = [1, 4, 6, 7, 8, 20]
costs = [2, 7, 15]
print(find_min_cost_for_tickets(days, costs))