from random import randint


class Codec:
    def __init__(self):
        self.char_set = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        self.cache = {}

    def get_short_url(self):
        while True:
            str_arr = []
            for _ in range(6):
                str_arr.append(self.char_set[randint(0, 61)])
            strn = "".join(str_arr)
            if strn not in self.cache:
                return strn

    def encode(self, long_url):
        short_url = self.get_short_url()
        self.cache[short_url] = long_url
        return short_url

    def decode(self, short_url):
        return self.cache[short_url]
