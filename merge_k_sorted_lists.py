import heapq

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next


def merge_k_sorted_lists(lists):
    num_list = len(lists)
    priq = []
    head = curr = ListNode(0)
    for idx, a_list in enumerate(lists):
        if not a_list:
            continue
        heapq.heappush(priq, (a_list.val, idx))
    print(priq)
    while priq:
        node_val, list_idx = heapq.heappop(priq)
        temp_node = ListNode(node_val)
        curr.next = temp_node
        curr = curr.next
        list_node = lists[list_idx].next
        if list_node:
            heapq.heappush(priq, (list_node.val, list_idx))
            lists[list_idx] = list_node
