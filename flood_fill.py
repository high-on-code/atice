def flood_fill_helper(image, sr, sc, old_color, new_color):
    diffs = ((0, 1), (0, -1), (1, 0), (-1, 0))
    image[sr][sc] = new_color
    for dx, dy in diffs:
        nx, ny = dx + sr, dy + sc
        # print(nx, ny, len(image))
        if 0 <= nx < len(image) and 0 <= ny < len(
                image[0]) and image[nx][ny] == old_color:
            flood_fill_helper(image, nx, ny, old_color, new_color)


def flood_fill(image, sr, sc, new_color):
    if new_color == image[sr][sc]:
        return image
    flood_fill_helper(image, sr, sc, image[sr][sc], new_color)
    return image


image = [[0, 0, 0], [0, 1, 1]]
sr = 1
sc = 1
new_color = 1
print(flood_fill(image, sr, sc, new_color))
