def fast_pow_helper(x, n):
    if not n:
        return 1
    res = fast_pow_helper(x, n // 2)
    res = res * res
    if n % 2:
        res = res * x
    return res


def my_pow(x, n):
    if n < 0:
        x = 1 / x
        n = -n

    return fast_pow_helper(x, n)


print(my_pow(2, -2))
