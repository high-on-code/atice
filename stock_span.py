class StockSpanner:
    def __init__(self):
        self.stack = []

    def next(self, price):
        current = 1
        while self.stack and self.stack[-1][0] <= price:
            current += self.stack.pop()[1]
        self.stack.append((price, current))
        return current
