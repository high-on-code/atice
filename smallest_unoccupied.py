def smallest_chair(times, target_friend):
    ntime = [[idx, time] for idx, time in enumerate(times)]
    ntime.sort(key=lambda a: a[1][0])
    print(ntime)
    chairs = []
    for idx, val in (ntime):
        entry = val[0]
        if not chairs:
            res = 0
            chairs.append(idx)
        else:
            for idx2, person in enumerate(chairs):
                if times[person][1] <= entry:
                    chairs[idx2] = idx
                    res = idx2
                    break
            else:
                chairs.append(idx)
                res = len(chairs) - 1
        if idx == target_friend:
            return res

times = [[1,4],[2,3],[4,6]]
target_friend = 1
# times = [[3,10],[1,5],[2,6]]
# target_friend = 0
print(smallest_chair(times, target_friend))