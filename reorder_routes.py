# https://leetcode.com/problems/reorder-routes-to-make-all-paths-lead-to-the-city-zero
def make_graph(n, connections):
    graph = [[] for _ in range(n)]
    for st, ed in connections:
        graph[st].append((ed, 1))
        graph[ed].append((st, 0))
    return graph


def reorder_recur(par, node, graph, count):
    for cch, st in graph[node]:
        if par == cch:
            continue
        count += st
        count = reorder_recur(node, cch, graph, count)
    return count


def reorder(n, connections):
    graph = make_graph(n, connections)
    # print("iron man graph", graph)
    return reorder_recur(-1, 0, graph, 0)


n = 6
connections = [[0, 1], [1, 3], [2, 3], [4, 0], [4, 5]]
print(reorder(n, connections))
