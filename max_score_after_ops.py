# https://leetcode.com/problems/maximize-score-after-n-operations/
import math

def find_max_score_after_ops_recur(nums, mask, pairs_picked, cache):
    nlen = len(nums)
    if cache[mask] != -1:
        return cache[mask]
    if 2 * pairs_picked == len(nums):
        return 0
    max_score = 0
    for fi in range(nlen):
        if (mask >> fi) & 1:
            continue
        for si in range(fi + 1, nlen):
            if (mask >> si) & 1:
                continue
            new_mask = mask | (1 << si) | (1 << fi)
            curr_score = (pairs_picked + 1) * math.gcd(nums[fi], nums[si])
            rem_score = find_max_score_after_ops_recur(nums, new_mask, pairs_picked + 1, cache)
            max_score = max(max_score, curr_score + rem_score)
    cache[mask] = max_score
    return cache[mask]
def find_max_score_after_ops(nums):
    nlen = len(nums)
    cache_size = 1 << nlen
    cache = [-1] * cache_size
    return find_max_score_after_ops_recur(nums, 0, 0, cache)

nums = [1,2]
print(find_max_score_after_ops(nums))