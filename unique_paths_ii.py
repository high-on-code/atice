# https://leetcode.com/problems/unique-paths-ii
def unique_paths(matrix):
    if matrix[0][0] == 1:
        return 0
    rc = len(matrix)
    cc = len(matrix[0])
    cache = [[0] * cc for _ in range(rc)]
    cache[0][0] = 1
    for cr in range(rc):
        for cuc in range(cc):
            if matrix[cr][cuc]:
                continue
            if cr > 0:
                cache[cr][cuc] += cache[cr - 1][cuc]
            if cuc > 0:
                cache[cr][cuc] += cache[cr][cuc - 1]
    return cache[-1][-1]


obstacleGrid = [[0, 0, 0], [0, 1, 0], [0, 0, 0]]
print(unique_paths(obstacleGrid))
m = 3
n = 7
# print(unique_paths(m, n))
