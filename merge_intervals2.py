def merge(intervals):
    intervals.sort()
    ran1 = intervals[0]
    idx = 1
    op_list = []
    while idx < len(intervals):
        if ran1[1] >= intervals[idx][0]:
            ran1[1] = max(intervals[idx][1], ran1[1])
        else:
            op_list.append(ran1)
            ran1 = intervals[idx]
        idx += 1
    op_list.append(ran1)
    return op_list


intervals = [[1, 3], [2, 6], [8, 10], [15, 18]]
intervals = [[1, 4], [4, 5]]
intervals = [[1, 4], [2, 3]]
print(merge(intervals))
