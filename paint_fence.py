def find_ways_to_paint_fence(posts, colors):
    pp_values = colors
    if posts == 1:
        return pp_values
    pvalues = colors ** 2
    if posts == 2:
        return pvalues
    for idx in range(3, posts + 1):
        val = (colors - 1) * (pvalues + pp_values)
        pp_values = pvalues
        pvalues = val
    return val


n = 3
k = 2
print(find_ways_to_paint_fence(n, k))
