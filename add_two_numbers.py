def add_two_numbers(l1, l2):
    carry = 0
    res = ListNode()
    curr = res
    while l1 or l2:
        curr.next = ListNode()
        curr = curr.next
        if l1:
            num1 = l1.val
            l1 = l1.next
        if l2:
            num2 = l2.val
            l2 = l2.next
        node_val = num1 + num2 + carry
        num1, num2 = 0, 0
        carry = node_val // 10
        node_val %= 10
        curr.val = node_val
    if carry:
        curr.next = ListNode()
        curr = curr.next
        curr.val = carry
    return res.next
