var waysToSplitArray = function(nums) {
    const numlen = nums.length;
    const prefixSum = new Array(numlen)
    prefixSum[0] = nums[0]
    for (let idx = 1; idx < numlen; idx++) {
        prefixSum[idx] = prefixSum[idx - 1] + nums[idx]
    }
    const totSum = prefixSum.at(-1)
    let res = 0
    for (let idx = 0; idx < numlen - 1; idx++) {
        res += prefixSum[idx] >= totSum - prefixSum[idx]? 1: 0
    }
    return res
};
const nums = [10,4,-8,7];
console.log(waysToSplitArray(nums))