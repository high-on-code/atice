def unique_paths(rc, cc):
    cache = [[0] * cc for _ in range(rc)]
    cache[0][0] = 1
    for cr in range(rc):
        for cuc in range(cc):
            if cr > 0:
                cache[cr][cuc] += cache[cr - 1][cuc]
            if cuc > 0:
                cache[cr][cuc] += cache[cr][cuc - 1]
    return cache[-1][-1]


print(unique_paths(2, 2))
m = 3
n = 7
print(unique_paths(m, n))
