def find_longest_susbtr_without_repeating_chars(strn):
    left, right = 0, 0
    chars_array = [None] * 128
    # strn_len = len(strn)
    max_len = 0
    for right, val in enumerate(strn):
        encode_val = ord(val) - ord("0")
        if chars_array[encode_val] != None:
            left = max(chars_array[encode_val] + 1, left)
            # chars_array[encode_val] = None
        chars_array[encode_val] = right
        max_len = max(max_len, right - left + 1)
    return max_len


s = "abcabcbb"
s = "abba"
print(find_longest_susbtr_without_repeating_chars(s))
