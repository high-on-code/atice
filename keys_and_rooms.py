def find_rooms_helper(rooms, croom, visited, all_keys):
    visited.add(croom)
    all_keys = all_keys.union(set(rooms[croom]))
    if len(all_keys) == len(rooms):
        return True, all_keys
    for a_key in rooms[croom]:
        if a_key not in visited:
            res, all_keys = find_rooms_helper(rooms, a_key, visited, all_keys)
            if res:
                return True, all_keys
    return False, all_keys


def find_rooms(rooms):
    return find_rooms_helper(rooms, 0, set(), {0})[0]


rooms = [[1], [2], [3], []]
rooms = [[1, 3], [3, 0, 1], [2], [0]]
# rooms = [[2, 3], [], [2], [1, 3]]
# rooms = [[13], [15, 29, 22], [5, 18, 9], [7], [27], [27], [6, 28], [26], [34],
#          [1, 44, 11], [8, 36], [17, 35], [11, 45,
#                                           46, 10, 49], [19, 38, 47, 39],
#          [20, 30], [34], [32, 31], [25, 19, 21, 29], [36], [], [38],
#          [2, 13, 17, 47], [12], [49, 46], [], [40], [], [39, 16, 24], [24, 41],
#          [14, 3, 40], [14, 43], [], [3, 20, 23], [37, 48], [6, 10], [26, 1, 4],
#          [], [41, 45], [23, 33], [], [22, 18, 37], [4, 33, 43], [28, 31, 42],
#          [30, 48], [16, 35], [5, 8, 44], [2, 25], [9, 21, 42], [7, 12, 32], []]
print(find_rooms(rooms))
