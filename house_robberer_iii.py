def helper(root, parent_robbed, rob_cache, not_rob_cache):
    if not root:
        return 0
    if parent_robbed:
        if root in rob_cache:
            return rob_cache[root]
        rob_cache[root] = helper(root.left, False, rob_cache, not_rob_cache) + helper(
            root.right, False, rob_cache, not_rob_cache
        )
        return rob_cache[root]
    else:
        if root in not_rob_cache:
            return not_rob_cache[root]
        rob = (
            root.val
            + helper(root.left, True, rob_cache, not_rob_cache)
            + helper(root.right, True, rob_cache, not_rob_cache)
        )
        not_rob = helper(root.left, False, rob_cache, not_rob_cache) + helper(
            root.right, False, rob_cache, not_rob_cache
        )
        not_rob_cache[root] = max(rob, not_rob)
        return not_rob_cache[root]


def find_max_amount(root):
    return helper(root, False, {}, {})
