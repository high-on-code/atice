# https://leetcode.com/problems/max-dot-product-of-two-subsequences/
from sys import maxsize
def find_max_dot_of_subseq_recur(nums1, nums2, idx1, idx2, cache):
    if idx1 == len(nums1) or idx2 == len(nums2):
        return 0
    if cache[idx1][idx2] is not None:
        return cache[idx1][idx2]
    res = nums1[idx1] * nums2[idx2] + find_max_dot_of_subseq_recur(nums1, nums2, idx1 + 1, idx2 + 1, cache)
    cache[idx1][idx2] = max(res,
        find_max_dot_of_subseq_recur(nums1, nums2, idx1 + 1, idx2, cache),
        find_max_dot_of_subseq_recur(nums1, nums2, idx1, idx2 + 1, cache),
    )
    return cache[idx1][idx2]
    
def find_max_dot_of_subseq(nums1, nums2):
    if max(nums1) < 0 and min(nums2) > 0:
        return max(nums1) * min(nums2)
    elif min(nums1) > 0 and max(nums2) < 0:
        return min(nums1) * max(nums2)
    cache = [[None] * len(nums2) for _ in range(len(nums1))]
    return find_max_dot_of_subseq_recur(nums1, nums2, 0, 0, cache)

nums1 = [2,1,-2,5]
nums2 = [3,0,-6]
nums1 = [-1, -1]
nums2 = [1, 1]
print(find_max_dot_of_subseq(nums1, nums2))