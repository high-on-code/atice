from collections import deque
class Solution:
    def postorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        stack = []
        res = deque()
        # stack.push(root)
        while stack or root:
            if root:
                stack.append(root)
                res.appendleft(root.val)
                root = root.right
            else:
                node = stack.pop()
                root = node.left
        return res
                
