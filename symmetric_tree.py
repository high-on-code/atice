# def find_symmetric_tree_helper(node1, node2):
#     if not any((node1, node2)):
#         return True
#     if not node1 or not node2 or node1.val != node2.val:
#         return False
#     res = find_symmetric_tree_helper(node1.left, node2.right)
#     if not res:
#         return res
#     return find_symmetric_tree_helper(node2.left, node1.right)


# beautiful code
# (Notice) above function is also correct
def find_symmetric_tree_helper(node1, node2):
    if not node1 and not node2:
        return True
    if not node1 or not node2:
        return False
    return (
        node1.val == node2.val
        and find_symmetric_tree_helper(node1.left, node2.right)
        and find_symmetric_tree_helper(node2.left, node1.right)
    )


def find_symmetric_tree(root):
    return find_symmetric_tree_helper(root.left, root.right)
