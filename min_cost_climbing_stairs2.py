INFI = float("inf")


def find_min_cost_climibing_stairs_recur(cost, idx, cache):
    if idx == len(cost) - 1:
        return cost[-1]
    elif idx == len(cost) - 2:
        return cost[-2]
    if cache[idx]:
        return cache[idx]
    cache[idx] = cost[idx] + min(
        find_min_cost_climibing_stairs_recur(cost, idx + 1, cache),
        find_min_cost_climibing_stairs_recur(cost, idx + 2, cache),
    )
    return cache[idx]


def find_min_cost_climibing_stairs(cost):
    cache = [False] * len(cost)
    cache[-1] = cost[-1]
    cache[-2] = cost[-2]
    find_min_cost_climibing_stairs_recur(cost, 0, cache)
    return min(cache[0], cache[1])


cost = [10, 15, 20]
# cost = [1, 100, 1, 1, 1, 100, 1, 1, 100, 1]
print(find_min_cost_climibing_stairs(cost))
