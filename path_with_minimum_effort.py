import heapq

INF = float("inf")


def find_path_with_min_effort(heights):
    rows = len(heights)
    cols = len(heights[0])
    min_efforts = [[INF] * cols for _ in range(rows)]
    visited = [[False] * cols for _ in range(rows)]

    # print(min_efforts)
    min_efforts[0][0] = 0
    pq = [(0, 0, 0)]
    while pq:
        effort, r, c = heapq.heappop(pq)
        visited[r][c] = True
        for dr, dc in ((0, 1), (1, 0), (0, -1), (-1, 0)):
            adj_r, adj_c = r + dr, c + dc
            if 0 <= adj_r < rows and 0 <= adj_c < cols:
                curr_diff = abs(heights[adj_r][adj_c] - heights[r][c])
                max_diff = max(curr_diff, min_efforts[r][c])
                # max_diff = min(curr_diff, min_efforts[r][c])

                if min_efforts[adj_r][adj_c] > max_diff:
                    min_efforts[adj_r][adj_c] = max_diff
                    heapq.heappush(pq, (max_diff, adj_r, adj_c))
    return min_efforts[-1][-1]


heights = [[1, 2, 2], [3, 8, 2], [5, 3, 5]]
print(find_path_with_min_effort(heights))
