import sys
print ("********* Program1 - DayOfTheWeek ********")
user_input = 3
# DayofTheWeek = int(input("Enter Day Of the Week (1-7) : "))
DayofTheWeek = user_input
# if not 1 <= user_input <= 7:
#     print("Not valid input")
#     sys.exit()
if DayofTheWeek == 1:
  print("Monday")
elif DayofTheWeek == 2:
  print("Tuesday")
elif DayofTheWeek == 3:
  print("Wednesday")
elif DayofTheWeek == 4:
  print("Thursday")
elif DayofTheWeek == 5:
  print("Friday")
elif DayofTheWeek == 6:
  print("Saturday")
elif DayofTheWeek == 7:
  print("Sunday")
else:
  print("Error : Number that is outside the range of 1 through 7")
print ("********* Program2 - AgeClassifier ********")  
# AgeClassifier = int(input("Enter Person''s Age : "))
user_input2 = 22
AgeClassifier = user_input2
if AgeClassifier <= 1 and AgeClassifier > 0:
  print("he or she is an infant")
elif AgeClassifier > 1 and AgeClassifier < 13:
  print("he or she is a child")
elif AgeClassifier >= 13 and AgeClassifier < 20:
  print("he or she is a teenager")
elif AgeClassifier >= 20:
  print("he or she is a adult")
else:
  print("Error : Invalid Age entered ", end ="")
  print (AgeClassifier)  
print ("********* Program3 - MassWieght ********")
user_input3 = 100  
# ObjectMass = int(input("Enter Object's Mass : "))
ObjectMass = user_input3
Weight = ObjectMass *9.8
if Weight > 500 :
  print("Object weight is too heavy")
elif Weight < 100 :
  print("Object weight is too light")
else :
  print("Object weight is in Range")