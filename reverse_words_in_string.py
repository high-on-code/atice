def reverse_a_word(boche):
    for idx in range(len(boche) // 2):
        boche[idx], boche[len(boche) - 1 - idx] = boche[len(boche) - 1 - idx], boche[idx]
    return ''.join(boche)
    
    
def reverse_words(strn):
    boche = []
    res = []
    for ch in strn:
        if ch == " ":
            rev_word = reverse_a_word(boche)
            res.append(rev_word)
            boche = []
        else:
            boche.append(ch)
    rev_word = reverse_a_word(boche)
    res.append(rev_word)
    return ' '.join(res)

s = "Let's take LeetCode contest"
print(reverse_words(s))