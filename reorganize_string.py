from collections import Counter
import heapq


def reorg_strn(strn):
    ch_ct = Counter(strn)
    pq = []
    for ch, val in ch_ct.items():
        heapq.heappush(pq, (-val, ch))
    # print(pq)

    def add_ch_to_pq(cct, cch):
        if cct == 0:
            return
        heapq.heappush(pq, (cct, cch))
    res = []
    strnlen = len(strn)
    for _ in range(strnlen):
        cct, cch = heapq.heappop(pq)
        if res:
            if cch == res[-1]:
                if not pq:
                    return ""
                sct, sch = heapq.heappop(pq)
                res.append(sch)
                add_ch_to_pq(sct + 1, sch)
                add_ch_to_pq(cct, cch)
            else:
                res.append(cch)
                add_ch_to_pq(cct + 1, cch)
        if not res:
            res.append(cch)
            heapq.heappush(pq, (cct + 1, cch))
    return ''.join(res)


s = "aab"
s = "aaab"
# s = "aaabb"
print(reorg_strn(s))
