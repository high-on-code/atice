def is_mon_decreasing(arr):
    for idx in range(1, len(arr)):
        if arr[idx] - arr[idx - 1] < 0:
            return False
    return True

def is_mon_increasing(arr):
    for idx in range(1, len(arr)):
        if arr[idx] - arr[idx - 1] > 0:
            return False
    return True
def find_if_mon(arr):
    if len(arr) == 1 or len(arr) == 2:
        return True
    for idx in range(0, len(arr) - 1):
        if arr[idx] - arr[idx + 1] < 0:
            return is_mon_decreasing(arr)
        elif arr[idx] - arr[idx + 1] > 0:
            return is_mon_increasing(arr)
    return True

nums = [1,2,2,3]
nums = [6,5,4,4]
nums = [1, 1, 1]
# nums = [1,3,2]
print(find_if_mon(nums))