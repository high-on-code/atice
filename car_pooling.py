import heapq


def get_new_capacity(passengers, start, capacity):
    if not passengers:
        return capacity
    while passengers and passengers[0][0] <= start:
        pass_got_down = heapq.heappop(passengers)
        capacity += pass_got_down[1]
    return capacity


def car_pooling(trips, capacity):
    passengers = []
    trips.sort(key=lambda x: x[1])
    for passen, start, end in trips:
        capacity = get_new_capacity(passengers, start, capacity)
        if capacity >= passen:
            capacity -= passen
            heapq.heappush(passengers, (end, passen))
        else:
            return False
    else:
        return True


trips = [[2, 1, 5], [3, 3, 7]]
# print(car_pooling([[2, 1, 5], [3, 3, 7]], 4))
# print(car_pooling([[2, 1, 5], [3, 3, 7]], 5))
print(car_pooling([[2, 1, 5], [3, 5, 7]], 3))
