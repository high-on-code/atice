def decode_string(strn):
    char_stack = []
    number_stack = []
    curr_num = 0
    curr_str = ""
    for ch in strn:
        if ch.isnumeric():
            curr_num = (curr_num * 10) + int(ch)
        elif ch.isalpha():
            curr_str += ch
        elif ch == "[":

            char_stack.append(curr_str)
            number_stack.append(curr_num)
            curr_num = 0
            curr_str = ""
        else:
            curr_num = number_stack.pop()
            dec_str = char_stack.pop()
            dec_str = dec_str + (curr_num * (curr_str))
            curr_str = dec_str
            curr_num = 0
    return curr_str


strn = "3[a]2[bc]"
strn = "3[a2[c]]"
print(decode_string(strn))
