def find_min_replacements_to_sort_array(nums):
    if len(nums) == 1:
        return 0
    min_replacements = 0
    for idx in range(len(nums) - 2, -1, -1):
        if nums[idx] > nums[idx + 1]:
            if nums[idx] % nums[idx + 1] == 0:
                num_replaced = nums[idx] // nums[idx + 1]
            else:
                num_replaced = (nums[idx] // nums[idx + 1]) + 1
            nums[idx] = nums[idx] // num_replaced
            min_replacements += num_replaced - 1
    return min_replacements

nums = [3,9,3]
nums = [1,2,3,4,5]
print(find_min_replacements_to_sort_array(nums))