from collections import deque
def find_min_depth(root):
    if root is None:
        return 0
    qu = deque()
    qu.append(root)
    depth = 0
    while qu:
        depth += 1
        wallen = len(qu)
        for i in range(wallen):
            cnode = qu.popleft()
            if cnode.left:
                qu.append(cnode.left)
            if cnode.right:
                qu.append(cnode.right)
            if cnode.left is None and cnode.right is None:
                return depth