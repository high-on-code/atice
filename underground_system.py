from collections import defaultdict


class UndergroundSystem:
    def __init__(self):
        self.checkin = {}
        self.avg_times = defaultdict(lambda: [0, 0])

    def checkIn(self, idx, station_name, time):
        self.checkin[idx] = (station_name, time)

    def checkOut(self, idx, station_name, time):
        checkin_stn, checkin_time = self.checkin[idx]
        self.avg_times[(checkin_stn, station_name)][0] += time - checkin_time
        self.avg_times[(checkin_stn, station_name)][1] += 1

    def getAverageTime(self, start_stn, end_stn):
        return (
            self.avg_times[(start_stn, end_stn)][0]
            / self.avg_times[(start_stn, end_stn)][1]
        )
