def find_longest_pal_subseq_recur(st, i, j, memo):
    if i > j:
        return 0
    if i == j:
        return 1
    if memo[i][j] != 0:
        return memo[i][j]
    if st[i] == st[j]:
        memo[i][j] = find_longest_pal_subseq_recur(st, i + 1, j - 1, memo) + 2
    else:
        memo[i][j] = max(find_longest_pal_subseq_recur(st, i + 1, j, memo), find_longest_pal_subseq_recur(st, i, j - 1, memo))
    return memo[i][j]
def find_longest_pal_subseq(st):
    stlen = len(st)
    # 2d array to hold all the recursion results
    memo = [[0 for _ in range(stlen)] for _ in range(stlen)]
    return find_longest_pal_subseq_recur(st, 0, stlen - 1, memo)