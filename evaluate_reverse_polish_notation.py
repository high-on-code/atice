def add(num1, num2):
    return num1 + num2


def diff(num1, num2):
    return num1 - num2


def prod(num1, num2):
    return num1 * num2


def div(num1, num2):
    return int(num1 / num2)


def evaluate_reverse_polish_notation(expr):
    op_dict = {"+": add, "-": diff, "*": prod, '/': div}
    stack = []
    for ch in expr:
        if ch in op_dict:
            num2 = stack.pop()
            num1 = stack.pop()
            stack.append(op_dict[ch](num1, num2))
        else:
            stack.append(int(ch))
    return stack[-1]


tokens = ["2", "1", "+", "3", "*"]
tokens = ["10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"]
print(evaluate_reverse_polish_notation(tokens))
