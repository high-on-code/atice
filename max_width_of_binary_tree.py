from collections import deque


def find_max_width_of_binary_tree(root):
    if not root:
        return 0
    node, col = root, 0
    qu = deque()
    qu.append((node, col))
    max_wall_len = 0
    while qu:
        wallen = len(qu)
        level_beg_idx = qu[0][1]
        for _ in range(wallen):
            curr_node, col = qu.popleft()
            if curr_node.left:
                col_val = 2 * col
                qu.append((curr_node.left, col_val))
            if curr_node.right:
                col_val = 2 * col + 1
                qu.append((curr_node.right, col_val))
        max_wall_len = max(max_wall_len, col - level_beg_idx + 1)
    return max_wall_len
