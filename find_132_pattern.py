def find132pattern(nums):
    num_len = len(nums)
    min_arr = [None] * num_len
    min_arr[0] = nums[0]
    for idx in range(1, num_len):
        min_arr[idx] = min(nums[idx], min_arr[idx - 1])
    stack = []
    for idx in range(num_len - 1, -1, -1):
        val = nums[idx]
        if val < min_arr[idx]:
            continue
        while stack and stack[-1] <= min_arr[idx]:
            stack.pop()
        if stack and stack[-1] < val:
            return True
        stack.append(val)
    return False


nums = [1, 3, 2]
# nums = [2, 1, 3]
nums = [3, 1, 4, 2]
print(find132pattern(nums))
