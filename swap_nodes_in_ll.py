# https://leetcode.com/problems/swapping-nodes-in-a-linked-list/
def swap_nodes(head, siz):
    curr = head
    fNode = None
    sNode = None
    listSize = 0
    while curr:
        listSize += 1
        if sNode:
            sNode = sNode.next
        if listSize == siz:
            fNode = curr
            sNode = head
        curr = curr.next
    sNode.val, fNode.val = fNode.val, sNode.val
    return head
