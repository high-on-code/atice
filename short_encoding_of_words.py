from collections import defaultdict

Trie = lambda: defaultdict(Trie)
COUNT = "count"


def find_short_word_length_encoding(words):
    nodes = {}
    trie = Trie()
    for idx, aword in enumerate(words):
        curr = trie
        for ch in aword[::-1]:
            curr[COUNT] = curr.get(COUNT, 0) + 1
            curr = curr[ch]
        nodes[aword] = curr
    res = 0
    for word, node in nodes.items():
        # print(node[COUNT])
        if node.get(COUNT, 0) == 0:
            res += len(word) + 1
    return res


# words = ["time", "me", "bell"]
words = ["time"] * 5

print(find_short_word_length_encoding(words))
