def is_palindrome(strn, low, high):
    while low < high:
        if strn[low] != strn[high]:
            return False
        low += 1
        high -= 1
    else:
        return True


def dfs(start, result, current_list, strn):
    if start >= len(strn):
        result.append(current_list[:])
    for end in range(start, len(strn)):
        if is_palindrome(strn, start, end):
            current_list.append(strn[start : end + 1])
            dfs(end + 1, result, current_list, strn)
            current_list.pop()


def partition(strn):
    result = []
    dfs(0, result, [], strn)
    return result


print(partition("aab"))
