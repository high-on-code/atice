def reverseWords(s):
    res = [' '] * len(s)
    lsi = -1
    nsi = None
    for idx, ch in enumerate(s):
        if ch == " ":
            nsi = idx
            new_func(s, res, lsi, nsi)
            lsi = nsi
    nsi = len(s)
    adder = 1
    while lsi + adder <= nsi - adder:
        res[lsi + adder], res[nsi - adder] = s[nsi - adder], s[lsi + adder]
        adder += 1
    
    # print(res)
    return "".join(res)

def new_func(s, res, lsi, nsi):
    adder = 1
    while lsi + adder <= nsi - adder:
        res[lsi + adder], res[nsi - adder] = s[nsi - adder], s[lsi + adder]
        adder += 1


strn = "Let's take LeetCode contest"
print(reverseWords(strn))
