from collections import deque

# bfs starting for current node


def bfs_from_node(idx, idy, grid, visited):
    # create a queue
    qu = deque()
    # add the current node to the queue
    qu.append((idx, idy))
    # add the current node to the visited set
    visited.add((idx, idy))
    # variable to store graph size
    graph_size = 1
    # loop to iterate over the queue
    while qu:
        cnode = qu.popleft()

        for dx, dy in ((0, 1), (1, 0), (0, -1), (-1, 0)):
            # check we are not crossing grid boundaries
            if not(0 <= cnode[0] + dx < len(grid) and 0 <= cnode[1] + dy < len(grid[0])):
                continue
            # if the current node is not visited
            if (cnode[0] + dx, cnode[1] + dy) not in visited and grid[cnode[0] + dx][cnode[1] + dy] == 1:
                # add the node to the queue
                qu.append((cnode[0] + dx, cnode[1] + dy))
                visited.add((cnode[0] + dx, cnode[1] + dy))
                # add the node to the visited set


def find_number_of_enclaves(grid):

    # create set to store visited nodes
    visited = set()
    res = 0
    # iterate over first row
    for idx in range(len(grid[0])):
        if grid[0][idx] == 1:
            bfs_from_node(0, idx, grid, visited)
    # iterate over last row
    for idx in range(len(grid[0])):
        if grid[-1][idx] == 1:
            bfs_from_node(len(grid) - 1, idx, grid, visited)
    # iterate over first column
    for idx in range(len(grid)):
        if grid[idx][0] == 1:
            bfs_from_node(idx, 0, grid, visited)
    # iterate over last column
    for idx in range(len(grid)):
        if grid[idx][-1] == 1:
            bfs_from_node(idx, len(grid[0]) - 1, grid, visited)
    # iterate over all the nodes
    for idx, row in enumerate(grid):
        for idy, val in enumerate(row):
            if val == 1 and (idx, idy) not in visited:

                res += 1
    return res


grid = [[0, 0, 0, 0], [1, 0, 1, 0], [0, 1, 1, 0], [0, 0, 0, 0]]
print(find_number_of_enclaves(grid))
