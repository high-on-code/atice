from collections import defaultdict

PRODUCT = "prod"


def pre_order(trie, res):
    if len(res) == 3:
        return
    if trie.get(PRODUCT, False):
        res.append(trie[PRODUCT])
    for child in sorted(trie.keys()):
        if child != PRODUCT:
            pre_order(trie[child], res)


def get_suggestions(pre, trie):
    for ch in pre:
        trie = trie[ch]
    res = []
    pre_order(trie, res)
    return res


def suggest_products(products, search_word):
    Trie = lambda: defaultdict(Trie)
    trie = Trie()
    for aprod in products:
        curr = trie
        for ch in aprod:
            curr = curr[ch]
        curr[PRODUCT] = aprod

    res = []
    pre = []
    for ch in search_word:
        pre.append(ch)
        res.append(get_suggestions(pre, trie))
    return res


products = ["mobile", "mouse", "moneypot", "monitor", "mousepad"]
searchWord = "mouse"
print(suggest_products(products, searchWord))
