# https://leetcode.com/problems/check-if-it-is-a-straight-line
def check_if_startight_line(cords):
    if len(cords) <= 2:
        return True
    for idx in range(1, len(cords) - 1):
        if (cords[0][0] - cords[idx][0]) * (cords[0][1] - cords[idx + 1][1]) != (cords[0][0] - cords[idx + 1][0]) * (cords[0][1] - cords[idx][1]):
            return False
    return True

coordinates = [[1,2],[2,3],[3,4],[4,5],[5,6],[6,7]]
print(check_if_startight_line(coordinates))