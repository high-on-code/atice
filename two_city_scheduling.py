def find_two_city_scheduling_cost(costs):
    res = 0
    costs.sort(key=lambda x: x[0] - x[1])
    half = len(costs) // 2
    for ac, bc in costs[:half]:
        res += ac
    for ac, bc in costs[half:]:
        res += bc
    return res


costs = [[10, 20], [30, 200], [400, 50], [30, 20]]
print(find_two_city_scheduling_cost(costs))
