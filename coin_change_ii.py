def find_coin_combinations(amount, coins):
    cache = [0] * (amount + 1)
    cache[0] = 1
    for a_coin in coins:
        for an_amount in range(1, amount + 1):
            if an_amount - a_coin >= 0:
                cache[an_amount] += cache[an_amount - a_coin]
    return cache[-1]


amount = 5
coins = [1, 2, 5]
print(find_coin_combinations(amount, coins))
