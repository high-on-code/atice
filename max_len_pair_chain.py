def find_max_len_pair_chain(pairs):
    pairs.sort(key=lambda x: x[1])
    print(pairs)
    res = []
    res.append(pairs[0])
    for idx in range(1, len(pairs)):
        if res[-1][1] < pairs[idx][0]:
            res.append(pairs[idx])
    return len(res)
pairs = [[1,2],[2,3],[3,4]]
# pairs = [[1,2],[7,8],[4,5]]
# pairs = [[-6,9],[1,6],[8,10],[-1,4],[-6,-2],[-9,8],[-5,3],[0,3]]
print(find_max_len_pair_chain(pairs))