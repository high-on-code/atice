def simplify_path(path):
    stack = []
    path_parts = path.split("/")
    for apart in path_parts:
        if apart == "..":
            if stack:
                stack.pop()
        elif apart == "" or apart == ".":
            continue
        else:
            stack.append(apart)
    return "/" + "/".join(stack)


path = "/home/"
path = "/../"
path = "/home//foo/"
print(simplify_path(path))
