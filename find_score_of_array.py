# import heapq
import heapq
def get_prev_idx(idx):
    if idx != 0:
        return idx - 1
def get_next_idx(idx, nlen):
    if idx != nlen - 1:
        return idx + 1
class Solution:
    def findScore(self, nums) -> int:
        num_len = len(nums)
        mark_list = [0] * num_len
        nums = [(val, idx) for idx, val in enumerate(nums)]
        print(nums)
        heapq.heapify(nums)
        res = 0
        while nums:
            val, idx = heapq.heappop(nums)
            # skip marked indexes
            if mark_list[idx]:
                continue
            res += val
            prev_idx = get_prev_idx(idx)
            if prev_idx is not None:
                mark_list[prev_idx] = True
            next_idx = get_next_idx(idx, num_len)
            if next_idx is not None:
                mark_list[next_idx] = True
        return res
    
print(Solution().findScore([2,1,3,4,5,2]))