def find_max_num_vowels(st, siz):
    subst = st[:siz]
    stlen = len(st)
    print('stlen', stlen)
    vowels = {'a', 'e', 'i', 'o', 'u'}
    st_sz = len(st)
    vow_ct = 0
    for ch in subst:
        if ch in vowels:
            vow_ct += 1
    max_vow_ct = vow_ct
    for idx in range(st_sz - siz):
        if st[idx] in vowels:
            vow_ct -= 1
        if st[idx + siz] in vowels:
            vow_ct += 1
        max_vow_ct = max(max_vow_ct, vow_ct)
    return max_vow_ct

s = "abciiidef"
k = 3
s = "ibpbhixfiouhdljnjfflpapptrxgcomvnb"
k = 33
print(find_max_num_vowels(s, k))