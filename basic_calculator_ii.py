def get_number(strn, start_idx):
    number = 0
    strn_len = len(strn)
    while start_idx < strn_len and strn[start_idx].isnumeric():
        number = (number * 10) + int(strn[start_idx])
        start_idx += 1
    return number, start_idx


def skip_spaces(strn, start_idx):
    strn_len = len(strn)
    while start_idx < strn_len and strn[start_idx] == " ":
        start_idx += 1
    return start_idx


def prod(num1, num2):
    return num1 * num2


def div(num1, num2):
    return num1 // num2


def add(num1, num2):
    return num1 + num2


def sub(num1, num2):
    return num1 - num2


OPERATE = {"+": add, "-": sub, "*": prod, "/": div}


def prepare_plus_minus_array(strn):
    stack = []
    strn_len = len(strn)
    idx = 0
    idx = skip_spaces(strn, idx)
    number, idx = get_number(strn, idx)
    stack.append(number)
    while idx < strn_len:
        # extract number

        # skip space
        idx = skip_spaces(strn, idx)
        if idx >= strn_len:
            break
        operator = strn[idx]
        idx += 1
        idx = skip_spaces(strn, idx)
        # do operation now
        if operator in "*/":

            number, idx = get_number(strn, idx)
            idx = skip_spaces(strn, idx)
            number2 = stack.pop()
            number = OPERATE[operator](number2, number)
            stack.append(number)
        else:
            stack.append(operator)
            number, idx = get_number(strn, idx)
            stack.append(number)
    return stack


def calculate(strn):
    array = prepare_plus_minus_array(strn)
    # print("*" * 80)
    # print("ironman array", array)
    res = array[0]
    idx = 0
    while idx < len(array) - 1:
        res = OPERATE[array[idx + 1]](res, array[idx + 2])
        idx += 2
    # print(res)
    return res


s = "3+2*2"
s = " 3/2 "
s = " 3+5 / 2 "
s = "42"
s = "5   "
s = "1 + 1"
print(calculate(s))
