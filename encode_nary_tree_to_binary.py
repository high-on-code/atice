from collections import deque


class Codec:
    def encode(self, root):
        if not root:
            return None
        rootNode = TreeNode(root.val)
        qu = deque([(rootNode, root)])
        while qu:
            parent, curr = qu.popleft()
            prevBNode = None
            headBNode = None
            for child in curr.children:
                newBNode = TreeNode(child.val)
                if prevBNode:
                    prevBNode.right = newBNode
                else:
                    headBNode = newBNode
                prevBNode = newBNode
                qu.append((newBNode, child))
        parent.left = headBNode
        return rootNode

    def decode(self, root):
        if not root:
            return None
        rootNode = Node(root.val, [])
        qu = deque([(rootNode, root)])
        while qu:
            parent, curr = qu.popleft()
            first_child = curr.left
            sibling = first_child
            while sibling:
                newNode = Node(sibling.val, [])
                parent.children.append(newNode)
                qu.append((newNode,sibling))
                sibling = sibling.right
        return rootNode
