from collections import defaultdict


def find_sub_array_sum(nums, k):
    cache = defaultdict(lambda: 0)
    summ = 0
    res = 0
    cache[0] = 1
    for val in nums:
        summ += val
        # (Better) (Notice) by changin the order of res + and cache[summ] we are
        # able to solve the issue where k=0
        res += cache[summ - k]
        cache[summ] += 1
    return res


nums = [1, 1, 1]
k = 2
nums = [1, 2, 2]
k = 0
print(find_sub_array_sum(nums, k))
