# https://leetcode.com/problems/spiral-matrix-ii/
def create_spiral_matrix(num):
    res = [[0] * num for _ in range(num)]
    print(res)
    layers = (num + 1) // 2
    idx = 1
    for layer in range(layers):
        for col in range(layer, num - layer):
            res[layer][col] = idx
            idx += 1
        for row in range(layer + 1, num - layer):
            res[row][num - layer - 1] = idx
            idx += 1
        for col in range(num - layer - 2, layer - 1, -1):
            res[num - layer - 1][col] = idx
            idx += 1
        for row in range(num - layer - 2, layer, -1):
            res[row][layer] = idx
            idx += 1
        return res
    
n = 3
print(create_spiral_matrix(n))