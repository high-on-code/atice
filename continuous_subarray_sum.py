def find_continuous_subarray_sum(nums, k):
    pref_map = {}
    presum = 0
    pref_map[0] = 0
    for idx, val in enumerate(nums):
        presum += val
        rem = presum % k
        if rem not in pref_map:
            pref_map[rem] = idx + 1
        elif pref_map[rem] < idx:
            return True
    return False
