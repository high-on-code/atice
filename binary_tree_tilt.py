def find_binary_tree_tilt(root):
    if not root:
        return 0, 0
    (left_sum, ltilt) = find_binary_tree_tilt(root.left)
    (right_sum, rtilt) = find_binary_tree_tilt(root.right)
    subtree_sum = root.val + left_sum + right_sum
    return subtree_sum, ltilt + rtilt + abs(left_sum - right_sum)
