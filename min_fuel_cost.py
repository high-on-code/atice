from collections import defaultdict
import math


def form_graph(roads):
    graph = defaultdict(list)
    for u, v in roads:
        graph[u].append(v)
        graph[v].append(u)
    return graph


def find_min_fuel_cost_recur(parent, child, adj, seats):

    reps = 1
    fuel = 0
    for ch in adj[child]:
        if parent == ch:
            continue
        res = find_min_fuel_cost_recur(child, ch, adj, seats)
        reps += res[0]
        fuel += res[1]
    if child != 0:

        fuel += math.ceil(reps / seats)
    return reps, fuel


def find_min_fuel_cost(roads, seats):
    adj = form_graph(roads)
    print("*" * 80)
    print("ironman adj", adj)
    return find_min_fuel_cost_recur(-1, 0, adj, seats)[1]


roads = [[0, 1], [0, 2], [0, 3]]
seats = 5
print(find_min_fuel_cost(roads, seats))
