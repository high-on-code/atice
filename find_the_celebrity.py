def is_celebrity(celeb_cand, peeps):
    for ap in range(peeps):
        if ap == celeb_cand:
            continue
        if knows(celeb_cand, ap) or not knows(ap, celeb_cand):
            return -1
    return celeb_cand


def find_celebrity(peeps):
    celeb_cand = 0
    for ap in range(peeps):
        if knows(celeb_cand, ap):
            celeb_cand = ap
    return is_celebrity(celeb_cand, peeps)


[[1, 1], [1, 1]]
