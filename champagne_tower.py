def champagne_tower(poured, query_row, query_glass):
    res = [[poured]]
    for ridx in range(query_row):
        next_row = [0] * (ridx + 2)
        res.append(next_row)
        for cidx in range(ridx + 1):
            next_val = (res[ridx][cidx] - 1) / 2
            if next_val < 0:
                continue
            next_row[cidx] += next_val
            next_row[cidx + 1] += next_val
    # print(res[-1])
    return min(1, res[query_row][query_glass])


def champagneTower(poured, query_row, query_glass):
    A = [[0] * k for k in range(1, 102)]
    A[0][0] = poured
    for r in range(query_row + 1):
        for c in range(r + 1):
            q = (A[r][c] - 1.0) / 2.0
            if q > 0:
                A[r + 1][c] += q
                A[r + 1][c + 1] += q
    print(A[query_row])
    return min(1, A[query_row][query_glass])


poured = 1
query_row = 1
query_glass = 1

poured = 2
query_row = 1
query_glass = 1

poured = 3
query_row = 1
query_glass = 1

poured = 4
query_row = 1
query_glass = 1

poured = 4
query_row = 2
query_glass = 1

poured = 100000009
query_row = 33
query_glass = 17

poured = 25
query_row = 6
query_glass = 1
poured = 6
query_row = 2
query_glass = 0
print(champagne_tower(poured, query_row, query_glass))
# print(champagneTower(poured, query_row, query_glass))
