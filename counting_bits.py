res = []


def countBits(n):
    if len(res) >= n + 1:
        return res[: n + 1]
    for ni in range(len(res), n + 1):
        curr = 0
        while ni:
            if ni | 1 == ni:
                curr += 1
            ni >>= 1
        res.append(curr)
    return res


n = 3
print(countBits(n))
