def dfs(start, result, current_list, strn, cache):
    if start >= len(strn):
        result.append(current_list[:])
    for end in range(start, len(strn)):
        if strn[start] == strn[end] and (end - start <= 2 or cache[start + 1][end - 1]):
            cache[start][end] = True
            current_list.append(strn[start : end + 1])
            dfs(end + 1, result, current_list, strn, cache)
            current_list.pop()


def partition(strn):
    cache = [[False] * len(strn) for _ in range(len(strn))]
    result = []
    dfs(0, result, [], strn, cache)
    return result


print(partition("aab"))
