MOD = 1000000007


def find_paths_recur(m, n, max_moves, sr, sc, cache):

    if sr == m or sc == n or sr < 0 or sc < 0:
        return 1
    if max_moves == 0:
        return 0
    if cache[sr][sc][max_moves] > -1:
        return cache[sr][sc][max_moves]

    res = 0
    for dx, dy in ((0, 1), (0, -1), (1, 0), (-1, 0)):
        nx, ny = dx + sr, dy + sc
        res += find_paths_recur(m, n, max_moves - 1, nx, ny, cache)
    cache[sr][sc][max_moves] = res
    return res


def find_paths(m, n, max_moves, sr, sc):
    cache = []
    for rid in range(m):
        cr = []
        for cid in range(n):
            # cc = [[-1] * max_moves]
            cc = [-1] * (max_moves + 1)

            cr.append(cc)
        cache.append(cr)
    return find_paths_recur(m, n, max_moves, sr, sc, cache) % MOD
    # print("*" * 80)
    # print("ironman cache", cache[0][0])


m = 1
n = 3
maxMove = 3
startRow = 0
startColumn = 1
print(find_paths(m, n, maxMove, startRow, startColumn))
