from functools import lru_cache


@lru_cache
def calculate_maximum_score_from_multiplications_helper(nums, multipliers, idx, left):
    if idx == len(multipliers):
        return 0
    return max(
        multipliers[idx] * nums[left]
        + calculate_maximum_score_from_multiplications_helper(
            nums, multipliers, idx + 1, left + 1
        ),
        multipliers[idx] * nums[len(nums) - 1 - (idx - left)]
        + calculate_maximum_score_from_multiplications_helper(
            nums, multipliers, idx + 1, left
        ),
    )


def calculate_maximum_score_from_multiplications(nums, multipliers):
    return calculate_maximum_score_from_multiplications_helper(
        tuple(nums), tuple(multipliers), 0, 0
    )


nums = [1, 2, 3]
multipliers = [3, 2, 1]
print(calculate_maximum_score_from_multiplications(nums, multipliers))
