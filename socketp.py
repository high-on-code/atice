import socket
HOST = "127.0.0.1" 
PORT = 65432
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    print("Server listening...")
    conn, addr = s.accept()
    with conn:
        print(f"Connected by {addr}")
        data = conn.recv(1024)
        sum_of_bytes = sum(data)
        print(f"Sum: {sum_of_bytes}")
        conn.sendall(sum_of_bytes.to_bytes(4, 'big'))

