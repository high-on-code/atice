def combination_sum3_backtrack(count, target, res, idx, next_val, combi):
    if target < 0:
        return
    if idx == count:
        if 0 == target:
            res.append(tuple(combi))
        else:
            return
    for idx3 in range(next_val, 10):
        if target - idx3 < 0:
            return
        combi[idx] = (idx3)
        combination_sum3_backtrack(
            count, target - idx3, res, idx + 1, idx3 + 1, combi
        )
        # combi.pop()


def combination_sum3(count, target):
    res = []
    combi = [0] * count
    combination_sum3_backtrack(count, target, res, 0, 1, combi)
    return res


k = 3
n = 9
print(combination_sum3(k, n))
