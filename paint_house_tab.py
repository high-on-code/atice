# https://leetcode.com/problems/paint-house/
INFI = float("inf")


def find_min_cost(costs):
    house_len = len(costs)
    color_len = len(costs[0])
    for hidx in range(house_len - 2, -1, -1):
        for cidx in range((color_len)):
            min_cost = INFI
            for ncidx, hcost in enumerate(costs[hidx + 1]):
                if cidx == ncidx:
                    continue
                min_cost = min(min_cost, hcost)
            costs[hidx][cidx] += min_cost
    return min(costs[0])


costs = [[17, 2, 17], [16, 16, 5], [14, 3, 19]]
costs = [[5, 8, 6], [19, 14, 13], [7, 5, 12], [14, 15, 17], [3, 20, 10]]
print(find_min_cost(costs))
