def get_ch_count(word):
    counter = [0] * 26
    for ch in word:
        counter[ord(ch) - ord('a')] += 1
    return counter


def find_all_anagrams_in_str(strn, word):
    word_len = len(word)
    strn_len = len(strn)
    if word_len > strn_len:
        return None
    counter1 = get_ch_count(word)
    counter2 = get_ch_count(strn[:word_len])
    res_list = []
    if counter2 == counter1:
        res_list.append(0)
    for idx in range(1, strn_len - word_len + 1):
        counter2[ord(strn[idx - 1]) - ord('a')] -= 1
        counter2[ord(strn[idx + word_len - 1]) - ord('a')] += 1
        if counter1 == counter2:
            res_list.append(idx)
    return res_list


strn = "cbaebabacd"
word = "abc"
print(find_all_anagrams_in_str(strn, word))
