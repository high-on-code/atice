def find_longest_valid_parans(strn):
    left = 0
    right = 0
    max_lent = 0
    for ch in strn:
        if ch == "(":
            left += 1
        else:
            right += 1
        if right > left:
            left, right = 0, 0
        if left == right:
            max_lent = max(max_lent, left + right)
    left = 0
    right = 0
    for ch in strn[::-1]:

        if ch == "(":
            left += 1
        else:
            right += 1
        if right < left:
            left, right = 0, 0
        if left == right:
            max_lent = max(max_lent, left + right)
    return max_lent


s = "(()"
print(find_longest_valid_parans(s))
