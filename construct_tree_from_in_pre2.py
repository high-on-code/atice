def construct_tree_from_in_pre_recur(preorder, inorder, left, right, pre_idx, in_map):
    if left > right:
        return None, pre_idx - 1
    root = TreeNode(preorder[pre_idx])
    node, npre_idx = construct_tree_from_in_pre_recur(
        preorder, inorder, left, in_map[preorder[pre_idx]] - 1, pre_idx + 1, in_map
    )
    root.left = node

    node, npre_idx = construct_tree_from_in_pre_recur(
        preorder, inorder, in_map[preorder[pre_idx]] + 1, right, npre_idx + 1, in_map
    )
    root.right = node

    return root, npre_idx


def construct_tree_from_in_pre(preorder, inorder):
    in_map = {val: idx for idx, val in enumerate(inorder)}
    return construct_tree_from_in_pre_recur(
        preorder, inorder, 0, len(inorder) - 1, 0, in_map
    )[0]
