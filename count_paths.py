# https://leetcode.com/problems/number-of-increasing-paths-in-a-grid/
def count_paths(grid):
    cache = [[1] * len(grid[0]) for _ in range(len(grid))]
    numr, numc = len(grid), len(grid[0])
    mod = 10 ** 9 + 7
    cells = [(i, j) for i in range(numr) for j in range(numc)]
    print(cells)
    cells.sort(key=lambda x: grid[x[0]][x[1]])
    for i, j in cells:
        for dx, dy in ((-1, 0), (1, 0), (0, -1), (0, 1)):
            x, y = i + dx, j + dy
            if 0 <= x < numr and 0 <= y < numc and grid[x][y] > grid[i][j]:
                cache[x][y] += cache[i][j]
                cache[x][y] %= mod
    return sum(sum(row) % mod for row in cache) % mod

grid = [[1,1],[3,4]]
print(count_paths(grid))