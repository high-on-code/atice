import math


# (Better) used less number of inputs by using nums_left and diffing from target
def find_equal_partitions_helper(nums, nums_left, target, cache):
    if cache[nums_left][target] is not None:
        return cache[nums_left][target]
    if nums_left == 0:
        return False
    elif target == 0:
        return True
    res = find_equal_partitions_helper(nums, nums_left - 1,
                                       target - nums[nums_left - 1],
                                       cache) or find_equal_partitions_helper(
                                           nums, nums_left - 1, target, cache)
    cache[nums_left][target] = res
    return res


def find_equal_partitions(nums):
    target = sum(nums) / 2
    if math.floor(target) != target:
        return False
    target = int(target)
    cache = [[None] * (target + 1) for _ in range(len(nums) + 1)]
    return find_equal_partitions_helper(nums, len(nums), target, cache)


nums = [1, 5, 11, 5]
# nums = [1, 2, 3, 5]
print(find_equal_partitions(nums))
