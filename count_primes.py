import math


def count_primes(limi):
    sieve = [False, False] + ([True] * (limi - 2))
    for idx in range(2, math.floor(math.sqrt(limi)) + 1):
        if sieve[idx]:
            for idx2 in range(idx * idx, limi, idx):
                sieve[idx2] = False
    print(sieve)
    return sum(sieve)


print(count_primes(5))
print(count_primes(10))
