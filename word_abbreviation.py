from collections import defaultdict


def shorten_word(word, idx):
    if len(word) - idx - 1 > 1:
        return word[:idx] + str(len(word) - idx - 1) + word[-1]
    else:
        return word


def find_word_abbrevations(words):
    groups = defaultdict(list)
    for idx, aw in enumerate(words):
        groups[len(aw), aw[0], aw[-1]].append((aw, idx))
    print(groups)
    Trie = lambda: defaultdict(Trie)
    COUNT = "count"
    for a_group in groups.values():
        trie = Trie()
        if len(a_group) == 1:
            word, idx = a_group[0]
            words[idx] = shorten_word(word, 1)
            continue
        else:
            for word, idx in a_group:
                cur = trie
                for ch in word[1:]:
                    cur[COUNT] = cur.get(COUNT, 0) + 1
                    cur = cur[ch]
                    
        for word, idx2 in a_group:
            curr = trie
            for idx, ch in enumerate(word[1:], 1):
                if curr[COUNT] == 1:
                    words[idx2] = shorten_word(word, idx)
                    break
                curr = curr[ch]
        # for aw, idx in a_group:
        #     if len(aw) == 1:
        #         words[]
    return words


words = [
    "like",
    "god",
    "internal",
    "me",
    # "internet",
    # "interval",
    "intension",
    "face",
    "intrusion",
]
print(find_word_abbrevations(words))
