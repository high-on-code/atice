# https://leetcode.com/problems/number-of-subsequences-that-satisfy-the-given-sum-condition/
def find_num_subseq(nums, tar):
    ans = 0
    nlen = len(nums)
    nums.sort()
    for lidx in range(nlen):
        for ridx in range(lidx, nlen):
            if tar - nums[lidx] >= nums[ridx]:
                ans += 1
    return ans