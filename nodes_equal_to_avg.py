def find_nodes_equal_to_avg_recur(root):
    if not root:
        return 0, 0, 0
    res, summ, count = find_nodes_equal_to_avg_recur(root.left)
    res2, summ2, count2 = find_nodes_equal_to_avg_recur(root.right)
    count3 = count + count2 + 1
    summ3 = summ + summ2 + root.val
    res3 = res + res2
    if root.val == summ3 / count3:
        res3 = res + res2 + 1
    return res3, summ3, count3

def find_nodes_equal_to_avg(root):
    return find_nodes_equal_to_avg_recur(root)[0]