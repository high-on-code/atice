def find_root(n, lc, rc):
    children = set(lc) | set(rc)
    for idx in range(n):
        if idx not in children:
            return idx
    return None
def validate_binary_tree_nodes_recur(node, lc, rc, seen):
    clc = lc[node]
    res = True
    if clc != -1:
        if clc in seen:
            return False
        seen.add(clc)
        res = validate_binary_tree_nodes_recur(clc, lc, rc, seen)
    crc = rc[node]
    if crc != -1:
        if crc in seen:
            return False
        seen.add(crc)
        res = res and validate_binary_tree_nodes_recur(crc, lc, rc, seen)
    return res

def validate_binary_tree_nodes(n, lc, rc):
    root = find_root(n, lc, rc)
    if root is None:
        return False
    stack = []
    stack.append(root)
    seen = set()
    seen.add(root)
    res = validate_binary_tree_nodes_recur(root, lc, rc, seen)
    return len(seen) == n and res


n = 4
leftChild = [1,-1,3,-1]
rightChild = [2,-1,-1,-1]
print(validate_binary_tree_nodes(n, leftChild, rightChild))
