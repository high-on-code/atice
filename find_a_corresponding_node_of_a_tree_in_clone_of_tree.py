def find_node_in_clone(original, cloned, target):
    stack = [(original, cloned)]
    while stack:
        on, cn = stack[-1]
        while on.left:
            on, cn = on.left, cn.left
            stack.append((on, cn))
        on, cn = stack.pop()
        if on == target:
            return cn
        if on.right:
            stack.append((on.right, cn.right))
