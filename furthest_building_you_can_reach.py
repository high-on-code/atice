import heapq


def find_furthest_building(heights, bricks, ladders):
    climbs_so_far = []
    height_len = len(heights)
    for idx in range(height_len - 1):
        height_diff = heights[idx + 1] - heights[idx]
        if height_diff < 0:
            continue
        heapq.heappush(climbs_so_far, height_diff)
        if len(climbs_so_far) > ladders:
            bricks -= heapq.heappop(climbs_so_far)
        if bricks < 0:
            return idx
    return height_len - 1


heights = [4, 2, 7, 6, 9, 14, 12]
bricks = 5
ladders = 1

heights = [4, 12, 2, 7, 3, 18, 20, 3, 19]
bricks = 10
ladders = 2
heights = [2, 7, 9, 12]
bricks = 5
ladders = 1
print(find_furthest_building(heights, bricks, ladders))
