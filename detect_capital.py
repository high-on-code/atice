def check_lower(strn):
    for ch in strn:
        if not ord("a") <= ord(ch) <= ord("z"):
            return False
    return True


def check_upper(strn):
    for ch in strn:
        if not ord("A") <= ord(ch) <= ord("Z"):
            return False
    return True


def detect_capital_use(strn):
    if check_lower(strn[0]):
        return check_lower(strn)

    elif check_upper(strn):
        return True
    else:
        return check_lower(strn[1:])


word = "USA"
word = "FlaG"
print(detect_capital_use(word))
