def find_min_bits_to_monotone_str(strn):
    m = strn.count("0")
    res = m
    for ch in strn:
        if ch == "0":
            m -= 1
            res = min(res, m)
        else:
            m += 1
    return res


s = "00110"

print(find_min_bits_to_monotone_str(s))
