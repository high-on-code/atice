def jump_game_recur(nums, mem, idx):
    if idx == len(nums) - 1:
        return 1
    if idx >= len(nums):
        return 0
    if mem[idx] == 0:
        return 0
    for idx2 in range(1, nums[idx] + 1):
        if jump_game_recur(nums, mem, idx + idx2) == 1:
            mem[idx] = 1
            return 1
    return 0


def jump_game(nums):
    num_len = len(nums)
    mem = [-1] * num_len
    mem[-1] = 1
    jump_game_recur(nums, mem, 0)
    return mem[0] == 1


nums = [2, 3, 1, 1, 4]
nums = [3, 2, 1, 0, 4]
nums = [2, 0, 0]

print(jump_game(nums))
