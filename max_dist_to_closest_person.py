import math


def find_max_dist_to_closest_person(seats):
    ans = 0
    zero_count = 0
    for a_seat in seats:
        if a_seat:
            ans = max(ans, math.ceil(zero_count / 2))
            zero_count = 0
        else:
            zero_count += 1
    zero_count = 0
    for a_seat in seats:
        if a_seat:
            ans = max(ans, zero_count)
            break
        else:
            zero_count += 1
    zero_count = 0
    for idx in range(len(seats) - 1, -1, -1):
        if seats[idx]:
            ans = max(ans, zero_count)
            break
        else:
            zero_count += 1
    return ans


seats = [1, 0, 0, 0, 1, 0, 1]
seats = [1,0,0,0]
print(find_max_dist_to_closest_person(seats))
