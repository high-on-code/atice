from collections import defaultdict


def find_max_erasure_values(data):
    max_sum = 0
    le = 0
    curr_sum = 0
    val_idx = defaultdict(lambda: None)
    data_len = len(data)
    for ri, val in enumerate(data):
        if val_idx[val] is not None:
            max_sum = max(max_sum, curr_sum)
            next_le = val_idx[val] + 1
            while le < next_le:
                curr_sum -= data[le]
                le += 1

        val_idx[val] = ri
        curr_sum += data[ri]
    return max(max_sum, curr_sum)


nums = [4, 2, 4, 5, 6]
nums = [5, 2, 1, 2, 5, 2, 1, 2, 5]
print(find_max_erasure_values(nums))
