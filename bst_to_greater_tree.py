summ = 0


def convert_bst_to_greater_tree(root):
    global summ
    if not root:
        return None
    # print("*" * 80)
    # print("ironman root.val", root.val, 0)
    convert_bst_to_greater_tree(root.right)
    summ += root.val
    root.val = summ
    convert_bst_to_greater_tree(root.left)
    return root
