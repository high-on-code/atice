def excel_sheet_col(columnNumber):
    strn = []
    while columnNumber > 0:
        columnNumber -= 1
        calph = columnNumber  % 26
        columnNumber //= 26
        strn.append(chr(ord('A') + (calph)))
    return "".join(strn)[::-1]


columnNumber = 1
print(excel_sheet_col(columnNumber))
