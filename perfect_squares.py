import math


def find_perfect_squares(num):
    squares = [i ** 2 for i in range(1, int(math.sqrt(num)) + 1)]
    squares = set(squares)

    def summable(numm, count):
        if count == 1:
            return numm in squares
        for a_sq in squares:
            if a_sq < numm and summable(numm - a_sq, count - 1):
                return True
        return False

    for count in range(1, num + 1):
        if summable(num, count):
            return count


print(find_perfect_squares(13))
