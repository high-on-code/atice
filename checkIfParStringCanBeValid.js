function canBeValid(s, locked) {
    if (s.length % 2 === 1) {
        return false;
    }
    const opened = [];
    const unLocked = [];
    for (let i = 0; i < s.length; i++) {
        if (locked[i] === '0') {
            unLocked.push(i);
        } else if (s[i] === '(') {
            opened.push(i);
        } else if (s[i] === ')') {
            if (opened.length > 0) {
                opened.pop();
            } else if (unLocked.length > 0) {
                unLocked.pop();
            } else {
                return false;
            }
        }
    }
    while (unLocked.length > 0 && opened.length > 0 && unLocked.at(-1) > opened.at(-1)) {
        unLocked.pop();
        opened.pop();
    }
    if (opened.length > 0) {
        return false;
    } else {
        return true;
    }
}

let locked ="010100"
let s ="))()))"
console.log(canBeValid(s, locked))