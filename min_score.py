from collections import deque
import sys

INFI = sys.maxsize


def make_graph(n, roads):
    graph = [[] for _ in range(n + 1)]
    for fr, ed, dist in roads:
        graph[fr].append(
            (
                ed,
                dist,
            )
        )
        graph[ed].append((fr, dist,))
    return graph


def min_score(n, roads):
    qu = deque()
    visited = set()
    graph = make_graph(n, roads)
    print("iron man graph", graph)
    qu.append(1)
    visited.add(1)
    answer = INFI
    while qu:
        node = qu.popleft()
        for ed, dist in graph[node]:
            # if answer > dist:
            # answer = dist
            answer = min(answer, dist)
            if ed not in visited:

                qu.append(ed)
                visited.add(ed)
    return answer


n = 4

roads = [
    [2, 9, 2308],
    [2, 5, 2150],
    [12, 3, 4944],
    [13, 5, 5462],
    [2, 10, 2187],
    [2, 12, 8132],
    [2, 13, 3666],
    [4, 14, 3019],
    [2, 4, 6759],
    [2, 14, 9869],
    [1, 10, 8147],
    [3, 4, 7971],
    [9, 13, 8026],
    [5, 12, 9982],
    [10, 9, 6459],
]
n = 14
# roads = [[1, 2, 9], [2, 3, 6], [2, 4, 5], [1, 4, 7]]
print(min_score(n, roads))
