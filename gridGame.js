var gridGame = function(grid) {
    // store the sum of the first row grid[0]
    let firstRowSum = grid[0].reduce((a, b) => a + b, 0);
    // initialze variable to very large number
    let minSecond = 0;
    let result = Number.MAX_SAFE_INTEGER;
    let secondRowSum = 0;
    for (let idx = 0; idx < grid[0].length; idx++) {
        firstRowSum -= grid[0][idx];
        result = Math.min(result, Math.max(firstRowSum, secondRowSum));
        secondRowSum += grid[1][idx];
    }
    return result;
};
let grid = [[2,5,4],[1,5,1]];
grid = [[1,3,1,15],[1,3,3,1]];
console.log(gridGame(grid));
