from collections import defaultdict


def delete_and_earn(nums):
    max_number = 0
    cache = [0]
    points_to_gain = defaultdict(int)
    for val in nums:
        points_to_gain[val] += val
        max_number = max(max_number, val)
    cache.append(points_to_gain[1])
    for val in range(2, max_number + 1):
        cache.append(max(points_to_gain[val] + cache[val-2], cache[val - 1]))
    return cache[-1]


nums = [3, 4, 2]
print(delete_and_earn(nums))