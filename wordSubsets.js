var wordSubsets = function(words1, words2) {
    const maxWord = new Array(26).fill(0);
    for (const word of words2) {
        const wordArr = new Array(26).fill(0);
        for (const ch of word) {
            wordArr[ch.charCodeAt(0) - 'a'.charCodeAt(0)]++;
        }
        for (let i = 0; i < 26; i++) {
            maxWord[i] = Math.max(maxWord[i], wordArr[i]);
        }
    }
    return words1.filter(word => {
        const wordArr = new Array(26).fill(0);
        for (const ch of word) {
            wordArr[ch.charCodeAt(0) - 'a'.charCodeAt(0)]++;
        }
        for (let i = 0; i < 26; i++) {
            if (wordArr[i] < maxWord[i]) {
                return false;
            }
        }
        return true;
    });
};


words1 =["amazon","apple","facebook","google","leetcode"]
words2 = ["e","o"]

console.log(wordSubsets(words1, words2))