def addToArrayForm(num, k):
    kl = []
    while k:
        kl.append(k % 10)
        k //= 10
    kl.reverse()
    len1 = len(num)
    len2 = len(kl)
    ldf = len1 - len2
    if ldf < 0:
        ln = len2
        num = ([0] * -ldf) + num
    else:
        ln = len1
        kl = ([0] * ldf) + kl
    # print(num, kl)
    carry = 0
    res = [0] * ln
    for idx in range(ln - 1, -1, -1):
        cres = carry + kl[idx] + num[idx]
        cv = cres % 10
        carry = cres // 10
        res[idx] = cv
    if carry:
        return [carry] + res
    return res


num = [1, 2, 0, 0]
k = 34
num = [0]
k = 23

print(addToArrayForm(num, k))
