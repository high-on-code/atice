def find_summary_ranges(nums):
    if not nums:
        return nums
    if len(nums) == 1:
        nums[0] = str(nums[0])
        return nums
    range_start = 0
    res = []
    for idx in range(1, len(nums)):
        if nums[idx] != nums[idx - 1] + 1:
            if idx - 1 == range_start:
                res.append(str(nums[range_start]))
                range_start = idx
            else:
                res.append("{}->{}".format(nums[range_start], nums[idx - 1]))
                range_start = idx
    if idx == range_start:
        res.append(str(nums[range_start]))
        range_start = idx
    else:
        res.append("{}->{}".format(nums[range_start], nums[idx]))
        range_start = idx
    return res


nums = [0, 1, 2, 4, 5, 7]
# nums = [0, 2, 3, 4, 6, 8, 9]
nums = [-1]
print(find_summary_ranges(nums))
