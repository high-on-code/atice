var xorAllNums = function(nums1, nums2) {
    const freqDict = {};
    const num2len = nums2.length;
    const num1len = nums1.length;
    // fill freqDict with freq of nums1 and nums2
    for (const num of nums1) {
        if (freqDict[num]) {
            freqDict[num] += num2len;
        } else {
            freqDict[num] = num2len;
        }
    }
    for (const num of nums2) {
        if (freqDict[num]) {
            freqDict[num] += num1len;
        } else {
            freqDict[num] = num1len;
        }
    }
    // calculate xor of all pairs
    let res = 0;
    for (const num in freqDict) {
        if (freqDict[num] % 2 !== 0) {
            res ^= Number(num);
        }
    }
    return res;
};

let nums1 = [2,1,3]
let nums2 = [10,2,5,0]
console.log(xorAllNums(nums1, nums2))