from collections import deque


def find_minimum_knight_movies(x, y):
    qu = deque()
    qu.append((0, 0))
    visited = {(0, 0)}
    # print(qu)
    # print(visited)
    steps = 0
    while qu:
        steps += 1
        curr_wall_len = len(qu)
        for _ in range(curr_wall_len):
            # print(qu)
            cnode = qu.popleft()
            # print("*" * 80)
            # print("ironman cnode", cnode)
            cx, cy = cnode
            for dx, dy in (
                (1, 2),
                (-1, 2),
                (1, -2),
                (-1, -2),
                (2, 1),
                (-2, 1),
                (2, -1),
                (-2, -1),
            ):
                nx, ny = cx + dx, cy + dy
                if (nx, ny) == (x, y):
                    return steps
                elif (nx, ny) in visited:
                    continue
                else:
                    visited.add((nx, ny))
                    qu.append((nx, ny))


x = 2
y = 1
x = 5
y = 5
print(find_minimum_knight_movies(x, y))
