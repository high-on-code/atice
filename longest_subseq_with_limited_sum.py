import bisect


def find_longest_subseq_with_limited_sum(nums, queries):
    nums.sort()
    print("*" * 80)
    print("ironman nums", nums)
    nlen = len(nums)
    pref = [0] * nlen
    pref[0] = nums[0]
    for idx in range(1, nlen):
        pref[idx] = nums[idx] + pref[idx - 1]
    print(pref)
    ans = []
    for qu in queries:
        ans.append(bisect.bisect_right(pref, qu))
    return ans


nums = [4, 5, 2, 1]

queries = [3, 10, 21]
print(find_longest_subseq_with_limited_sum(nums, queries))
