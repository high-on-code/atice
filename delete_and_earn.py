from collections import Counter


def delete_and_earn(nums):
    num_count = Counter(nums)
    prev = None
    avoid, using = 0, 0
    for val in sorted(num_count):
        # Notice: good use of comparison by setting prev to None
        if val - 1 == prev:
            # TODO: See how the solution for house robber is working where we don't check the condition prev != val -1
            # is it because 0's in between are essentially removing the use of the check, that is making avoid = using
            avoid, using = max(avoid, using), val * num_count[val] + avoid
        else:
            avoid, using = max(avoid, using), val * num_count[val] + max(avoid, using)
        prev = val
    return max(avoid, using)


nums = [3, 4, 2]
nums = [2, 2, 3, 3, 3, 4]
print(delete_and_earn(nums))
