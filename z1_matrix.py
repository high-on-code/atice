from collections import deque


def find_nearest_0_helper(mat, rid, cid, dist_mat, visited):
    if mat[rid][cid] == 0:
        return 0
    qu = deque()
    qu.append((rid, cid))
    visited.add((rid, cid))
    distance = 0
    while qu:
        wall_len = len(qu)
        distance += 1

        while wall_len:
            ridx, cidx = qu.popleft()
            wall_len -= 1
            dists = ((0, 1), (0, -1), (1, 0), (-1, 0))
            for dx, dy in dists:
                nx, ny = dx + ridx, dy + cidx
                if 0 <= nx < len(mat) and 0 <= ny < len(mat[0]):
                    if mat[nx][ny] == 0:
                        dist_mat[rid][cid] = distance
                        return
                    elif (nx, ny) not in visited:
                        qu.append((nx, ny))
                        visited.add((nx, ny))


def find_nearest_0(mat):
    dist_mat = [[0] * len(mat[0]) for _ in range(len(mat))]
    for rid, row in enumerate(mat):
        for cid, val in enumerate(row):
            find_nearest_0_helper(mat, rid, cid, dist_mat, set())
    return dist_mat


# mat = [[0, 0, 0], [0, 1, 0], [0, 0, 0]]
# mat = [[0, 0, 0], [0, 1, 0], [1, 1, 1]]
mat = [[0], [0], [0], [0], [0]]
mat = [
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [0, 0, 0],
]
# mat = [[1, 1, 1], [1, 1, 1], [0, 0, 0]]

dist_mat = find_nearest_0(mat)
print(dist_mat)
