def find_kth_largest_recur(nums, k, start, end):
    pivot = nums[start]
    pivot_idx = start
    for idx, val in enumerate(nums[pivot_idx + 1 :], pivot_idx + 1):
        if val < pivot:
            nums[idx], nums[pivot_idx] = nums[pivot_idx], nums[idx]
            pivot_idx += 1
    nums[pivot_idx] = pivot
    if pivot_idx == k - 1:
        return pivot
    elif pivot_idx > k - 1:
        return find_kth_largest_recur(nums, k, start, pivot_idx - 1)
    else:
        return find_kth_largest_recur(nums, k, pivot_idx + 1, end)


def find_kth_largest(nums, k):
    return find_kth_largest_recur(nums, len(nums) - k, 0, len(nums) - 1)


nums = [3, 2, 1, 5, 6, 4]
k = 2
# k = 3
k = 5
nums = [5, 2, 4, 1, 3, 6, 0]
k = 4
nums = [
    3,
    2,
    3,
    1,
    2,
    4,
    5,
    5,
    6,
    7,
    7,
    8,
    2,
    3,
    1,
    1,
    1,
    10,
    11,
    5,
    6,
    2,
    4,
    7,
    8,
    5,
    6,
]
k = 1
# nums = [1,1,2,2]
# k = 1
print(find_kth_largest(nums, k))
