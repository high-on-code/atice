# find out number of primes till 50
def find_primes_till_50():
    # loop to find primes till 50
    for idx in range(2, 50):
        # check if number is prime
        if idx > 1:
            for i in range(2, idx):
                if (idx % i) == 0:
                    break
            else:
                print(idx)
