var queryResults = function(limit, queries) {
    const ballColor = {};
    const colorCount = {};
    const result = [];
    for (const [ball, color] of queries) {
        if (ballColor[ball] !== undefined) {
            colorCount[ballColor[ball]]--;
            if (colorCount[ballColor[ball]] === 0) {
                delete colorCount[ballColor[ball]];
            }
        }
        ballColor[ball] = color;
        colorCount[color] = (colorCount[color] || 0) + 1;
        result.push(Object.keys(colorCount).length);
    }
    return result;
};
let limit = 4;
const queries = [[1,4],[2,5],[1,3],[3,4]];
console.log(queryResults(limit, queries)); // [true, true, false, true]