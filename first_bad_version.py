def find_first_bad_version(size):
    start = 0
    last = size - 1
    while start != last:
        mid = (start + last) // 2
        if isBadVersion(mid):
            start = mid + 1
        else:
            last = mid - 1
    return start
