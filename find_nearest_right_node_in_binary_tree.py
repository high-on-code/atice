from collections import deque


def nearest_right_node(root, val):
    if not root:
        return None
    qu = deque()
    qu.append(root)
    while qu:
        wall_size = len(qu)
        while wall_size:
            wall_size -= 1
            node = qu.popleft()
            if node.val == val:
                if wall_size > 0:
                    return qu.popleft().val
                else:
                    return None
            if node.left:
                qu.append(node.left)
            if node.right:
                qu.append(node.right)
                
    return None
