def find_longest_substr_without_rep(strn):
    char_map = {}
    left, right = 0, 0
    res = 0
    while right < len(strn):
        ch = strn[right]
        if ch in char_map:
            left = max(char_map[ch] + 1, left)
        char_map[ch] = right
        res = max(res, right - left + 1)

        right += 1
    return res


strn = "abcabcbb"
strn = "abba"
print(find_longest_substr_without_rep(strn))
