from collections import defaultdict


def jump_game_iv(nums):
    val_idx = defaultdict(list)
    nums_len = len(nums)
    if nums_len <= 1:
        return 0
    for idx, val in enumerate(nums):
        val_idx[val].append(idx)
    steps = 0
    forward_wall = {0}
    backward_wall = {nums_len - 1}
    visited = {0, nums_len - 1}
    while forward_wall:
        next_wall = set()
        if len(forward_wall) > len(backward_wall):
            forward_wall, backward_wall = backward_wall, forward_wall
        for val in forward_wall:
            for val2 in val_idx[nums[val]]:
                if val2 in backward_wall:
                    return steps + 1
                elif val2 not in visited:
                    visited.add(val2)
                    next_wall.add(val2)
            del val_idx[nums[val]]

            for val2 in [val + 1, val - 1]:
                if val2 in backward_wall:
                    return steps + 1
                if 0 <= val2 < nums_len and val2 not in visited:
                    visited.add(val2)
                    next_wall.add(val2)
        forward_wall = next_wall
        steps += 1
    return -1


arr = [100, -23, -23, 404, 100, 23, 23, 23, 3, 404]
arr = [1, 2, 3]
print(jump_game_iv(arr))
