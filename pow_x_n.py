def pow_helper(res, x, n):
    if not n:
        return res
    if n < 0:
        res *= 1 / x
    else:
        res *= x
    n = n / abs(n) * (abs(n) - 1)
    return pow_helper(res, x, n)


def my_pow(x, n):
    return pow_helper(1, x, n)


print(pow_helper(1, 2, -2))
