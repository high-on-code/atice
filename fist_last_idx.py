# https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array
def find_first_last_idx(nums, target):
    first, last = -1, -1
    for idx, val in enumerate(nums):
        if val == target:
            if first == -1:
                first = idx
            last = idx
    return first, last

nums = [5,7,7,8,8,10]
target = 8
print(find_first_last_idx(nums, target))