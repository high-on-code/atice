def find_players_with_01_losses(matches):
    cache = {}
    for wi, lo in matches:
        if wi not in cache:
            cache[wi] = [0, 0]
        if wi not in cache:
            cache[lo] = [0, 0]
        cache[wi][0] += 1
        cache[lo][1] += 1
    wins, loss = [], []
    for pl, (w, l) in cache.items():
        if l == 0:
            wins.append(pl)
        if l == 1:
            loss.append(pl)
    return [wins, loss]