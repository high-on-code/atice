def find_maxswap(num):
    strnum = str(num)
    strnum = list(strnum)
    numlen = len(strnum)
    max_right_idx = [None] * numlen
    max_right_idx[-1] = numlen - 1
    for idx in range(numlen - 2, -1, -1):
        if strnum[idx] > strnum[max_right_idx[idx + 1]]:
            max_right_idx[idx] = idx
        else:
            max_right_idx[idx] = max_right_idx[idx + 1]
    for idx in range(0, numlen - 1):
        if strnum[idx] < strnum[max_right_idx[idx]]:
            strnum[idx], strnum[max_right_idx[idx]] = strnum[max_right_idx[idx]], strnum[idx]
            break
    return int("".join(strnum))
num = 2736
num = 98368
# num = 22341345
print(find_maxswap(num))