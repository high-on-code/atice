def find_longest_common_subsequence(str1, str2):
    str1_len = len(str1)
    str2_len = len(str2)
    cache = [[0] * (len(str1) + 1) for _ in range(len(str2) + 1)]

    for idx1, val1 in enumerate(str2):
        for idx2, val2 in enumerate(str1):
            if val1 == val2:
                cache[idx1 + 1][idx2 + 1] = 1 + cache[idx1][idx2]
            else:
                cache[idx1 + 1][idx2 + 1] = max(
                    cache[idx1 + 1][idx2], cache[idx1][idx2 + 1]
                )
    # print(cache)
    return cache[str2_len][str1_len]


text1 = "abcde"
text2 = "ace"
print(find_longest_common_subsequence(text1, text2))
