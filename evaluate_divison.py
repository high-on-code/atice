def evaluate_division(equations, values, queries):
    uf = {}

    def find(node):
        if node not in uf:
            uf[node] = node, 1
        gid, wgt = uf[node]
        if gid != node:
            new_gid, group_weight = find(gid)
            uf[node] = new_gid, wgt * group_weight
        return uf[node]

    def union(dvd, dvs, val):
        dvd_grp, dvd_wgt = find(dvd)
        dvs_grp, dvs_wgt = find(dvs)
        if dvd_grp != dvs_grp:
            uf[dvd_grp] = dvs_grp, dvs_wgt * val / dvd_wgt

    for (dvd, dvs), val in zip(equations, values):
        union(dvd, dvs, val)
    results = []
    for dvd, dvs in queries:
        if dvd not in uf or dvs not in uf:
            results.append(-1.0)
        else:
            dvd_gid, dvd_wgt = find(dvd)
            dvs_gid, dvs_wgt = find(dvs)
            if dvd_gid != dvs_gid:
                results.append(-1.0)
            else:
                results.append(dvd_wgt / dvs_wgt)
    return results


equations = [["a", "b"], ["b", "c"]]
values = [2.0, 3.0]
queries = [["a", "c"], ["b", "a"], ["a", "e"], ["a", "a"], ["x", "x"]]
print(evaluate_division(equations, values, queries))
