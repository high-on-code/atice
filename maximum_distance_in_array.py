def find_max_dis_in_array(all_arrays):
    res = 0
    min_val = all_arrays[0][0]
    max_val = all_arrays[0][-1]

    for next_arr in all_arrays[1:]:
        res = max(res, next_arr[-1] - min_val, max_val - next_arr[0])
        min_val = min(min_val, next_arr[0])
        max_val = max(max_val, next_arr[-1])
    return res


arrays = [[1, 2, 3], [4, 5], [1, 2, 3]]
print(find_max_dis_in_array(arrays))
