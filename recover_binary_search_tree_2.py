def recover_tree(root):
    pred = TreeNode(-float("inf"))
    stack = []
    rog1 = None
    rog2 = None
    while True:
        while root:
            stack.append(root)
            root = root.left
        root = stack.pop()
        if pred.val > root.val:
            if rog1:
                rog2 = root
                break
            else:
                rog1 = root
                rog2 = pred

        pred = root
        root = root.right
    rog1.val, rog2.val = rog2.val, rog1.val
    return root
