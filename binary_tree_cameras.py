# 0: covered
# -1: need camera
# 1: have camera
def binary_tree_cameras_recur(root):
    if not root:
        return 0, 0
    lcr, resl = binary_tree_cameras_recur(root.left)
    rcr, resr = binary_tree_cameras_recur(root.right)
    res = resl + resr
    if lcr == -1 or rcr == -1:
        return 1, res + 1
    elif lcr == 1 or rcr == 1:
        return 0, res
    elif lcr == rcr == 0:
        return -1, res


def binary_tree_cameras(root):
    need, res = binary_tree_cameras_recur(root)
    if need == -1:
        res += 1
    return res
