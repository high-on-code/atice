def deepest_leaves_sum(root):
    if not root:
        return None
    stack = [(root, 0)]
    max_depth = 0
    res = 0
    while stack:
        node, depth = stack.pop()
        if not node.left and not node.right:
            if depth == max_depth:
                res += node.val
            elif depth > max_depth:
                res = node.val
                max_depth = depth
        if node.right:
            stack.append((node.right, depth + 1))
        if node.left:
            stack.append((node.left, depth + 1))
    return res