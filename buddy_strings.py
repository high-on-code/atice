# https://leetcode.com/problems/buddy-strings/
from collections import Counter
def find_buddy_strings(s1, s2):
    if len(s1) != len(s2):
            return False

    unmatch_idxs = []
    for idx in range(len(s1)):
        if s1[idx] != s2[idx]:
            unmatch_idxs.append(idx)
            if len(unmatch_idxs) > 2:
                return False
    if not unmatch_idxs:
        char_count = Counter(s1)
        if all([x == 1 for x in char_count.values()]):
            return False
        else:
            return True
    if len(unmatch_idxs) != 2 or s1[unmatch_idxs[0]] != s2[unmatch_idxs[1]] or s1[unmatch_idxs[1]] != s2[unmatch_idxs[0]]:
        return False
    return True

# s = "ab"
# goal = "ba"
s = "ab"
goal = "ab"
print(find_buddy_strings(s, goal))