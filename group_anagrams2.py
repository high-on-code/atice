def get_code(word):
    res = [0] * 26
    for ch in word:
        res[ord(ch) - ord("a")] += 1
    return tuple(res)


def group_anagrams(strs):
    mapper = {}
    for word in strs:
        code = get_code(word)
        if code not in mapper:
            mapper[code] = []
        mapper[code].append(word)
    return list(mapper.values())


strs = ["eat", "tea", "tan", "ate", "nat", "bat"]
print(group_anagrams(strs))
