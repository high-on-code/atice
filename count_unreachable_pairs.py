# https://leetcode.com/problems/count-unreachable-pairs-of-nodes-in-an-undirected-graph
def make_graph(n, connections):
    graph = [[] for _ in range(n)]
    for st, ed in connections:
        graph[st].append((ed))
        graph[ed].append((st))
    return graph


def find_component_size(node, graph, count, visited):
    for adj in graph[node]:
        if adj not in visited:
            count += 1
            visited.add(adj)
            count = find_component_size(adj, graph, count, visited)
    return count


def count_unreachable_pairs(n, edges):
    graph = make_graph(n, edges)
    visited = set()
    remaining = n
    pairs = 0
    for idx in range(n):
        if idx not in visited:
            count = 1
            visited.add(idx)
            count = find_component_size(idx, graph, count, visited)
            remaining -= count
            pairs += remaining * count
            # print("*" * 80)
            # print("ironman count", count)
    # ("*" * 80)
    # print("ironman graph", graph)
    return pairs


n = 3
edges = [[0, 1], [0, 2], [1, 2]]
n = 7
edges = [[0, 2], [0, 5], [2, 4], [1, 6], [5, 4]]
print(count_unreachable_pairs(n, edges))
