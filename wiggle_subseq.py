# https://leetcode.com/problems/wiggle-subsequence/
def find_largest_wiggle_subseq(nums):
    num_len = len(nums)
    if num_len == 1:
        return 1
    elif num_len == 2:
        return 1 + abs(nums[0] - nums[1])
    prev_diff = nums[0] - nums[1]
    res = 2 if nums[0] - nums[1] != 0 else 1
    for idx in range(1, num_len - 1):
        diff = nums[idx] - nums[idx + 1]
        if (diff > 0 and prev_diff <= 0) or (diff < 0 and prev_diff >= 0):
            res += 1
            prev_diff = diff
    return res


nums = [1, 7, 4, 9, 2, 5]
# nums = [1, 17, 5, 10, 13, 15, 10, 5, 16, 8]
# nums = [1, 2, 3, 4, 5, 6, 7, 8, 9]
# nums = [2, 2, 2, 2]

print(find_largest_wiggle_subseq(nums))
