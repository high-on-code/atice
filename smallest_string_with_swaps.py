from collections import defaultdict


class UnionFind:
    def __init__(self, size):
        self.groups = [val for val in range(size)]
        self.rank = [1] * size

    def find_group(self, val):
        if self.groups[val] != val:
            self.groups[val] = self.find_group(self.groups[val])
        return self.groups[val]

    def union(self, val1, val2):
        root_val1 = self.find_group(val1)
        root_val2 = self.find_group(val2)
        if root_val1 != root_val2:
            if self.rank[root_val1] > self.rank[root_val2]:
                self.groups[root_val2] = root_val1
                self.rank[root_val1] += self.rank[root_val2]
            else:
                self.groups[root_val1] = root_val2
                self.rank[root_val2] += self.rank[root_val1]


def smallest_string_with_with_swaps(strn, pairs):
    uff = UnionFind(len(strn))

    for st, ed in pairs:
        uff.union(st, ed)

    root_verts = defaultdict(list)
    for vt in range(len(strn)):
        root = uff.find_group(vt)
        root_verts[root].append(vt)
    smallest_string = [None] * len(strn)
    for indices in root_verts.values():
        str_lst = [strn[idx] for idx in indices]
        str_lst.sort()
        for idx, val in enumerate(indices):
            smallest_string[val] = str_lst[idx]
    return "".join(smallest_string)


s = "dcab"
pairs = [[0, 3], [1, 2]]
print(smallest_string_with_with_swaps(s, pairs))
