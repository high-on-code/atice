SEQ_STRING = "123456789"


def find_num_len(num):
    leng = 0
    while num:
        leng += 1
        num //= 10
    # print(leng)
    return leng


def find_sequential_digits(low, high):
    low_len = find_num_len(low)
    high_len = find_num_len(high)
    str_len = len(SEQ_STRING)
    output_list = []

    for curr_len in range(low_len, high_len + 1):
        for idx in range(str_len - curr_len + 1):
            curr_num = int(SEQ_STRING[idx : idx + curr_len])
            if low <= curr_num <= high:
                output_list.append(curr_num)
    return output_list


low = 1000
high = 13000
print(find_sequential_digits(low, high))
