from collections import defaultdict


def find_if_path_exists(edge_count, edges, src, dest):
    graph = defaultdict(list)
    for st, ed in edges:
        graph[st].append(ed)
        graph[ed].append(st)
    st = [src]
    visited = set()
    while st:
        node = st.pop()
        if node in visited:
            continue
        visited.add(node)
        if node == dest:
            return True

        for ed in graph[node]:
            st.append(ed)
    return False


# n = 3
# edges = [[0, 1], [1, 2], [2, 0]]
# source = 0
# destination = 2

n = 6
edges = [[0, 1], [0, 2], [3, 5], [5, 4], [4, 3]]
source = 0
destination = 5
print(find_if_path_exists(n, edges, source, destination))
