from collections import deque


def clone_graph(node):
    if not node:
        return node
    qu = deque()
    qu.append(node)
    new_graph = Node(node.val)
    cache = {node: new_graph}
    while qu:
        curr_node = qu.popleft()
        for nnode in curr_node.neighbors:
            if nnode in cache:
                cache[curr_node].neighbors.append(cache[nnode])
            else:
                new_node = Node(nnode.val)
                cache[nnode] = new_node
                qu.append(nnode)
                cache[curr_node].neighbors.append(new_node)
    return new_graph
