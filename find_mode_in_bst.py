# https://leetcode.com/problems/find-mode-in-binary-search-tree

def find_mode_in_bst_recur(root, currnum, curr_streak, max_streak, res):
    if not root:
        return
    if not root.left:
        if root.val == currnum:
            curr_streak += 1
            if curr_streak > max_streak:
                max_streak = curr_streak
                res = [root.val]
        else:
            curr_streak = 1
        if curr_streak == max_streak:
            res.append(root.val)
    else:
        currnum = find_mode_in_bst_recur(root.left, currnum, curr_streak, max_streak, res)


    find_mode_in_bst_recur(root.right, root.val, curr_streak, max_streak, res)
    
def find_mode_in_bst(root):
    res = []
    max_streak = 0
    currnum = -1
    find_mode_in_bst_recur(root, currnum, 0, max_streak, res)
    return res