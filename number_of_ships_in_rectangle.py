def find_number_of_ships_in_rectangle(sea, top_right, bottom_left):
    if top_right.x < bottom_left.x or top_right.y < bottom_left.y:
        return 0
    if not sea.hasShips(top_right, bottom_left):
        return 0
    if top_right.x == bottom_left.x or top_right.y == bottom_left.y:
        return 1
    midx, midy = (top_right.x + bottom_left.x) // 2, (top_right.y + bottom_left.y) // 2
    return (
        find_number_of_ships_in_rectangle(sea, Point(midx, midy), bottom_left)
        + find_number_of_ships_in_rectangle(sea, top_right, Point(midx + 1, midy + 1))
        + find_number_of_ships_in_rectangle(
            sea, Point(midx, top_right.y), Point(bottom_left.x, midy + 1)
        )
        + find_number_of_ships_in_rectangle(
            sea, Point(top_right.x, midy), Point(midx + 1, bottom_left.y)
        )
    )
