import sys

INFI = sys.maxsize


def find_cost_to_paint_house_recur(house_number, prev_color, costs, cache):
    if cache.get((house_number, prev_color)):
        return cache[(house_number, prev_color)]
    if house_number == len(costs):
        return 0
    current_cost = INFI
    for idx in range(len(costs[0])):
        if idx == prev_color:
            continue
        current_cost = min(
            current_cost,
            costs[house_number][idx]
            + find_cost_to_paint_house_recur(house_number + 1, idx, costs, cache),
        )
    cache[(house_number, prev_color)] = current_cost
    return current_cost


def find_cost_to_paint_house(costs):
    cache = {}
    # number_of_houses = len(costs)
    res = INFI
    for hn in range(len(costs[0])):
        res = min(res, find_cost_to_paint_house_recur(0, hn, costs, cache))
    return res


costs = [[17, 2, 17], [16, 16, 5], [14, 3, 19]]
costs = [[7, 6, 2]]
print(find_cost_to_paint_house(costs))
