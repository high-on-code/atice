# https://leetcode.com/problems/interleaving-string/
def find_if_strings_are_interleaving_recur(s1, s2, s3, idx1, idx2, cache):
    idx3 = idx1 + idx2

    if idx1 == len(s1):
        return s2[idx2:] == s3[idx1 + idx2 :]
    elif idx2 == len(s2):
        return s1[idx1:] == s3[idx1 + idx2 :]
    if cache[idx1][idx2] >= 0:
        return True if cache[idx1][idx2] == 1 else False

    ans = False
    if (
        s1[idx1] == s3[idx3]
        and find_if_strings_are_interleaving_recur(s1, s2, s3, idx1 + 1, idx2, cache)
    ) or (
        s2[idx2] == s3[idx3]
        and find_if_strings_are_interleaving_recur(s1, s2, s3, idx1, idx2 + 1, cache)
    ):
        ans = True
    cache[idx1][idx2] = 1 if ans else 0
    return ans


def find_if_strings_are_interleaving(s1, s2, s3):
    if s1 == "":
        return s2 == s3
    elif s2 == "":
        return s1 == s3
    elif len(s1) + len(s2) != len(s3):
        return False
    cache = [[-1] * len(s2) for _ in range(len(s1))]
    for row in cache:
        print(row)
    return find_if_strings_are_interleaving_recur(s1, s2, s3, 0, 0, cache)


s1 = "aabcc"
s2 = "dbbca"
s3 = "aadbbcbcac"

# s1 = "aabcc"
# s2 = "dbbca"
# s3 = "aadbbbaccc"
s1 = "a"
s2 = "b"
s3 = "a"

s1 = "aa"
s2 = "ab"
s3 = "abaa"
s1 = "aabaac"
s2 = "aadaaeaaf"
s3 = "aadaaeaabaafaac"

s1 = "aabaac"
s2 = "aadaaeaaf"
s3 = "aadaaeaabaafaac"
print(find_if_strings_are_interleaving(s1, s2, s3))
