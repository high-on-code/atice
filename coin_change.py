from functools import lru_cache


@lru_cache(maxsize=None)
def coin_change_helper(coins, amount):
    if amount == 0:
        return 0
    elif amount < 0:
        return None
    min_coins = float("inf")
    for a_coin in coins:
        res = coin_change_helper(coins, amount - a_coin)
        if res is not None:
            min_coins = min(min_coins, coin_change_helper(coins, amount - a_coin))
    return 1 + min_coins if min_coins != float('inf') else -1


def coin_change(coins, amount):
    return coin_change_helper(tuple(coins), amount)


coins = [1, 2, 5]
amount = 11
print(coin_change(coins, amount))
