class UnionFind:
    def __init__(self, nodes):
        self.rank = [1] * nodes
        self.parent = [i for i in range(nodes)]

    def find(self, node):
        if self.parent[node] == node:
            return node
        else:
            self.parent[node] = self.find(self.parent[node])
            return self.parent[node]

    def union(self, node1, node2):
        # print("*" * 80)
        # print("ironman node1, node2", node1, node2)
        pnode1 = self.find(node1)
        pnode2 = self.find(node2)
        if pnode1 == pnode2:
            return False
        if self.rank[pnode1] > self.rank[pnode2]:
            self.parent[pnode2] = pnode1
        elif self.rank[pnode1] < self.rank[pnode2]:
            self.parent[pnode1] = pnode2
        else:
            self.parent[pnode2] = pnode1
            self.rank[pnode1] += 1
        # print("*" * 80)
        # print("ironman self.rank, self.parent", self.rank, self.parent)
        return True


def find_number_of_provinces(graph):
    nodes = len(graph)
    res = UnionFind(nodes)
    for rid, row in enumerate(graph):
        for cid, val in enumerate(row):
            if rid != cid and val:
                res.union(rid, cid)
    comp = 0
    print(res.parent)
    for idx, val in enumerate(res.parent):
        if idx == val:
            comp += 1
    return comp


isConnected = [[1, 1, 0], [1, 1, 0], [0, 0, 1]]
isConnected = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
isConnected = [[1, 0, 0, 1], [0, 1, 1, 0], [0, 1, 1, 1], [1, 0, 1, 1]]
print(find_number_of_provinces(isConnected))
