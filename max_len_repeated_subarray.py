def find_max_length_repeated_subarray(nums1, nums2):
    cache = [[0] * (len(nums2) + 1) for _ in range(len(nums1) + 1)]
    # print("*" * 80)
    # print("ironman cache", cache)
    max_val = 0
    for (
        idx,
        val1,
    ) in enumerate(nums1):
        for idx2, val2 in enumerate(nums2):
            if val1 == val2:
                cache[idx + 1][idx2 + 1] = cache[idx][idx2] + 1
                max_val = max(cache[idx + 1][idx2 + 1], max_val)
            # else:
            #     cache[idx + 1][idx2 + 1] = max(
            #         cache[idx][idx2 + 1], cache[idx + 1][idx2]
            #     )
    return max_val


nums1 = [1, 2, 3, 2, 1]
nums2 = [3, 2, 1, 4, 7]

nums1 = [0, 1, 1, 1, 1]
nums2 = [1, 0, 1, 0, 1]
# print(find_max_length_repeated_subarray([0, 0], [0, 0]))
print(find_max_length_repeated_subarray(nums1, nums2))
