


/** 
 * Your NumberContainers object will be instantiated and called as such:
 * var obj = new NumberContainers()
 * obj.change(index,number)
 * var param_2 = obj.find(number)
 */
class NumberContainers {
  constructor() {
    this.indexNums = {};
    this.numsToIndex = {}
  }

  change(index, number) {
    this.indexNums[index] = number;
    // add number to set
    // check if number exists in numsToIndex
    if (number in this.numsToIndex) {
        // add index to set
        this.numsToIndex[number].add(index);
        this.numsToIndex[number].
    } else {
        // else create a new set and add index
        this.numsToIndex[number] = new Set([index]);
    }
  }

  find(number) {
    // return value from set
    return number in this.numsToIndex ? Array.from(this.numsToIndex[number])[0] : -1;
    // return Object.keys(this.container).find((key) => this.container[key] === number) || -1;
  }
}