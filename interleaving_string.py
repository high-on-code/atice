def find_string_interleaving(str1, str2, str3):
    cache = [[True] * (len(str2) + 1) for _ in range(len(str1) + 1)]
    str1_len = len(str1)
    str2_len = len(str2)
    str3_len = len(str3)
    if str1_len + str2_len != str3_len:
        return False
    for idx1 in range(1, str1_len + 1):
        if str1[idx1 - 1] == str3[idx1 - 1]:
            cache[idx1][0] = cache[idx1 - 1][0]
        else:
            cache[idx1][0] = False
    for idx1 in range(1, str2_len + 1):
        if str2[idx1 - 1] == str3[idx1 - 1]:
            cache[0][idx1] = cache[0][idx1 - 1]
        else:
            cache[0][idx1] = False
    # print("*" * 80)
    # print("ironman cache", cache)
    for idx1 in range(0, len(str1)):
        for idx2 in range(0, len(str2)):
            idx3 = idx1 + idx2 + 1
            if str1[idx1] == str3[idx3] and cache[idx1][idx2 + 1]:
                # cache[idx1 + 1][idx2 + 1] = cache[idx1 + 1][idx2]
                cache[idx1 + 1][idx2 + 1] = True
            elif str2[idx2] == str3[idx3] and cache[idx1 + 1][idx2]:
                # cache[idx1 + 1][idx2 + 1] = cache[idx1][idx2 + 1]
                cache[idx1 + 1][idx2 + 1] = True
            else:
                cache[idx1 + 1][idx2 + 1] = False
    # print("*" * 80)
    # print("ironman cache", cache)
    return cache[-1][-1]


str1 = "a"
str2 = "b"
str3 = "ab"
# str3 = "ba"
# str1 = "ab"
# str2 = "c"
# str3 = "bac"
str1 = "aabcc"
str2 = "dbbca"
str3 = "aadbbcbcac"
print(find_string_interleaving(str1, str2, str3))
