def lca_helper(node, t1, t2):

    if not node:
        return False, None
    print('node.val', node.val)
    if node == t1 or node == t2:
        mid = True
    else:
        mid = False
    left, res = lca_helper(node.left, t1, t2)
    if res:
        return True, res
    right, res = lca_helper(node.right, t1, t2)
    if res:
        return True, res
    if mid + left + right >= 2:
        print('sol')
        return True, node
    return (left or right or mid), None
