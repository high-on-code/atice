def format_sol(nn, curr_sol):
    sol = []
    for idx in curr_sol:
        curr_row = ["."] * nn
        curr_row[idx] = "Q"
        sol.append(curr_row)
    return sol


def nqueens_recur(nn, row, cols, dia, andia, curr_sol):
    if row == nn:
        return 1
        # sol.append(curr_sol)
    res = 0
    for idx in range(nn):
        if idx in cols:
            continue
        if row - idx in dia:
            continue
        if row + idx in andia:
            continue
        res += nqueens_recur(
            nn,
            row + 1,
            cols | {idx},
            dia | {row - idx},
            andia | {row + idx},
            curr_sol + [idx],
        )
    return res


def nqueens(nn):
    return nqueens_recur(nn, 0, set(), set(), set(), [])


n = 4
print(nqueens(4))
