def find_min_cost_for_climbing_stairs(cost):
    if len(cost) == 1:
        return cost[0]
    p_value = 0
    pp_value = 0

    for idx in range(2, len(cost) + 1):
        val = min(p_value + cost[idx - 1], pp_value + cost[idx - 2])
        pp_value = p_value
        p_value = val
    return val


# cost = [10, 15, 20, 20]
# cost = [1, 100, 1, 1, 1, 100, 1, 1, 100, 1]
cost = [1, 2]
cost = [1]
print(find_min_cost_for_climbing_stairs(cost))
