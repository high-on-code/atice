from random import randrange


class Solution:
    def __init__(self, nums):
        self.arr = nums
        self.original = list(nums)

    def reset(self):
        self.arr = list(self.original)
        return self.arr

    def shuffle(self):
        arrlen = len(self.arr)
        for idx in range(arrlen):
            swap_idx = randrange(idx, arrlen)
            self.arr[idx], self.arr[swap_idx] = self.arr[swap_idx], self.arr[idx]
        return self.arr


nums = [1, 2, 3]
sl = Solution(nums)
print(sl.shuffle())
print(sl.shuffle())
print(sl.shuffle())
print(sl.shuffle())
print(sl.shuffle())
print(sl.shuffle())
