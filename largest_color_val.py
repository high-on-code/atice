from collections import defaultdict, deque
# function to prepare adjancency list


def make_graph(edges, node_count):
    # list to hold adjacency list
    graph = [[] for x in range(node_count)]
    for fr, to in edges:
        graph[fr].append(to)
    return graph

# function to calculate indegree for each node


def calculate_ideg(graph):
    ideg = [0] * len(graph)
    for node in range(len(graph)):
        for neighbor in graph[node]:
            ideg[neighbor] += 1
    return ideg


def create_2dlist(rows, cols):
    return [[0 for x in range(cols)] for y in range(rows)]


def find_largest_color_val(colors, edges):
    node_count = len(colors)
    graph = make_graph(edges, node_count)
    ideg = calculate_ideg(graph)
    count = create_2dlist(node_count, 26)
    # print(count)
    qu = deque()
    # add all nodes with indegree 0 to the queue
    for node in ideg:
        if ideg[node] == 0:
            qu.append(node)
    ans = 0
    nodes_seen = 0
    # do a bfs using qu
    while qu:
        node = qu.popleft()
        ans = max(ans, count[node][ord(colors[node]) - ord('a')] + 1)
        nodes_seen += 1
        for neighbor in graph[node]:
            ideg[neighbor] -= 1
            if ideg[neighbor] == 0:
                qu.append(neighbor)
            # iterate over 26 alphabets
            for i in range(26):
                count[neighbor][i] = max(count[node][i], count[neighbor][i])
    return ans if nodes_seen == node_count else -1


colors = "abaca"
edges = [[0, 1], [0, 2], [2, 3], [3, 4]]
print(find_largest_color_val(colors, edges))
