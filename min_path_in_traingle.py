infi = float("inf")


def find_min_path(triangle):
    rlen = len(triangle)
    for rid in range(1, rlen):
        min_val = infi
        for cid in range(rid + 1):
            if cid > 0:
                min_val = triangle[rid - 1][cid - 1]
            if cid < rid:
                min_val = min(min_val, triangle[rid - 1][cid])
            triangle[rid][cid] += min_val
    return min(triangle[-1])


triangle = [[2], [3, 4], [6, 5, 7], [4, 1, 8, 3]]
print(find_min_path(triangle))
