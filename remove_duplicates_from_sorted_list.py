def remove_duplicates(alist):
    if not alist:
        return alist
    dummy = ListNode()
    unique = dummy
    curr = alist
    # unique = dummy
    while curr.next:
        if curr.val == curr.next.val:
            repp = curr.val
            curr = curr.next
            while curr and repp == curr.val:
                curr = curr.next
            unique.next = curr
        else:
            unique.next = curr
            unique = unique.next
            curr = curr.next
    return dummy.next
