class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def partition_list(head, tre):
    before = before_head = ListNode()
    after = after_head = ListNode()

    curr_node = head
    while curr_node:
        if curr_node.val < tre:
            before.next = curr_node
            before = before.next
        else:
            after.next = curr_node
            after = after.next
        curr_node = curr_node.next
    after.next = None
    before.next = after_head.next
    return before_head.next

def print_list(head):
    curr_node = head
    while curr_node:
        print(curr_node.val)
        curr_node = curr_node.next