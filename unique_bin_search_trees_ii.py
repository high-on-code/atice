def generate_trees_helper(start, end):
    if start > end:
        return [None]
    trees = []
    for val in range(start, end + 1):
        left_trees = generate_trees_helper(start, val - 1)
        right_trees = generate_trees_helper(val + 1, end)
        for lt in left_trees:
            for rt in right_trees:
                root = TreeNode(val)
                root.left = lt
                root.right = rt
                trees.append(root)
    return trees


def generate_trees(n):
    return generate_trees_helper(1, n)
    return trees


n = 3
print(generate_trees(n))
