def add_one_row_recur(root, val, depth):
    if depth == 1:
        left = root.left
        right = root.right
        new_left = TreeNode(val)
        new_right = TreeNode(val)
        root.left = new_left
        root.right = new_right
        root.left.left = left
        root.right.right = right
        return
    add_one_row_recur(root.left, val, depth - 1)
    add_one_row_recur(root.right, val, depth - 1)


def add_one_row(root, val, depth):
    if depth == 1:
        new_node = TreeNode(val, root)
        return new_node
    add_one_row_recur(root, val, depth)
    return root
