class UnionFind:
    def __init__(self, grid):
        rlen = len(grid)
        clen = len(grid[0])
        grid_len = len(grid) * len(grid[0])
        self.sets, self.rank = [0] * grid_len, [0] * grid_len
        self.set_count = 0
        for rid, row in enumerate(grid):
            for cid, val in enumerate(row):
                if val == "1":
                    convert_idx = (rid * clen) + cid
                    self.set_count += 1
                    self.sets[convert_idx] = convert_idx

    def union(self, set1, set2):
        pset1 = self.find_parent(set1)
        pset2 = self.find_parent(set2)
        if pset1 == pset2:
            return
        if self.rank[pset1] >= self.rank[pset2]:
            self.sets[pset2] = pset1
            self.rank[pset1] += 1
        else:
            self.sets[pset1] = pset2
            self.rank[pset2] += 1
        self.set_count -= 1

    def find_parent(self, setn):
        if self.sets[setn] != setn:
            self.sets[setn] = self.find_parent(self.sets[setn])
        return self.sets[setn]


def find_number_of_islands(grid):
    uf = UnionFind(grid)
    four_4 = [(1, 0), (-1, 0), (0, 1), (0, -1)]
    rlen = len(grid)
    clen = len(grid[0])
    for rid, row in enumerate(grid):
        for cid, val in enumerate(row):
            if val == "1":
                # grid[rid][cid] = "0"
                for x, y in four_4:
                    crid, ccid = rid + x, cid + y
                    if (
                        0 <= crid < rlen
                        and 0 <= ccid < clen
                        and grid[crid][ccid] == "1"
                    ):
                        if val == "1":
                            uf.union(rid * clen + cid, crid * clen + ccid)
                            # print(rid, cid)
                            # print(crid, ccid)
                            # print(uf.sets)
    return uf.set_count


# grid = [
#     ["1", "1", "1", "1", "0"],
#     ["1", "1", "0", "1", "0"],
#     ["1", "1", "0", "0", "0"],
#     ["0", "0", "0", "0", "0"],
# ]
grid = [
    ["1", "1", "0", "0", "0"],
    ["1", "1", "0", "0", "0"],
    ["0", "0", "1", "0", "0"],
    ["0", "0", "0", "1", "1"],
]
grid = [["0"]]
grid = [["1"], ["1"]]
print(find_number_of_islands(grid))
