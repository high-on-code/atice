import sys

MAXINT = -sys.maxsize


def find_max_subarray(arr):
    rans = MAXINT
    ans = 0
    for val in arr:
        ans += val
        rans = max(ans, rans)
        if ans < 0:
            ans = 0
    return rans


def find_max_circular_subarray(arr):
    right_max = [0] * len(arr)
    right_max[-1] = arr[-1]
    arrlen = len(arr)
    suffix_sum = arr[-1]
    for idx in range(arrlen - 2, -1, -1):
        suffix_sum += arr[idx]
        right_max[idx] = max(suffix_sum, right_max[idx + 1])
    # print(right_max)
    special_sum = arr[0]
    prefix_sum = 0
    for idx in range(arrlen - 2):
        prefix_sum += arr[idx]
        special_sum = max(special_sum, prefix_sum + right_max[idx + 1])
    return max(special_sum, find_max_subarray(arr))


nums = [1, -2, 3, -2]
nums = [6, 9, -3]
print(find_max_circular_subarray(nums))
