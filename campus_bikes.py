# https://leetcode.com/problems/campus-bikes/
def assign_bikes(workers, bikes):
    worker_assigned = [False] * len(workers)
    bikes_assigned = [False] * len(bikes)
    assigned_bikes = []
    for wid, worker in enumerate(workers):
        x1, y1 = worker
        for bid, bike in enumerate(bikes):
            x2, y2 = bike
            man_dist = abs(x1 - x2) + abs(y1 - y2)
            assigned_bikes.append((man_dist, wid, bid))
    assigned_bikes.sort()
    print(assigned_bikes)
    res = [-1] * len(workers)
    for dist, wid, bid in assigned_bikes:
        if not worker_assigned[wid] and not bikes_assigned[bid]:
            worker_assigned[wid] = True
            bikes_assigned[bid] = True
            # res.append((wid, bid))
            res[bid] = wid
    return res


workers = [[0, 0], [2, 1]]
bikes = [[1, 2], [3, 3]]
workers = [[0, 0], [1, 1], [2, 0]]
bikes = [[1, 0], [2, 2], [2, 1]]
workers = [
    [240, 446],
    [745, 948],
    [345, 136],
    [341, 68],
    [990, 165],
    [165, 580],
    [133, 454],
    [113, 527],
]
bikes = [
    [696, 140],
    [95, 393],
    [935, 185],
    [767, 205],
    [387, 767],
    [214, 960],
    [804, 710],
    [956, 307],
]
print(assign_bikes(workers, bikes))
