# https://leetcode.com/problems/minimum-remove-to-make-valid-parentheses/
def find_min_paran_to_remove(strn):
    stack = []
    indexes_to_remove = set()
    for idx, ch in enumerate(strn):
        if ch == "(":
            stack.append(idx)
        elif ch == ")":
            if stack:
                stack.pop()
            else:
                indexes_to_remove.add(idx)
    indexes_to_remove |= set(stack)
    ch_list = []
    for idx, ch in enumerate(strn):
        if idx in indexes_to_remove:
            continue
        ch_list.append(ch)
    return "".join(ch_list)


strn = "lee(t(c)o)de)"
print(find_min_paran_to_remove(strn))
