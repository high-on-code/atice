def find_best_time_to_buy_sell_with_cooldown_recur(dayi, holding, sold, prices, cache):
    if dayi == len(prices):
        return 0
    if cache[dayi][holding][sold]:
        return cache[dayi][holding][sold]
    if holding:
        res = max(
            prices[dayi]
            + find_best_time_to_buy_sell_with_cooldown_recur(
                dayi + 1, False, True, prices, cache
            ),
            find_best_time_to_buy_sell_with_cooldown_recur(
                dayi + 1, True, False, prices, cache
            ),
        )

    else:
        if sold:
            res = find_best_time_to_buy_sell_with_cooldown_recur(
                dayi + 1, False, False, prices, cache
            )
        else:
            res = max(
                find_best_time_to_buy_sell_with_cooldown_recur(
                    dayi + 1, True, False, prices, cache
                )
                - prices[dayi],
                find_best_time_to_buy_sell_with_cooldown_recur(
                    dayi + 1, False, False, prices, cache
                ),
            )
    cache[dayi][holding][sold] = res
    return res


def find_best_time_to_buy_sell_with_cooldown(prices):
    cache = []
    for di in range(len(prices)):
        day_cache = [[0, 0], [0, 0]]
        cache.append(day_cache)
    return find_best_time_to_buy_sell_with_cooldown_recur(0, 0, 0, prices, cache)


prices = [1, 2, 3, 0, 2]
print(find_best_time_to_buy_sell_with_cooldown(prices))
