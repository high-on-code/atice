def min_arrows_to_burst_ballons(points):
    points.sort(key=lambda x: x[1])
    num_arrow = 1
    balloon_end = points[0][1]
    for st, ed in points[1:]:
        if st > balloon_end:
            num_arrow += 1
            balloon_end = ed
    return num_arrow
 

points = [[10, 16], [2, 8], [1, 6], [7, 12]]
points = [[1, 2], [3, 4], [5, 6], [7, 8]]
print(min_arrows_to_burst_ballons(points))
