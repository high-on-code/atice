class NumArray:
    def __init__(self, nums: List[int]):
        self.orig = nums
        self.range_tree = [0] * ((len(nums) * 2))
        self.orig_len = len(nums)
        num_len = len(nums)
        for idx in range(num_len):
            # print(idx, idx + num_len)
            self.range_tree[idx + num_len] = nums[idx]
        for idx in range(num_len - 1, 0, -1):
            self.range_tree[idx] = (
                self.range_tree[idx * 2] + self.range_tree[idx * 2 + 1]
            )

        # print(self.range_tree)

    def update(self, index: int, val: int) -> None:
        index += self.orig_len
        self.range_tree[index] = val
        while index > 0:
            left = index
            right = index
            if index % 2 == 0:
                right = index + 1
            else:
                left = index - 1
            index //= 2
            self.range_tree[index] = self.range_tree[left] + self.range_tree[right]

    def sumRange(self, left: int, right: int) -> int:
        left += self.orig_len
        right += self.orig_len
        res = 0
        while left <= right:
            if left % 2 == 1:
                res += self.range_tree[left]
                left += 1
            if right % 2 == 0:
                res += self.range_tree[right]
                right -= 1
            left //= 2
            right //= 2
        return res


# Your NumArray object will be instantiated and called as such:
# obj = NumArray(nums)
# obj.update(index,val)
# param_2 = obj.sumRange(left,right)
