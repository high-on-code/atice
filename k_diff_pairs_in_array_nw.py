# this must have worked
# check why it was not working
class Solution:
    def findPairs(self, nums: List[int], k: int) -> int:
        pairs = set()
        vals = set()
        for val in nums:

            # if (k - val) in vals:
            #     (a, b) = (val, k - val) if val > k - val else (k - val, val)
            #     pairs.add((a, b))

            if val - k in vals:
                (a, b) = (val, val - k) if val > val - k else (val - k, val)
                (a, b) = (val, val - k)
                pairs.add((a, b))
            elif val + k in vals:
                (a, b) = (val, val + k) if val > val + k else (val + k, val)
                (a, b) = (val, val + k)
                pairs.add((a, b))
            vals.add(val)
        print(pairs)
        return len(pairs)
