def find_best_time_to_buy_sell_recur(day, trans, holding, profit, prices, cache):
    if day == len(prices) or trans == 0:
        return profit
    elif cache[day][trans - 1][holding] is not None:
        return cache[day][trans - 1][holding]
    if holding:
        res = max(
            prices[day]
            + find_best_time_to_buy_sell_recur(
                day + 1, trans - 1, 0, profit, prices, cache
            ),
            find_best_time_to_buy_sell_recur(day + 1, trans, 1, profit, prices, cache),
        )
        cache[day][trans - 1][1] = res
        return res
    else:
        res = max(
            find_best_time_to_buy_sell_recur(day + 1, trans, 1, profit, prices, cache)
            - prices[day],
            find_best_time_to_buy_sell_recur(day + 1, trans, 0, profit, prices, cache),
        )
        cache[day][trans - 1][0] = res
    return res


def find_best_time_to_buy_sell(num_trans, prices):
    days = len(prices)
    cache = []
    for day in range(days):
        day_cache = []
        cache.append(day_cache)
        for tran in range(num_trans):
            tran_cache = [None, None]
            day_cache.append(tran_cache)

    return find_best_time_to_buy_sell_recur(0, num_trans, 0, 0, prices, cache)


num_trans = 2
prices = [2, 4, 1]

num_trans = 2
prices = [3, 2, 6, 5, 0, 3]

num_trans = 1
prices = [2, 6]

num_trans = 1
prices = [2, 6, 9]
num_trans = 2
prices = [2, 6, 9]

num_trans = 2
prices = [2, 6, 9, 4]

print(find_best_time_to_buy_sell(num_trans, prices))
