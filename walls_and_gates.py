from collections import deque


def find_closest_gate(rooms):
    fourd = [(1, 0), (-1, 0), (0, 1), (0, -1)]
    rlen = len(rooms)
    clen = len(rooms[0])
    INF = (2 ** 31) - 1
    gates = []
    for rid, row in enumerate(rooms):
        for cid, val in enumerate(row):
            if val == 0:
                gates.append((rid, cid))
    if not gates:
        return rooms
    gate_len = len(gates)
    # (Notice) (Better) instead of adding multiple queues it would have been enough to add all gates into one queue
    qus = [deque() for _ in range(gate_len)]
    for idx, gate in enumerate(gates):
        qus[idx].append(gate)
    dist = 0
    while True:
        dist += 1
        inf_seen = False
        for idx, aq in enumerate(qus):
            wall_len = len(aq)
            while wall_len:
                nidr, nidc = aq.popleft()
                for dx, dy in fourd:
                    cx, cy = nidr + dx, nidc + dy
                    if 0 <= cx < rlen and 0 <= cy < clen:
                        if rooms[cx][cy] == INF:
                            inf_seen = True
                            rooms[cx][cy] = dist
                            aq.append((cx, cy))
                wall_len -= 1
            if idx == gate_len - 1 and not inf_seen:
                # break
                return rooms


# rooms = [
#     [2147483647, -1, 0, 2147483647],
#     [2147483647, 2147483647, 2147483647, -1],
#     [2147483647, -1, 2147483647, -1],
#     [0, -1, 2147483647, 2147483647],
# ]
# rooms = [[-1]]
rooms = [
    [
        -1,
        2147483647,
        0,
        2147483647,
        -1,
        2147483647,
        -1,
        -1,
        -1,
        0,
        0,
        2147483647,
        -1,
        0,
        0,
        0,
        0,
        0,
        -1,
        0,
        0,
        2147483647,
        0,
        2147483647,
        2147483647,
        -1,
        2147483647,
        -1,
        2147483647,
        -1,
        -1,
        -1,
        0,
    ]
]
print(find_closest_gate(rooms))
