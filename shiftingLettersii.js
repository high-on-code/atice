function getResCh(ch, diff) {
    diff %= 26; // handling negative
    if (diff < 0) {
        diff += 26
    }
    // y + 24
    ch = ((ch.charCodeAt(0) - ('a'.charCodeAt(0))  + diff) % 26) + 'a'.charCodeAt(0)
    // ch = ((ch.charCodeAt(0)  + diff) % 26 ) + 'a'.charCodeAt(0)
    return String.fromCharCode(ch)
}
var shiftingLetters = function(s, shifts) {
    const strLen = s.length;
    const diffArr = new Array(strLen).fill(0)
    for (aShift of shifts) {
        let [st, ed, dir] = aShift;
        dir = dir === 0?-1:1;
        diffArr[st] += dir;
        if (ed < strLen - 1) {
            diffArr[ed + 1] += -dir;
        }
    }
    let acc = 0
    const res = Array.from(s)
    for (let idx = 0; idx < strLen; idx ++) {
        acc += diffArr[idx]
        res[idx] = getResCh(s[idx], acc)
    }
    return res.join("")
};

const s = "abc"
const shifts = [[0,1,0],[1,2,1],[0,2,1]]
console.log(shiftingLetters(s, shifts))

console.log(getResCh('a', -2))
console.log(getResCh('y', 12))
console.log(getResCh('d', 2))
console.log(getResCh('f', -2))
