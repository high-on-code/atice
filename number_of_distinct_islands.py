def find_number_of_distinct_islands(grid):
    seen = set()

    def dfs(rid, cid, path):
        for dx, dy, direct in ((0, 1, 'R'), (1, 0, 'D'), (-1, 0, 'U'), (0, -1,
                                                                        'L')):
            cx, cy = rid + dx, cid + dy
            if 0 <= cx < len(grid) and 0 <= cy < len(
                    grid[0]) and (cx, cy) not in seen and grid[cx][cy]:
                seen.add((cx, cy))
                path.append(direct)
                dfs(
                    cx,
                    cy,
                    path,
                )
                path.append('0')

    unique_islands = set()
    for rid, row in enumerate(grid):
        for cid, val in enumerate(row):
            if val and (rid, cid) not in seen:
                path = ['0']
                dfs(rid, cid, path)
                unique_islands.add(tuple(path))
    return len(unique_islands)


grid = [[1, 1, 0, 0, 0], [1, 1, 0, 0, 0], [0, 0, 0, 1, 1], [0, 0, 0, 1, 1]]
grid = [[1, 1, 0, 1, 1], [1, 0, 0, 0, 0], [0, 0, 0, 0, 1], [1, 1, 0, 1, 1]]
print(find_number_of_distinct_islands(grid))