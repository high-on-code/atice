def rotate_array(nums, step):
    num_len = len(nums)
    start = 0
    curr_idx = 0
    curr_val = nums[0]
    for idx in range(len(nums)):
        next_idx = (curr_idx + step) % num_len
        next_val = nums[next_idx]
        nums[next_idx] = curr_val
        curr_val = next_val
        curr_idx = next_idx
        if curr_idx == start:
            curr_idx += 1
            curr_val = nums[curr_idx]
            start += 1
    return nums


# nums = [1, 2, 3]
# step = 2
nums = [-1, -100, 3, 99]
step = 2
print(rotate_array(nums, step))
