from functools import lru_cache
import math


@lru_cache(maxsize=None)
def find_stone_solution(remaining):
    if remaining == 0:
        return False
    upper_limit = math.floor(math.sqrt(remaining))

    for val in range(1, upper_limit + 1):
        if not find_stone_solution(remaining - (val ** 2)):
            return True
    else:
        return False


print(find_stone_solution(1))
