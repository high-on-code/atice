def find_subsets(nums):
    res = [[]]
    for val in nums:
        res += [curr + [val] for curr in res]
    return res


nums = [1, 2, 3]
print(find_subsets(nums))
