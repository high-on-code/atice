# https://leetcode.com/problems/minimum-path-sum/
import sys

INFI = sys.maxsize


def find_min_path_sum(grid):
    rlen = len(grid)
    clen = len(grid[0])
    for cr in range(rlen):
        for cc in range(clen):
            if cr == cc == 0:
                continue
            curr = INFI
            if cr > 0:
                curr = min(curr, grid[cr - 1][cc])
            if cc > 0:
                curr = min(curr, grid[cr][cc - 1])
            grid[cr][cc] += curr
    return grid[-1][-1]


grid = [[1, 3, 1], [1, 5, 1], [4, 2, 1]]
grid = [[1, 2, 3], [4, 5, 6]]
print(find_min_path_sum(grid))
