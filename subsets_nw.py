def find_subsets(nums):
    num_len = len(nums)
    res = [[]]
    for idx in range(num_len):
        # temp = [num_len[idx]]
        # res.append(temp)
        for idx2 in range(idx, num_len):
            res.append(nums[idx : idx2 + 1])
    return res


nums = [1, 2, 3]
print(find_subsets(nums))
