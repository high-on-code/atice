from collections import deque
class MyStack:

    def __init__(self):
        self.q1 = deque()
        self.q2 = deque()
        self.topi = None
    def push(self, x: int) -> None:
        self.topi = x
        self.q1.append(x)

    def pop(self) -> int:
        while len(self.q1) > 1:
            self.topi = self.q1.popleft()
            self.q2.append(self.topi)
        res = self.q1.popleft()
        self.q1, self.q2 = self.q2, self.q1 
        return res

    def top(self) -> int:
        return self.topi

    def empty(self) -> bool:
        return len(self.q1) == 0
    

obj1 = MyStack()
obj1.push(1)
obj1.push(2)
print(obj1.top())
print(obj1.pop())
print(obj1.pop())
