# https://leetcode.com/problems/maximum-number-of-events-that-can-be-attended-ii/
import bisect
def find_max_num_events_recur(idx, events, count, dp, starts):
    if idx == len(events) or count == 0:
        return 0
    if dp.get((idx, count)):
        return dp[(idx, count)]
    
    next_idx = bisect.bisect_right(starts, events[idx][1])
    dp[(idx, count)] = max(find_max_num_events_recur(idx + 1, events, count, dp, starts), find_max_num_events_recur(next_idx, events, count - 1, dp, starts) + events[idx][2])
    return dp[(idx, count)]
def find_max_num_events(events, count):
    dp = {}
    events.sort(key=lambda x: x[0])
    starts = [event[0] for event in events]
    return find_max_num_events_recur(0, events, count, dp, starts)


events = [[1,2,4],[3,4,3],[2,3,1]]
k = 2
print(find_max_num_events(events, k))