def word_to_bitset(word):
    bitset = 0
    for ch in word:
        word_num = ord(ch) - ord("a")
        ch_bitset = 1 << word_num
        if ch_bitset & bitset:
            return None
        bitset |= ch_bitset
    return bitset | (len(word) << 26)


# print(word_to_bitset("a"))
# print(word_to_bitset("ab"))
# print(word_to_bitset("abc"))
def dfs(opt_arr, pos, res):
    old_res = res & ((1 << 26) - 1)
    old_len = res >> 26
    best = old_len

    for idx in range(pos, len(opt_arr)):
        new_chars = opt_arr[idx] & ((1 << 26) - 1)
        new_len = opt_arr[idx] >> 26

        if new_chars & old_res:
            continue
        new_res = new_chars + old_res + ((new_len + old_len) << 26)
        best = max(best, dfs(opt_arr, idx + 1, new_res))
    return best


def find_length_of_concat_str(arr):
    res_set = set()
    for aw in arr:
        bitset = word_to_bitset(aw)
        if bitset:
            res_set.add(bitset)
    opt_arr = list(res_set)
    return dfs(opt_arr, 0, 0)


arr = ["un", "iq", "ue"]

print(find_length_of_concat_str(arr))
