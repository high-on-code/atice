from collections import defaultdict
from functools import cache


def deleteAndEarn(nums):
    points = defaultdict(int)
    max_number = 0
    # Precompute how many points we gain from taking an element
    for num in nums:
        points[num] += num
        max_number = max(max_number, num)

    @cache
    def max_points(num):
        # Check for base cases
        if num == 0:
            return 0
        if num == 1:
            return points[1]

        # Apply recurrence relation
        point_1 = max_points(num - 1)
        point_2 = max_points(num - 2)
        return max(point_1, point_2 + points[num])

    return max_points(max_number)


nums = [3, 4, 2]
print(deleteAndEarn(nums))
