def find_number_of_palindromic_substrings(strn):
    strn_len = len(strn)
    res = strn_len
    cache = [[False] * strn_len for _ in range(strn_len)]
    for idx in range(strn_len):
        cache[idx][idx] = True
    for idx in range(strn_len - 1):
        cache[idx][idx + 1] = True if strn[idx] == strn[idx + 1] else False
        if cache[idx][idx + 1]:
            res += 1
    for sub_len in range(3, strn_len + 1):
        for idx in range(0, strn_len - sub_len + 1):
            idx2 = idx + sub_len - 1
            cache[idx][idx2] = (
                True if strn[idx] == strn[idx2] and cache[idx + 1][idx2 - 1] else False
            )
            if cache[idx][idx2]:
                res += 1
    print(cache)
    return res


strn = "abba"
print(find_number_of_palindromic_substrings(strn))
