def get_idx(ch):
    return ord(ch) - ord('a')
def find_klen_substrings_without_repeated_chars(strn, klen):
    freq_arr = [0] * 26
    left, right = 0, 0
