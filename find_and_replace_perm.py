# https://leetcode.com/problems/find-and-replace-pattern/
def find_and_replace(words, pattern):
    res = []
    for aword in words:
        if len(aword) != len(pattern):
            continue
        mapp = {}
        perm = True
        for c1, c2 in zip(aword, pattern):
            if c1 in mapp and mapp[c1] != c2:
                perm = False
                break
            else:
                mapp[c1] = c2
        if perm and len(mapp) == len(set(mapp.values())):
            res.append(aword)
    return res


words = ["abc", "deq", "mee", "aqq", "dkd", "ccc"]
pattern = "abb"
print(find_and_replace(words, pattern))
