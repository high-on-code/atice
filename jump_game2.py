import sys

inf = sys.maxsize


def find_min_jumps_recur(nums, st_idx, cache):
    if st_idx >= len(nums) - 1:
        return 0
    # if st_idx in cache:
    #     return cache[st_idx]
    res = inf
    for idx in range(1, nums[st_idx] + 1):
        res = min(res, find_min_jumps_recur(nums, st_idx + idx, cache))
    # cache[st_idx] = res
    return res + 1


def find_min_jumps(nums):
    cache = {}
    return find_min_jumps_recur(nums, 0, cache)


nums = [2, 3, 1, 1, 4]
# nums = [1]
# nums = [1, 2]
# nums = [1, 1, 2]

print(find_min_jumps(nums))
