def copy_random_list(head):
    visited = {}
    if not head:
        return head
    new_node = Node(head.val)
    visited[head] = new_node
    curr = head
    while curr:
        if curr.next and curr.next not in visited:
            new_new_node = Node(curr.next.val)
            visited[curr.next] = new_new_node
        new_node.next = visited.get(curr.next)
        new_new_node = None
        if curr.random and curr.random not in visited:
            new_new_node = Node(curr.random.val)
            visited[curr.random] = new_new_node
        new_node.random = visited.get(curr.random)
        curr = curr.next
        new_node = new_node.next
    return visited[head]
