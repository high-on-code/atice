class PeekingIterator:
    def __init__(self, iterator):
        self._next = iterator.next()
        self._iterator = iterator

    def peek(self):
        # print(self._next)
        return self._next

    def next(self):
        if self._next is None:
            raise StopIteration()
        ret = self._next
        self._next = None
        if self._iterator.hasNext():
            self._next = self._iterator.next()
        # print(ret)
        return ret

    def hasNext(self):
        # print(self._iterator.hasNext())
        return self._next is not None
