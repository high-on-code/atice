def remove_k_digits(num, k):
    if k == 0:
        return num
    stack = [num[0]]
    for idx in range(1, len(num)):
        while k and stack and int(stack[-1]) > int(num[idx]):
            stack.pop()
            k -= 1
            if k == 0:
                return ("".join(stack) + num[idx:]).lstrip("0") or "0"
        stack.append(num[idx])

    if k != 0:
        return "".join(stack[: len(stack) - k]).lstrip("0") or "0"


num = "1234567890"
k = 3
num = "1432219"
k = 3
num = "10200"
k = 1
print(remove_k_digits(num, k))
