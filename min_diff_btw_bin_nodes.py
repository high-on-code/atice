import sys

MAXVAL = sys.maxsize


def min_diff_nodes_recur(root, res):
    if not root:
        return res, -MAXVAL
    res, prev = min_diff_nodes_recur(root.left)
    min_diff_nodes_recur(root.right, res)
    return min(res, root.val - prev), root.val


def min_diff_nodes(root):
    return min_diff_nodes_recur(root, MAXVAL)
