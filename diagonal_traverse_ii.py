def traverse_diagonal(nums):
    result = []
    for i in range(len(nums)):
        for j in range(len(nums[i])):
            if i + j == len(nums) - 1:
                result.append(nums[i][j])
    return result