from collections import defaultdict, deque

def find_shortest_path_to_all_keys(grid):
    rows = len(grid)
    cols = len(grid[0])
    seen = defaultdict(set)
    key_set, lock_set = set(), set()
    all_keys = 0
    start_r, start_c = -1, -1
    for rid in range(rows):
        for cid in range(cols):
            cell = grid[rid][cid]
            if cell in "abcdef":
                all_keys += (1 << (ord(cell) - ord('a')))
                key_set.add(cell)
            elif cell in "ABCDEF":
                lock_set.add(cell)
            elif cell == "@":
                start_c, start_r = cid, rid
    qu = deque()
    qu.append((start_r, start_c, 0, 0))
    seen[0].add((start_r, start_c))