def reverse_bits(n):
    ret, powe = 0, 31
    while n:
        ret += (n & 1) << powe
        n >>= 1
        powe -= 1
    return ret


print(reverse_bits(1))
print(reverse_bits(2))
print(reverse_bits(3))
