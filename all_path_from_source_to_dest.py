# https://leetcode.com/problems/all-paths-from-source-lead-to-destination/
from collections import defaultdict, deque

# 0: white
# 1: gray
# 2: black
def check_if_all_paths_from_src_to_dest_recur(graph, colors, source, destination):
    if colors[source] == 1:
        return False
    if source != destination and source not in graph:
        return False
    colors[source] = 1
    for cdest in graph[source]:
        if not check_if_all_paths_from_src_to_dest_recur(
            graph, colors, cdest, destination
        ):
            return False
    colors[source] = 2
    return True


def check_if_all_paths_from_src_to_dest(n, edges, source, destination):
    graph = defaultdict(list)
    colors = [0] * n
    for st, ed in edges:
        graph[st].append(ed)
    return check_if_all_paths_from_src_to_dest_recur(graph, colors, source, destination)


n = 3
edges = [[0, 1], [0, 2]]
source = 0
destination = 2
n = 4
edges = [[0, 1], [0, 3], [1, 2], [2, 1]]
source = 0
destination = 3

n = 4
edges = [[0, 1], [0, 2], [1, 3], [2, 3]]
source = 0
destination = 3
print(check_if_all_paths_from_src_to_dest(n, edges, source, destination))
