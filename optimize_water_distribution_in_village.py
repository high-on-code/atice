class UnionFind:
    def __init__(self, num_verts):
        self.group = [idx for idx in range(num_verts)]
        self.rank = [0] * num_verts

    def find(self, node):
        if self.group[node] != node:
            self.group[node] = self.find(self.group[node])
        return self.group[node]

    def union(self, n1, n2):
        pn1 = self.find(n1)
        pn2 = self.find(n2)
        if pn1 == pn2:
            return False
        else:
            if self.rank[pn1] > self.rank[pn2]:
                self.group[pn2] = pn1
            elif self.rank[pn1] < self.rank[pn2]:
                self.group[pn1] = pn2
            else:
                self.group[pn1] = pn2
                self.rank[pn2] += 1
            return True


def optimize_water_dist(num_houses, wells, pipes):
    sorted_edges = []
    for idx, weight in enumerate(wells):
        sorted_edges.append((weight, 0, idx + 1))
    for h1, h2, wg in pipes:
        sorted_edges.append((wg, h1, h2))
    sorted_edges.sort()
    print(sorted_edges)
    res = 0
    uf = UnionFind(num_houses + 1)
    for cost, h1, h2 in sorted_edges:
        if uf.union(h1, h2):
            res += cost
    return res


n = 3
wells = [1, 2, 2]
pipes = [[1, 2, 1], [2, 3, 1]]
print(optimize_water_dist(n, wells, pipes))
