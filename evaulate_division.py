from collections import defaultdict


def backtrack(graph, visited, source, target, prod):
    visited.add(source)
    ret = -1.0
    neis = graph[source]
    if target in neis:
        ret = prod * neis[target]
    else:
        for anei, value in neis.items():
            if anei in visited:
                continue
            ret = backtrack(graph, visited, anei, target, prod * value)
            if ret != -1.0:
                break
    visited.remove(source)
    return ret


def evaluate_division(equations, values, queries):
    graph = defaultdict(defaultdict)
    for (div, divs), value in zip(equations, values):
        graph[div][divs] = value
        graph[divs][div] = 1 / value

    results = []
    for div, divs in queries:
        if div not in graph or divs not in graph:
            ret = -1.0
        elif div == divs:
            ret = 1.0
        else:
            visited = set()
            ret = backtrack(graph, visited, div, divs, 1)
        results.append(ret)
    return results


equations = [["a", "b"], ["b", "c"]]
values = [2.0, 3.0]
queries = [["a", "c"], ["b", "a"], ["a", "e"], ["a", "a"], ["x", "x"]]
print(evaluate_division(equations, values, queries))
