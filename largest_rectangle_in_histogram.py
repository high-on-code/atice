def find_largest_rectangle_in_histogram(heights):
    if not heights:
        return None
    heights.append(0)
    stack = [0]
    max_area = 0
    for idx in range(1, len(heights)):
        if heights[stack[-1]] > heights[idx]:
            while stack and heights[stack[-1]] > heights[idx]:
                a_height_idx = stack.pop()
                a_height = heights[a_height_idx]
                if not stack:
                    width = (idx)
                else:
                    width = (idx - stack[-1] - 1)
                current_area = a_height * width
                max_area = max(max_area, current_area)
            stack.append(idx)

        else:
            stack.append(idx)
    return max_area


heights = [2, 1, 5, 6, 2, 3]
heights = [4,2,0,3,2,5]
heights = [2,1,5,6,2,3]
print(find_largest_rectangle_in_histogram(heights))
