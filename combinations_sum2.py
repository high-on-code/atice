def combination_sum_recur(candidates, target, path, idx, res):
    if idx < len(candidates) and idx > 0 and candidates[idx - 1] == candidates[idx]:
        combination_sum_recur(candidates, target, path, idx + 1, res)
    if target < 0:
        return
    if target == 0 and idx == len(candidates):
        res.append(path)
    if idx == len(candidates):
        return
    combination_sum_recur(candidates, target - candidates[idx], path + [candidates[idx]], idx + 1, res)
    combination_sum_recur(candidates, target, path, idx + 1, res)
def combination_sum(candidates, target):
    candidates.sort()
    res = []
    combination_sum_recur(candidates, target, [], 0, res)
    return res

candidates = [10,1,2,7,6,1,5]
target = 8
print(combination_sum(candidates, target))