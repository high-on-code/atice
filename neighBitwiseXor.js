var doesValidArrayExist = function(derived) {
    // initialize array to store original
    const original = new Array(derived.length).fill(0);
    console.log('ironman original.length', JSON.stringify(original.length));
    // compute original using original[i + 1] = derived[i] ^ original[i]
    for (let i = 0; i < derived.length - 1; i++) {
        original[i + 1] = derived[i] ^ original[i];
    }
    console.log('ironman original.length', JSON.stringify(original.length));
    console.log('ironman original', JSON.stringify(original));
// check if original[0] XOR original[derived.length - 1] is derived[derived.length - 1]
    if  (original[0] ^ original[derived.length - 1] === derived[derived.length - 1]) {
        return true;
    } else {
        return false;
    }
}

let derived = [1,1,0];
derived = [1,1]
derived = [1,0]
console.log(doesValidArrayExist(derived));