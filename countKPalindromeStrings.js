/**
 * @param {string} s
 * @param {number} k
 * @return {boolean}
 */
function countChars(s) {
    const charCount = new Array(26).fill(0)
    for (ch of s) {
        charCount[ch.charCodeAt(0) - 'a'.charCodeAt(0)] += 1
    }
    return charCount;
}
var canConstruct = function(s, k) {
    const slen = s.length;
    if (k > slen) {
        return false;
    }
    const charCount = countChars(s)
    const oddCount = charCount.reduce((acc, count) => {
        if (count % 2 == 1) {
             acc += 1;
        }
        return acac
    }, 0)
    // const oddCount = charCount.reduce((count, init) => count + (init % 2), 0)
    console.log('ironman oddCount', JSON.stringify(oddCount));
    if (oddCount  > k) {
        return false
    } else {
        return true
    }
};
let s = "annabelle"
let k = 2
// s = "leetcode"
// k = 3
console.log(canConstruct(s, k))