def check_valid_parantheses(paran):
    par_map = {"}": "{", ")": "(", "]": "["}
    stack = []
    for ch in paran:
        if ch in ")]}":
            if not stack or par_map[ch] != stack.pop():
                return False
        else:
            stack.append(ch)
    return not stack


paran = "()"
print(check_valid_parantheses(paran))
