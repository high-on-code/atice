def max_val_of_coins_recur(piles, curr_coins, curr_pile, cache):
    if curr_pile == 0:
        return 0
    if cache[curr_pile][curr_coins] != -1:
        return cache[curr_pile][curr_coins]
    curr_sum = 0
    cache[curr_pile][curr_coins] = max_val_of_coins_recur(piles, curr_coins, curr_pile - 1, cache)
    for ccount in range(1, min(curr_coins, len(piles[curr_pile - 1])) + 1):
        curr_sum += piles[curr_pile - 1][ccount - 1]
        cache[curr_pile][curr_coins] = max(cache[curr_pile][curr_coins],
                                           max_val_of_coins_recur(piles, curr_coins - ccount, curr_pile - 1, cache) + curr_sum)
    return cache[curr_pile][curr_coins]

def max_val_of_coins(piles, target_coins):
    num_piles = len(piles)
    cache = [[-1 for _ in range(target_coins + 1)] for _ in range(num_piles + 1)]
    return max_val_of_coins_recur(piles, target_coins, num_piles, cache)
piles = [[1,100,3],[7,8,9]]
k = 2
print(max_val_of_coins(piles, k))