MOD = 10 ** 9 + 7

def find_num_ways_to_stay_in_place_recur(remain, arrlen, curr, mem):
    if remain == 0:
        if curr == 0:
            return 1
        else:
            return 0
    remidx = remain - 1
    if mem[curr][remidx] != -1:
        return mem[curr][remidx]
    mem[curr][remidx] = find_num_ways_to_stay_in_place_recur(
        remain - 1, arrlen, curr,  mem
    ) % MOD
    if curr > 0:
        mem[curr][remidx] += find_num_ways_to_stay_in_place_recur(
            remain - 1, arrlen, curr - 1,  mem
        ) % MOD
    if curr < arrlen - 1:
        mem[curr][remidx] += find_num_ways_to_stay_in_place_recur(
            remain - 1, arrlen, curr + 1,  mem
        ) % MOD
    return mem[curr][remidx]
def find_num_ways_to_stay_in_place(steps, arrlen):
    arrlen = min(arrlen, steps)
    mem = [[-1] * steps for _ in range(arrlen)]
    return find_num_ways_to_stay_in_place_recur(steps, arrlen, 0, mem)

steps = 3
arrLen = 2
steps = 2
arrLen = 4
print(find_num_ways_to_stay_in_place(steps, arrLen))