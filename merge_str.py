def merge_strings_alternatively(s1, s2):
    s1len = len(s1)
    s2len = len(s2)
    idx1, idx2 = 0, 0
    res = []
    while True:
        if idx1 < s1len:
            res.append(s1[idx1])
            idx1 += 1
        if idx2 < s2len:
            res.append(s2[idx2])
            idx2 += 1
        if idx1 == s1len and idx2 == s2len:
            break
    return ''.join(res)
word1 = "abc"
word2 = "pqr"
print(merge_strings_alternatively(word1, word2))
