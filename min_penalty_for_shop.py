def find_min_penalty(customers):
    hour_close  = 0
    min_penalty = 0
    for val in customers:
        if val == 'Y':
            min_penalty += 1
    curr_penalty = min_penalty
    for idx, val in enumerate(customers):
        if val == 'Y':
            curr_penalty -= 1
        else:
            curr_penalty += 1
        if curr_penalty < min_penalty:
            min_penalty = curr_penalty
            hour_close = idx + 1
    return hour_close


customers = "YYNY"
customers = "NNNNN"
print(find_min_penalty(customers))