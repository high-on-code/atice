def count_vowel_strings_backtrack(count, res, idx):
    if idx == count:
        res += 1
    for ch in "aeiou":
        count_vowel_strings_backtrack()


def count_vowel_strings(count):
    res = []
    return count_vowel_strings_backtrack(count, res, 0)
