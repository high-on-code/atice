import math


def find_k_closest(points, k):
    dist_point_map = []
    for x, y in points:
        point_dist = math.sqrt(x ** 2 + y ** 2)
        dist_point_map.append([point_dist, (x, y)])
    dist_point_map.sort()
    return [val[1] for val in dist_point_map[:k]]


points = [[1, 3], [-2, 2]]
k = 1
print(find_k_closest(points, k))
