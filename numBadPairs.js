/**
 * @param {number[]} nums
 * @return {number}
 */
var countBadPairs = function(nums) {
    const idxValCount = {};
    let badPairs = 0;
    for (let i = 0; i < nums.length; i++) {
        const idxVal = i - nums[i];
        // pattern adding previous accumulation 
        let goodPairs = idxValCount[idxVal] || 0;
        badPairs += i - goodPairs;
        idxValCount[idxVal] = (idxValCount[idxVal] || 0) + 1;
    }
    return badPairs;
};

let nums = [4,1,3,3]
nums = [1,2,3,4,5]
console.log(countBadPairs(nums)) // 1