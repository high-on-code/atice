def num_lasers_in_bank(bank):
    prev, ans = 0, 0
    for row in bank:
        count = 0
        for ch in row:
            if ch == '1':
                count += 1
        # count = sum(row)
        if count:
            ans += prev * count
            prev = count
    return ans

bank = ["000","111","000"]
bank = ["011001","000000","010100","001000"]
print(num_lasers_in_bank(bank))