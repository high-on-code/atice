from collections import defaultdict
def maximal_network_rank(cities, roads):
    adjm = defaultdict(set)
    for st, ed in roads:
        adjm[st].add(ed)
        adjm[ed].add(st)
    res = 0
    # sorted_cities = sorted(adjm, key=lambda x: len(adjm[x]), reverse=True)
    # print(sorted_cities)
    # print(adjm)
    # return len(adjm[sorted_cities[0]]) + len(adjm[sorted_cities[1]] ) - 1 if sorted_cities[1] in adjm[sorted_cities[0]] else 0
    for sr in adjm:
        for ed in adjm:
            if sr == ed:
                continue
            res = max(res, len(adjm[sr]) + len(adjm[ed]) - (1 if sr in adjm[ed] else 0))
    return res
n = 4
roads = [[0,1],[0,3],[1,2],[1,3]]
n = 8
roads = [[0,1],[1,2],[2,3],[2,4],[5,6],[5,7]]

print(maximal_network_rank(n, roads))