def find_maximal_square(matrix):
    rlen = len(matrix)
    clen = len(matrix[0])

    arow = [int(i) for i in matrix[0]]
    max_sqr = 1 if any(arow) else 0

    for rid in range(1, rlen):
        prev = arow[0]
        arow[0] = int(matrix[rid][0])
        max_sqr = max(max_sqr, 1 if any(arow) else 0)
        for cid in range(1, clen):
            if matrix[rid][cid] == "1":
                temp = arow[cid]
                arow[cid] = 1 + min(prev, arow[cid - 1], arow[cid])
                prev = temp
                if arow[cid] > max_sqr:
                    max_sqr = arow[cid]
            else:
                arow[cid] = 0
    return max_sqr ** 2


matrix = [
    ["1", "0", "1", "0", "0"],
    ["1", "0", "1", "1", "1"],
    ["1", "1", "1", "1", "1"],
    ["1", "0", "0", "1", "0"],
]

print(find_maximal_square(matrix))
