import heapq


def find_max_units_on_truck(box_types, truck_size):
    box_types = [[-i, j] for j, i in box_types]
    # print("*" * 80)
    # print("ironman box_types", box_types)
    heapq.heapify(box_types)
    # print("*" * 80)
    # print("ironman box_types", box_types)
    res = 0
    while truck_size and box_types:
        curr_box_type = heapq.heappop(box_types)
        boxes_to_load = min(truck_size, curr_box_type[1])
        res -= boxes_to_load * curr_box_type[0]
        curr_box_type[1] -= boxes_to_load
        truck_size -= boxes_to_load
        if curr_box_type[1] > 0:
            heapq.heappush(box_types, curr_box_type)
    return res


boxTypes = [[1, 3], [2, 2], [3, 1]]
truckSize = 4
print(find_max_units_on_truck(boxTypes, truckSize))
