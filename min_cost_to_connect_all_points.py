class UnionFind:
    def __init__(self, size):
        self.group = [idx for idx in range(size)]
        self.rank = [0] * size

    def find_group(self, node):
        if self.group[node] != node:
            self.group[node] = self.find_group(self.group[node])
        return node

    def join(self, node1: int, node2: int) -> bool:
        group1 = self.find(node1)
        group2 = self.find(node2)

        # node1 and node2 already belong to same group.
        if group1 == group2:
            return False

        if self.rank[group1] > self.rank[group2]:
            self.group[group2] = group1
        elif self.rank[group1] < self.rank[group2]:
            self.group[group1] = group2
        else:
            self.group[group1] = group2
            self.rank[group2] += 1

        return True


def min_cost_to_connect_all_points(points):
    print("*" * 80)
    print("ironman points", points)
    all_edges = []
    for idx, node in enumerate(points):
        for idx2, node2 in enumerate(points[idx + 1 :]):
            weight = abs(node[0] - node2[0]) + abs(node[1] - node2[1])
            all_edges.append((weight, idx, idx2 + idx + 1))
    print("*" * 80)
    print("ironman all_edges", all_edges)
    all_edges.sort()


points = [[0, 0], [2, 2], [3, 10], [5, 2], [7, 0]]
print(min_cost_to_connect_all_points(points))
