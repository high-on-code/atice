from collections import deque


def populate_next_right_pointer(root):
    if not root:
        return root
    qu = deque()
    qu.append(root)
    while qu:
        wall_len = len(qu)
        prev = None
        while wall_len:
            node = qu.popleft()
            wall_len -= 1
            if prev:
                prev.next = node
            if node.left:
                qu.append(node.left)
            if node.right:
                qu.append(node.right)
            prev = node
    return root
