class UnionFind:
    def __init__(self, lenn):
        self.groups = [i for i in range(lenn)]
        self.rank = [0] * lenn

    def find(self, val):
        # print('*' * 80)
        # print('ironman val', val)
        if self.groups[val] != val:
            self.groups[val] = self.find(self.groups[val])
        return self.groups[val]

    def union(self, val1, val2):
        val1_group = self.find(val1)
        val2_group = self.find(val2)
        friends_now = False
        if val1_group != val2_group:
            friends_now = True
            if self.rank[val1_group] > self.rank[val2_group]:
                self.groups[val2_group] = val1_group
                self.rank[val1_group] += 1
            else:
                self.groups[val1_group] = val2_group
                self.rank[val2_group] += 1
        return friends_now


def find_the_moment_everyone_became_friends(logs, lenn):
    logs.sort(key=lambda x: x[0])
    print(logs)
    group_count = lenn
    uf = UnionFind(lenn)
    for time, frnd_a, frnd_b in logs:
        frnd_a = int(frnd_a)
        frnd_b = int(frnd_b)
        friends_now = uf.union(frnd_a, frnd_b)
        if friends_now:
            group_count -= 1
            if group_count == 1:
                return time
    return -1
    # print(logs)


logs = [
    [20190101, 0, 1],
    [20190104, 3, 4],
    [20190107, 2, 3],
    [20190211, 1, 5],
    [20190224, 2, 4],
    [20190301, 0, 3],
    [20190312, 1, 2],
    [20190322, 4, 5],
]
n = 6
logs = [
    [5, 4, 3],
    [2, 0, 4],
    [1, 1, 2],
    [0, 0, 2],
    [9, 1, 3],
    [3, 1, 4],
    [8, 2, 4],
    [6, 1, 0],
]
n = 5
logs = [
    [1, 2, 1],
    [7, 4, 1],
    [4, 0, 3],
    [3, 0, 1],
    [2, 0, 5],
    [12, 4, 3],
    [8, 3, 1],
    [0, 3, 2],
    [6, 3, 5],
]
n = 6
print(find_the_moment_everyone_became_friends(logs, n))
