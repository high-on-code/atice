class UnionFind:
    def __init__(self, num_vals):
        self.rank = [1] * num_vals
        self.parent = [idx for idx in range(num_vals)]

    def find(self, node):
        if self.parent[node] == node:
            return node
        else:
            self.parent[node] = self.find(self.parent[node])
            return self.parent[node]

    def union(self, node1, node2):
        pnode1 = self.find(node1)
        pnode2 = self.find(node2)
        if pnode1 == pnode2:
            return False
        if self.rank[pnode1] > self.rank[pnode2]:
            self.parent[pnode2] = pnode1
        elif self.rank[pnode1] < self.rank[pnode2]:
            self.parent[pnode1] = pnode2
        else:
            self.parent[pnode1] = pnode2
            self.rank[pnode2] += 1
        return True


def find_if_graph_is_valid_tree(num_ver, edges):
    uf = UnionFind(num_ver)
    if len(edges) != num_ver - 1:
        return False
    for u, v in edges:
        if not uf.union(u, v):
            return False
    return True


n = 5
edges = [[0, 1], [0, 2], [0, 3], [1, 4]]
n = 5
edges = [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]]
print(find_if_graph_is_valid_tree(n, edges))
