# https://leetcode.com/problems/non-overlapping-intervals/
def find_min_intervals_to_remove(intervals):
    intervals.sort(key=lambda v: v[1])
    print(intervals)
    skips = 0
    idx = 0
    while idx < len(intervals) - 1:
        nxt = 1
        while idx + nxt < len(intervals) and intervals[idx][1] > intervals[idx + nxt][0]:
            skips += 1
            nxt += 1
        idx += nxt
    return skips
intervals = [[1,2],[2,3],[3,4],[1,3]]
intervals = [[1, 2], [1, 2], [1, 2]]
print(find_min_intervals_to_remove(intervals))