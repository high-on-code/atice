def reverse_prefix_word(word, ch):
    str_arr = list(word)
    ch_idx = -1
    for idx, val in enumerate(word):
        if val == ch:
            ch_idx = idx
            break
    else:
        return word
    st_idx = 0
    while st_idx < ch_idx:
        str_arr[st_idx], str_arr[ch_idx] = str_arr[ch_idx], str_arr[st_idx]
        st_idx += 1
        ch_idx -= 1
    return "".join(str_arr)

print(reverse_prefix_word("abcdefd","d"))
print(reverse_prefix_word("xyxzxe", "z"))
print(reverse_prefix_word("abcd","z"))