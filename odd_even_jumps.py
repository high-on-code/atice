def odd_even_jumps(arr):
    arr_len = len(arr)
    next_higher = [0] * len(arr)
    stack = []
    for val, idx in sorted([(val, idx) for (idx, val) in enumerate(arr)]):
        while stack and stack[-1] < idx:
            next_higher[stack.pop()] = idx
        stack.append(idx)

    next_lower = [0] * len(arr)
    stack = []
    for val, idx in sorted([(-val, idx) for (idx, val) in enumerate(arr)]):
        while stack and stack[-1] < idx:
            next_lower[stack.pop()] = idx
        stack.append(idx)
    higher = [0] * arr_len
    lower = [0] * arr_len
    higher[-1] = 1
    lower[-1] = 1
    for idx in range(arr_len - 2, -1, -1):
        higher[idx] = lower[next_higher[idx]]
        lower[idx] = higher[next_lower[idx]]
    return sum(higher)


arr = [10, 13, 12, 14, 15]
print(odd_even_jumps(arr))
