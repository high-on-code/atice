# https://leetcode.com/problems/number-of-longest-increasing-subsequence/editorial/
def find_num_longest_increasing_subseq(nums):
    numlen = len(nums)
    if numlen == 1:
        return 1
    lent = [1] * len(nums)
    count = [1] * len(nums)
    max_len = 1
    for idx1 in range(1, len(nums)):
        for idx2 in range(idx1):
            if nums[idx1] > nums[idx2]:
                if lent[idx1] < lent[idx2] + 1:
                    lent[idx1] = lent[idx2] + 1
                    count[idx1] = 0
                    max_len = max(max_len, lent[idx1])
                if lent[idx1] == lent[idx2] + 1:
                    count[idx1] += count[idx2]
    res = 0
    for idx, val in enumerate(lent):
        if val == max_len:
            res += count[idx]
    return res

nums = [1,3,5,4,7]
print(find_num_longest_increasing_subseq(nums))