def find_frog_jump_recur(stones, cache, idx, cjump, st_idx):
    # print(idx, cjump)
    if idx == len(stones) - 1:
        return True
    if cache[idx][cjump] != -1:
        return cache[idx][cjump]
    ans = False
    for nju in [cjump - 1, cjump, cjump + 1]:
        # nju = dju + st_idx[idx]
        if nju > 0 and nju + stones[idx] in st_idx:
            ans = ans or find_frog_jump_recur(
                stones, cache, st_idx[nju + stones[idx]], nju, st_idx)
    cache[idx][cjump] = ans
    return ans


def find_frog_jump(stones):
    if stones[1] != 1:
        return False
    st_idx = {val: idx for idx, val in enumerate(stones)}
    max_jump = stones[-1] + stones[-2]
    stnlen = len(stones)
    cache = [[-1] * (max_jump + 1) for _ in range(stnlen)]
    return find_frog_jump_recur(stones, cache, 0, 0, st_idx)


stones = [0, 1, 3, 5, 6, 8, 12, 17]
print(find_frog_jump(stones))
