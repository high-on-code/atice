# https://leetcode.com/problems/minimum-ascii-delete-sum-for-two-strings/description/
def find_acii(ch):
    return ord(ch)
def find_min_ascii_delete_recur(st1, st2, idx1, idx2, cache):
    if idx1 == idx2 == -1:
        return 0
    if (idx1, idx2) in cache and cache[(idx1, idx2)] > -1:
        return cache[(idx1, idx2)]
    if idx1 == -1:
        cache[(idx1, idx2)] = find_acii(st2[idx2]) + find_min_ascii_delete_recur(st1, st2, idx1, idx2 - 1, cache)
        return cache[(idx1, idx2)]
    if idx2 == -1:
        cache[(idx1, idx2)] = find_acii(st1[idx1]) + find_min_ascii_delete_recur(st1, st2, idx1 - 1, idx2, cache)
        return cache[(idx1, idx2)]
    if st1[idx1] == st2[idx2]:
        cache[(idx1, idx2)] = find_min_ascii_delete_recur(st1, st2, idx1 - 1, idx2 - 1, cache)
        return cache[(idx1, idx2)]
    cache[(idx1, idx2)] = min( find_acii(st2[idx2]) + find_min_ascii_delete_recur(st1, st2, idx1, idx2 - 1, cache) ,find_acii(st1[idx1]) + find_min_ascii_delete_recur(st1, st2, idx1 - 1, idx2, cache))
    return cache[(idx1, idx2)]
def find_min_ascii_delete(st1, st2):
    cache = {}
    return find_min_ascii_delete_recur(st1, st2, len(st1) - 1, len(st2) - 1, cache)

s1 = "sea"
s2 = "eat"
print(find_min_ascii_delete(s1, s2))