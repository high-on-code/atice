def find_longest_palin_substr(strn):
    strnlen = len(strn)
    cache = [[0] * strnlen for _ in range(strnlen)]
    # print("*" * 80)
    # print("ironman cache", cache)
    max_len = 0
    for gap in range(strnlen):
        rid, cid = 0, gap
        while cid < strnlen:
            if gap == 0:
                cache[rid][cid] = True
            elif gap == 1:
                cache[rid][cid] = True if strn[rid] == strn[cid] else False
            else:
                cache[rid][cid] = (
                    True
                    if strn[rid] == strn[cid] and cache[rid + 1][cid - 1]
                    else False
                )
            if cache[rid][cid]:
                max_len = gap + 1
                max_str = strn[rid : cid + 1]
            rid, cid = rid + 1, cid + 1
    return max_str


strn = "a"
strn = "ab"
strn = "aa"
strn = "aba"
strn = "abac"
strn = "crbidxnkyieminyzchamldzjzglygkfbbyggagwdqdqamzpguppqjfrcewfduwvcmhliahovcwoupwxwhaoiiiguahuqxiqndwxqxweppcbeoasgtucyqlxnxpvtepwretywgjigjjuxsrbwucatffkrqyfkesakglyhpmtewfknevopxljgcttoqonxpdtzbccpyvttuaisrhdauyjyxgquinvqvfvzgusyxuqkyoklwslljbimbgznpvkdxmakqwwveqrpoiabmiegoyzuyoignfcgmqxvpcmldivknulqbpyxjuvyhrzcsgiusdhsogftokekbdynmksyebsnzbxjxfvwamccphzzlzuvotvubqvhmusdtwvlsnbqwqhqcigmlfoupnqcxdyflpgodnoqaqfekhcyxythaiqxzkahfnblyiznlqkbithmhhytzpcbkwicstapygjpjzvrjcombyrmhcusqtslzdyiiyvujnrxvkrwffwxtmdqqrawtvayiskcnpyociwkeopardpjeyuymipekbefbdfuybfvgzfkjtvurfkopatvusticwbtxdtfifgklpmjamiocalcocqwdivyulupammxhdbeazrrktxiyothnvbwwrsocxzxaxmoenigbhvxffddexrwsioqoyovaqvtmkwzupstkgkmvrddzolmuzjnsj"
print(find_longest_palin_substr(strn))
