def smallest_from_leaf(root):
    if root.left:
        strn = smallest_from_leaf(root.left)
    if root.right:
        strn = min(strn, smallest_from_leaf(root.right))
    return strn + chr(root.val + ord('a'))