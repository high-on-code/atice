# https://leetcode.com/problems/jump-game-iii
from collections import deque


def can_reach(arr, start):
    arrlen = len(arr)
    deq = deque()
    deq.append(start)

    while deq:
        wallen = len(deq)
        for _ in range(wallen):
            curr_node = deq.popleft()
            curr_val = arr[curr_node]
            arr[curr_node] = -1
            if curr_val == 0:
                return True
            if 0 <= curr_node - curr_val and curr_val != -1:
                deq.append(curr_node - curr_val)
            if curr_node + curr_val < arrlen and curr_val != -1:
                deq.append(curr_node + curr_val)
    return False


arr = [4, 2, 3, 0, 3, 1, 2]
start = 5
arr = [3, 0, 2, 1, 2]
start = 2
print(can_reach(arr, start))
