def find_max_depth(root):
    if not root:
        return 0
    stack = []
    stack.append(root)
    curr_h = 1
    curr_node = root
    res = 0
    while stack:
        if curr_node.left:
            stack.append(curr_node.left)
            curr_node = curr_node.left
            curr_h += 1
        elif curr_node.right:
            stack.append(curr_node.right)
            curr_node = curr_node.right
            curr_h += 1
        else:
            res = max(res, curr_h)
            stack.pop()
            curr_h -= 1
    return res
