from collections import defaultdict
import heapq

INFI = float("inf")


def network_delay_time(times, node_count, start):
    graph = defaultdict(list)
    for st, ed, ti in times:
        graph[st].append((ti, ed))
    reached_at = [INFI] * node_count
    pq = [(0, start)]
    reached_at[start - 1] = 0
    while pq:
        time, current_node = heapq.heappop(pq)
        for time, ed in graph[current_node]:
            if reached_at[current_node - 1] + time < reached_at[ed - 1]:
                reached_at[ed - 1] = reached_at[current_node - 1] + time
                heapq.heappush(pq, (reached_at[ed - 1], ed))
    max_time = -float("inf")
    for time in reached_at:
        max_time = max(time, max_time)
    return -1 if max_time == INFI else max_time


times = [[2, 1, 1], [2, 3, 1], [3, 4, 1]]
n = 4
k = 2
"""
times = [[1, 2, 1]]
n = 2
k = 1
"""
print(network_delay_time(times, n, k))
