def get_target_sum(nums, cidx, csum, target, res):
    if cidx == len(nums):
        res += 1 if target == csum else 0
        return res
    res = get_target_sum(nums, cidx + 1, csum + nums[cidx], target, res)
    return get_target_sum(nums, cidx + 1, csum - nums[cidx], target, res)


def find_target_sum_ways(nums, target):
    return get_target_sum(nums, 0, 0, target, 0)


nums = [1, 1, 1, 1, 1]
target = 3
print(find_target_sum_ways(nums, 3))
