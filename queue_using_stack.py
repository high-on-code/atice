class MyQueue:
    def __init__(self):
        self.s1 = []
        self.s2 = []

    def push(self, val):
        self.s1.append(val)

    def pop(self, val):
        if not self.s2:
            self._pour()
        return self.s2.pop()

    def _pour(self):
        while self.s1:
            self.s2.append(self.s1.pop())

    def peek(self):
        return self.s2[-1]

    def empty(self):
        return self.s1 or self.s2
