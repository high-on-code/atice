def romanToInt(s):
    map = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}
    if len(s) == 1:
        return map[s]
    strn_len = len(s)
    res = 0
    idx = 0
    while idx < (strn_len - 1):
        if map[idx] < map[idx + 1]:
            res += map[idx + 1] - map[idx]
            idx += 2
        else:
            res += map[idx]
            idx += 1
    if idx == strn_len - 1:
        res += map[idx]


s = "III"
print(romanToInt(s))
