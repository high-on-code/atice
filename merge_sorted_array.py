def merge_sorted_arrays(nums1, nums2, num1_len, num2_len):
    p1 = num1_len - 1
    p2 = num2_len - 1
    while p1 >= 0 and p2 >= 0:
        if nums1[p1] > nums2[p2]:
            nums1[p1 + p2 + 1] = nums1[p1]
            p1 -= 1
        else:
            nums1[p1 + p2 + 1] = nums2[p2]
            p2 -= 1
    if p2 >= 0:
        for idx in range(p2, -1, -1):
            nums1[idx] = nums2[p2]
            p2 -= 1

nums1 = [1, 2, 3, 0, 0, 0]
m = 3
nums2 = [2, 5, 6]
n = 3

nums1 = [4, 5, 6, 0, 0, 0]
m = 3
nums2 = [1, 2, 3]
n = 3
merge_sorted_arrays(nums1, nums2, m, n)
print(nums1)
