import heapq


def min_meeting_rooms(intervals):
    if not intervals:
        return 0
    intervals.sort(key=lambda x: x[0])
    end_times = [intervals[0][1]]
    total_rooms = 1
    for st, ed in intervals[1:]:
        if end_times[0] <= st:
            heapq.heappushpop(end_times, ed)
        else:
            total_rooms += 1
            heapq.heappush(end_times, ed)
    return total_rooms


intervals = [[0, 30], [5, 10], [15, 20]]
print(min_meeting_rooms(intervals))
