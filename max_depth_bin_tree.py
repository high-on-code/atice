def find_max_depth_recur(node):
    if not node:
        return 0
    return 1 + max(find_max_depth_recur(node.left), find_max_depth_recur(node.right))


def find_max_depth(node):
    return find_max_depth_recur(node)
