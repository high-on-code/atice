def find_longest_arith_subseq(arr, diff):
    dp = {}
    for val in arr:
        if val - diff in dp:
            dp[val] = dp[val - diff] + 1
        else:
            dp[val] = 1
    return max(dp.values())

arr = [1,2,3,4]
difference = 1
print(find_longest_arith_subseq(arr, difference))