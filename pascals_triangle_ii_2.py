def pascals_triangle_helper(cache, row, target):
    if row == target:
        return
    cache.append(1)
    for idx in range(row - 1, 0, -1):
        cache[idx] = (cache[idx - 1] + cache[idx])

    pascals_triangle_helper(cache, row + 1, target)


def pascals_triangle(row):
    cache = [1]
    if row == 0:
        return cache
    pascals_triangle_helper(cache, 1, row)
    return cache


row = 3
print(pascals_triangle(row))
