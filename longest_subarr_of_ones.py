# https://leetcode.com/problems/longest-subarray-of-1s-after-deleting-one-element/
def find_longest_subarray_of_1s(arr):
    start = 0
    zero_count = 0
    max_size = 0
    for idx, val in enumerate(arr):
        if val == 0:
            zero_count += 1
        if zero_count > 1:
            max_size = max(max_size, idx - start - 1)
            while zero_count > 1:
                if arr[start] == 0:
                    zero_count -= 1
                start += 1
    return max(max_size, idx - start)