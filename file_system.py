class TrieNode:
    def __init__(self, path=None, value=None):
        self.nexts = {}
        self.path = path
        self.value = value


class FileSystem:
    def __init__(self):
        self.root = TrieNode()

    def createPath(self, path, value):
        curr = self.root
        comps = path.split("/")[1:]
        for a_comp in comps[:-1]:
            if a_comp in curr.nexts:
                curr = curr.nexts[a_comp]
            else:
                # curr.nexts[a_comp] = TrieNode(a_comp)
                # curr = curr.nexts[a_comp]
                return False
        a_comp = comps[-1]
        if a_comp in curr.nexts:
            return False
        else:
            curr.nexts[a_comp] = TrieNode(a_comp, value)
            return True

    def get(self, path):
        curr = self.root
        comps = path.split("/")[1:]
        for a_comp in comps:
            if a_comp in curr.nexts:
                curr = curr.nexts[a_comp]
            else:
                return -1
        return curr.value
