def find_longest_increasing_subsequence(nums):
    num_len = len(nums)
    res = [1] * num_len
    max_len = 0
    for idx in range(1, num_len):
        for idx2 in range(idx):
            if nums[idx] > nums[idx2]:
                res[idx] = max(res[idx], res[idx2] + 1)
        max_len = max(max_len, res[idx])

    return max_len


nums = [10, 9, 2, 5, 3, 7, 101, 18]
print(find_longest_increasing_subsequence(nums))
