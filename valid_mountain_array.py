def check_valid_mountain_array(arr):
    idx = 0
    arr_len = len(arr)
    while idx < arr_len - 1 and arr[idx] <= arr[idx + 1]:
        idx += 1
    if idx == arr_len - 1 or idx == 0:
        return False
    while idx < arr_len - 1 and arr[idx] >= arr[idx + 1]:
        idx += 1
    if idx == arr_len - 1:
        return True
    else:
        False


arr = [2, 1]
arr = [3, 5, 5]
arr = [0, 3, 2, 1]
print(check_valid_mountain_array(arr))
