from collections import Counter, defaultdict


def min_window_substr(src, tar):
    tar_ch_counter = Counter(tar)
    win_ch_counter = defaultdict(lambda: 0)
    formed = 0
    req = len(tar_ch_counter)
    left, right = 0, 0
    src_len = len(src)
    ans = float("inf"), None, None
    while right < src_len:
        char = src[right]
        win_ch_counter[char] += 1
        if tar_ch_counter[char] == win_ch_counter[char]:
            formed += 1
        while formed == req:
            curr_win_size = right - left + 1
            if ans[0] > curr_win_size:
                ans = curr_win_size, left, right
            char = src[left]
            left += 1
            win_ch_counter[char] -= 1
            if tar_ch_counter[char] > win_ch_counter[char]:
                formed -= 1
        right += 1
    return "" if ans[0] == float("inf") else src[ans[1] : ans[2] + 1]


s = "ADOBECODEBANC"
t = "ABC"
s = "a"
t = "a"
s, t = "a", "aa"
print(min_window_substr(s, t))
