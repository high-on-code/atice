# https://leetcode.com/problems/number-of-connected-components-in-an-undirected-graph/
class UnionFind:
    def __init__(self, num_nodes):
        self.parent = [idx for idx in range(num_nodes)]
        self.rank = [1] * num_nodes

    def find(self, node):
        if self.parent[node] == node:
            return node
        else:
            self.parent[node] = self.find(self.parent[node])
            return self.parent[node]

    def union(self, node1, node2):
        pnode1 = self.find(node1)
        pnode2 = self.find(node2)
        if pnode1 == pnode2:
            return False
        if self.rank[pnode1] > self.rank[pnode2]:
            self.parent[pnode2] = pnode1
        elif self.rank[pnode1] < self.rank[pnode2]:
            self.parent[pnode1] = pnode2
        else:
            self.parent[pnode2] = pnode1
            self.rank[pnode1] += 1
        return True


def find_num_connected_components(num_nodes, edges):
    uf = UnionFind(num_nodes)
    for u, v in edges:
        uf.union(u, v)
    component_count = 0
    for idx, val in enumerate(uf.parent):
        if idx == val:
            component_count += 1
    return component_count


n = 5
edges = [[0, 1], [1, 2], [3, 4]]

print(find_num_connected_components(n, edges))
