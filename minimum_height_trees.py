def find_mininum_height_trees(edges, edge_count):
    # base cases
    if edge_count <= 2:
        return [i for i in range(edge_count)]
    graph = [set() for _ in range(edge_count)]
    for to, fro in edges:
        graph[to].add(fro)
        graph[fro].add(to)

    leaves = []
    for idx, val in enumerate(graph):
        if len(val) == 1:
            leaves.append(idx)

    nodes_remaining = edge_count
    while nodes_remaining > 2:
        nodes_remaining -= len(leaves)
        new_leaves = []
        for a_leav in leaves:
            relative = graph[a_leav].pop()
            graph[relative].remove(a_leav)
            if len(graph[relative]) == 1:
                new_leaves.append(relative)
        leaves = new_leaves
    return leaves


edges = [[1, 0], [1, 2], [1, 3]]
n = 4
print(find_mininum_height_trees(edges, n))
