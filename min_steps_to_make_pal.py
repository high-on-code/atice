def find_max_lcs(st, rst, st1, st2, cache):
    if cache[st1][st2] != -1:
        return cache[st1][st2]
    if st1 == 0 or st2 == 0:
        return 0
    if st[st1 - 1] == rst[st2 - 1]:
        cache[st1][st2] = 1 + find_max_lcs(st, rst, st1 - 1, st2 - 1, cache)
    else:
        cache[st1][st2] = max(find_max_lcs(st, rst, st1 - 1, st2, cache), find_max_lcs(st, rst, st1, st2 - 1, cache))
    return cache[st1][st2]
def min_steps_to_make_pal(st):
    stlen = len(st)
    cache = [[-1] * (stlen + 1) for _ in range(stlen + 1)]
    max_lwc = find_max_lcs(st, st[::-1], stlen, stlen, cache)
    return stlen - max_lwc

s = "zzazz"
s = "mbadm"
print(min_steps_to_make_pal(s))