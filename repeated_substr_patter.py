def check_if_substr_repeated(strn):
    strlen = len(strn)
    if strlen %2 == 0:
        hallen = strlen // 2
        if strn[:hallen] == strn[hallen:]:
            return True
    if strlen % 3 == 0:
        hallen = strlen // 3
        if strn[:hallen] == strn[hallen: 2 * hallen] == strn[2 * hallen:]:
            return True
    return False

s = "abab"
s = "aba"
s = "abcabcabc"
s = "ababababab"
print(check_if_substr_repeated(s))