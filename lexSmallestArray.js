var lexicographicallySmallestArray = function(nums, limit) {
    const numsToGroup = {};
    const groupToNums = {};
    // copy nums to sortedNums
    const sortedNums = [...nums];
    // sort the nums array
    sortedNums.sort((a, b) => a - b);
    console.log('ironman nums', JSON.stringify(sortedNums));
    let currGroup = 0;
    numsToGroup[sortedNums[0]] = currGroup;
    groupToNums[currGroup] = [sortedNums[0]];
    for (let i = 1; i < sortedNums.length; i++) {
        const diff = Math.abs(sortedNums[i] - sortedNums[i - 1]);
        if (diff > limit) {
            currGroup++;
        }
        numsToGroup[sortedNums[i]] = currGroup;
        // check if currGroup exists in groupToNums
        if (groupToNums[currGroup]) {
            groupToNums[currGroup].push(sortedNums[i]);
        } else {
            groupToNums[currGroup] = [sortedNums[i]];
        }
    }
    // iterate over nums
    for (let idx = 0; idx < nums.length; idx++) {
        const num = nums[idx];
        const group = numsToGroup[num];
        const newNum = groupToNums[group].shift();
        nums[idx] = newNum;
    }
    return nums;
};

let nums = [1,5,3,9,8];
let limit = 2;

console.log(lexicographicallySmallestArray(nums, limit))