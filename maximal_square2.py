def find_maximal_square(matrix):
    num_rows = len(matrix)
    num_cols = len(matrix[0])
    cache = [[0] * num_cols for _ in range(num_rows)]
    # print(cache)
    # max_sqlen = int(matrix[0][0])
    cache[0] = [int(val) for val in matrix[0]]
    max_sqlen = any(cache[0])
    for ridx in range(1, num_rows):
        cache[ridx][0] = int(matrix[ridx][0])
        max_sqlen = max(max_sqlen, cache[ridx][0])
        for cidx in range(1, num_cols):
            if matrix[ridx][cidx] == "1":
                cache[ridx][cidx] = (
                    min(
                        cache[ridx - 1][cidx - 1],
                        cache[ridx][cidx - 1],
                        cache[ridx - 1][cidx],
                    )
                    + 1
                )
                max_sqlen = max(max_sqlen, cache[ridx][cidx])
    return max_sqlen ** 2


matrix = [
    ["1", "0", "1", "0", "0"],
    ["1", "0", "1", "1", "1"],
    ["1", "1", "1", "1", "1"],
    ["1", "0", "0", "1", "0"],
]
# matrix = [["0", "1"], ["1", "0"]]
# matrix = [["0", "1"]]
# matrix = [
#     ["1", "1", "1", "1", "1"],
#     ["1", "1", "1", "1", "1"],
#     ["0", "0", "0", "0", "0"],
#     ["1", "1", "1", "1", "1"],
#     ["1", "1", "1", "1", "1"],
# ]
print(find_maximal_square(matrix))
