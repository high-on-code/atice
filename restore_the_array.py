def restore_the_array_recur(st, tar, idx, cache, mod):
    if cache[idx] != -1:
        return cache[idx]
    stlen = len(st)
    if idx == stlen:
        # cache[idx] = 1
        return 1
    if st[idx] == 0:
        # cache[idx] = 0
        return 0
    count = 0
    for ed in range(idx, stlen):
        curr_num = st[idx: ed + 1]
        if int(curr_num) > tar:
            break
        count += restore_the_array_recur(st, tar, ed + 1, cache, mod)
    cache[idx] = count % mod
    return count
    
        
    
    
def restore_the_array(st, tar):
    stlen = len(st)
    cache = [-1] * (len(st) + 1)
    mod = 10 ** 9 + 7
    restore_the_array_recur(st, tar, 0, cache, mod)
    return cache[0] % mod



    
s = "1000"
k = 10000
print(restore_the_array(s, k))