def burst_ballons_helper(nums, cache):
    if not nums:
        return 0
    max_coins = 0
    for idx in range(1, len(nums) -1):
        max_coins = max(max_coins, burst_ballons_helper())

def burst_ballons(nums):
    return burst_ballons_helper([1] + nums + [1], {})
