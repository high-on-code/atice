def find_permutation(strn):
    stack = ["1"]
    idx = 2
    res = []
    for val in strn:
        if val == "I":
            while stack:
                res.append(stack.pop())
        stack.append(str(idx))
        idx += 1
    while stack:
        res.append(stack.pop())
    return "".join(res)


s = "I"
s = "DI"
s = "D"
print(find_permutation(s))
