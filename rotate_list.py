def rotate_list(head, poss):
    if head is None or head.next is None:
        return head
    old_tail = head
    list_len = 1
    while old_tail.next:
        list_len += 1
        old_tail = old_tail.next
    old_tail.next = head
    new_tail = head
    for _ in range((list_len - poss - 1) % list_len):
        new_tail = new_tail.next
    head = new_tail.next
    new_tail.next = None
    return head
