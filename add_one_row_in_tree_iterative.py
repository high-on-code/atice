def add_one_row_in_tree(root, val, depth):
    if depth == 1:
        new_node = TreeNode(val, root)
        return new_node
    stack = [(root, depth)]
    while stack:
        node, depth = stack.pop()
        if not node:
            continue
        if depth - 1 == 1:

            left = node.left
            right = node.right
            new_left = TreeNode(val)
            new_right = TreeNode(val)
            node.left = new_left
            node.right = new_right
            node.left.left = left
            node.right.right = right
        elif depth <= 1:
            continue
        else:
            stack.append((node.left, depth - 1))
            stack.append((node.right, depth - 1))
    return root
