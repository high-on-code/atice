# https://leetcode.com/problems/fruit-into-baskets
def find_max_fruits_in_basket(fruits):
    if not fruits:
        return 0
    sol = 1
    fruit_idx = {}
    pre, nxt = 0, 0
    while nxt < len(fruits):
        fruit_type = fruits[nxt]
        fruit_idx[fruit_type] = nxt
        if len(fruit_idx) > 2:
            pre = min(fruit_idx.values())
            del fruit_idx[fruits[pre]]
            pre += 1
        sol = max(nxt - pre + 1, sol)
        nxt += 1
    return sol


fruits = [1, 2, 1]
fruits = [0,1,2,2]
print(find_max_fruits_in_basket(fruits))
