function findNum1(numm) {
    let num1s = 0;
    // count number of 1s in binary representation of num1
    while (numm) {
        num1s += numm & 1;
        numm >>= 1;
    }
    return num1s;
}
var minimizeXor = function(num1, num2) {
    const num1s = findNum1(num2);
    console.log('ironman num1s', JSON.stringify(num1s));
    let res = num1;
    let currbit = 0;
    // while res has more than num1s 1s
    while (num1s < findNum1(res)) {
        // if current bit is set
        if (res & (1 << currbit)) {
            res ^= 1 << currbit;
        }
        currbit += 1;
    }
    while (num1s > findNum1(res)) {
        if (!(res & (1 << currbit))) {
            res ^= 1 << currbit;
        }
        currbit += 1;
    }
    return res;
};

let num1 = 3
let num2 = 5
num1 = 1
num2 = 12
console.log(minimizeXor(num1, num2))