def count_vowels_permutation(str_len):
    vowel_start_witha = [1] * str_len
    vowel_start_withe = [1] * str_len
    vowel_start_withi = [1] * str_len
    vowel_start_witho = [1] * str_len
    vowel_start_withu = [1] * str_len
    MOD = 10 ** 9 + 7
    for idx in range(1, str_len):
        vowel_start_witha[idx] = (
            vowel_start_withe[idx - 1]
            + vowel_start_withi[idx - 1]
            + vowel_start_withu[idx - 1]
        ) % MOD
        vowel_start_withe[idx] = (
            vowel_start_witha[idx - 1] + vowel_start_withi[idx - 1]
        ) % MOD

        vowel_start_withi[idx] = (
            vowel_start_withe[idx - 1] + vowel_start_witho[idx - 1]
        ) % MOD
        vowel_start_witho[idx] = vowel_start_withi[idx - 1] % MOD
        vowel_start_withu[idx] = (
            vowel_start_withi[idx - 1] + vowel_start_witho[idx - 1]
        ) % MOD
    return (
        vowel_start_witha[-1]
        + vowel_start_withe[-1]
        + vowel_start_withi[-1]
        + vowel_start_witho[-1]
        + vowel_start_withu[-1]
    ) % MOD


print(count_vowels_permutation(2))
print(count_vowels_permutation(5))
