import socket

# Define the server address and port
SERVER_ADDRESS = 'localhost'
SERVER_PORT = 65432

# Define the array of bytes to send
# array_of_bytes = b'\x01\x02\x03\x04\x05\x06\x07\x08'
sum_of_bytes = [1, 2, 3, 4, 5, 6, 7, 8]

# Convert the list of integers to a byte array
array_of_bytes = bytes(sum_of_bytes)

# Create a socket object and connect to the server
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect((SERVER_ADDRESS, SERVER_PORT))

# Send the array of bytes to the server
client_socket.sendall(array_of_bytes)

# Receive the sum over 4 bytes from the server
sum_over_4_bytes = client_socket.recv(4)

# Convert the sum to an integer and print it to the screen
sum_as_int = int.from_bytes(sum_over_4_bytes, byteorder='big')
print(sum_as_int)

# Close the socket connection
client_socket.close()