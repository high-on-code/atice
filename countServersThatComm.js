var countServers = function(grid) {
    const rowCount = new Array(grid.length).fill(0);
    const colCount = new Array(grid[0].length).fill(0);
    let count = 0;
    for (let i = 0; i < grid.length; i++) {
        for (let j = 0; j < grid[0].length; j++) {
            if (grid[i][j] === 1) {
                rowCount[i]++;
                colCount[j]++;
            }
        }
    }
    for (let i = 0; i < grid.length; i++) {
        for (let j = 0; j < grid[0].length; j++) {
            if (grid[i][j] === 1 && (rowCount[i] > 1 || colCount[j] > 1)) {
                count++;
            }
        }
    }
    return count;
};
let grid = [[1,0],[0,1]];
grid = grid = [[1,0],[1,1]];
console.log(countServers(grid));