from collections import deque


class MovingAverage:
    def __init__(self, size):
        self.size = size
        self.qu = deque()
        self.winsum = 0
        self.count = 0

    def next(self, val):
        self.count += 1
        trash = self.qu.popleft() if self.size < self.count else 0
        self.winsum += val - trash
        self.qu.append(val)
        return self.winsum / min(self.count, self.size)
