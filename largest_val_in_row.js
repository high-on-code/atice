/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */

var largestValues = function(root) {
    const res = [];
    // bfs(root)
    const qu = []
    qu.push(root)
    res.push(root.val)
    while (qu) {
        let currWall = qu.length;
            let maxVal = 0;
        for (let i = 0; i < currWall; i ++) {
            let currNode = qu.shift();
            let currVal = currNode.val;
            maxVal = Math.max(currVal, maxVal)
            if (currNode.left) {
                qu.push(currNode.left);
            }
            if (currNode.right) {
                qu.push(currNode.right)
            }
        }
        res.push(maxVal)
    }
    return res;
};

const root = [1,3,2,5,3,null,9];
console.log(largestValues(root))