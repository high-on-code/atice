class UnionFind:
    def __init__(self, size):
        self.group = [i for i in range(size)]
        self.rank = [0] * size

    def find(self, node):
        if self.group[node] != node:
            self.group[node] = self.find(self.group[node])
        return self.group[node]

    def union(self, node1, node2):
        root1 = self.find(node1)
        root2 = self.find(node2)
        if root1 != root2:
            if self.rank[root1] < self.rank[root2]:
                self.group[root1] = root2
            else:
                self.group[root2] = root1
                if self.rank[root1] == self.rank[root2]:
                    self.rank[root2] += 1

    def are_connected(self, node1, node2):
        return self.group[node1] == self.group[node2]


def dist_limited_path(edge_ct, edge_list, queries):
    edge_list = sorted(edge_list, key=lambda x: x[-1])
    # print(queries)
    # for idx, val in enumerate(queries):
    #     print(idx, val)
    queries = [(org, dest, dist, idx)
               for idx, (org, dest, dist) in enumerate(queries)]
    queries = sorted(queries, key=lambda x: x[-2])
    # print(queries)
    # print(edge_ct)
    # print(edge_list)
    uf = UnionFind(edge_ct)
    res = [False] * len(queries)
    for org, dest, dist, idx in queries:
        for or2, de2, di2 in edge_list:
            if dist < di2:
                uf.union(or2, de2)
            else:
                res[idx] = uf.are_connected(org, dest)
    return res


n=3
edgeList=[[0, 1, 2], [1, 2, 4], [2, 0, 8], [1, 0, 16]]
queries=[[0, 1, 2], [0, 2, 5]]
print(dist_limited_path(n, edgeList, queries))
