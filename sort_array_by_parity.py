def sort_array_by_parity(nums):
    start = 0
    num_len = len(nums)
    endp = num_len - 1
    while True:
        while start < num_len and nums[start] % 2 == 0:
            start += 1
        while endp >= 0 and nums[endp] % 2 == 1:
            endp -= 1
        if start < endp and endp >= 0 and start < num_len:
            nums[start], nums[endp] = nums[endp], nums[start]
            start += 1
            endp -= 1
        else:
            break
    return nums


nums = [3, 1, 2, 4]
nums = [0]
print(sort_array_by_parity(nums))
