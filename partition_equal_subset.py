import math


# (Better) used less number of inputs by using nums_left and diffing from target
def find_equal_partitions_helper(nums, nums_left, target):
    if nums_left == 0:
        return False
    elif target == 0:
        return True
    res = find_equal_partitions_helper(
        nums, nums_left - 1,
        target - nums[nums_left - 1]) or find_equal_partitions_helper(
            nums, nums_left - 1, target)
    return res


def find_equal_partitions(nums):
    target = sum(nums) / 2
    if math.floor(target) != target:
        return False
    return find_equal_partitions_helper(nums, len(nums), target)


nums = [1, 5, 11, 5]
nums = [1, 2, 3, 5]
print(find_equal_partitions(nums))
