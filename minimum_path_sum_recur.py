# https://leetcode.com/problems/minimum-path-sum/
import sys


def minPathSum_recur(row, col, grid, cache):
    if row == len(grid) - 1 and col == len(grid[0]) - 1:
        return grid[row][col]
    if not (-1 < row < len(grid) and -1 < col < len(grid[0])):
        return sys.maxsize
    if cache[row][col] is not None:
        return cache[row][col]
    # for rid, cid in [(row + 1, col), (row, col + 1)]:
    res = (
        min(
            minPathSum_recur(row + 1, col, grid, cache),
            minPathSum_recur(row, col + 1, grid, cache),
        )
        + grid[row][col]
    )
    cache[row][col] = res
    return res


def minPathSum(grid):
    cache = []
    for _ in range(len(grid)):
        cache.append([None] * len(grid[0]))
    return minPathSum_recur(0, 0, grid, cache)


grid = [[1, 3, 1], [1, 5, 1], [4, 2, 1]]
print(minPathSum(grid))
