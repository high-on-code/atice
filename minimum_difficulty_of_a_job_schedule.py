def find_minimum_difficulty_of_a_job_schedule_helper(
    job_difficulty, days, curr_day, curr_job, cache
):
    if cache[curr_job][curr_day - 1] is not None:
        return cache[curr_job][curr_day - 1]
    if curr_day == days:
        return max(job_difficulty[curr_job:])
    last_job_that_can_be_done = len(job_difficulty) - (days - curr_day)
    min_difficulty = float("inf")
    max_day_difficulty = -float("inf")

    for idx in range(curr_job, last_job_that_can_be_done):
        max_day_difficulty = max(job_difficulty[idx], max_day_difficulty)
        res = (
            find_minimum_difficulty_of_a_job_schedule_helper(
                job_difficulty, days, curr_day + 1, idx + 1, cache
            )
            + max_day_difficulty
        )
        min_difficulty = min(min_difficulty, res)
    cache[curr_job][curr_day - 1] = min_difficulty
    return cache[curr_job][curr_day - 1]


def find_minimum_difficulty_of_a_job_schedule(job_difficulty, days):
    if days > len(job_difficulty):
        return -1
    cache = [[None] * days for _ in range(len(job_difficulty))]
    res = find_minimum_difficulty_of_a_job_schedule_helper(
        job_difficulty, days, 1, 0, cache
    )
    for row in cache:
        print(row)
    return res if res is not None else -1


jobDifficulty = [6, 5, 4, 3, 2, 1]
d = 3
# jobDifficulty = [6, 5, 8]
# d = 2
jobDifficulty = [11, 111, 22, 222, 33, 333, 44, 444]
d = 6
print(find_minimum_difficulty_of_a_job_schedule(jobDifficulty, d))
