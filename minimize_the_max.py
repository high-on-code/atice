def find_the_max_min(nums, pairs):
    
    def count_valid_pairs(thresh):
        idx, count = 0, 0
        while idx < len(nums) - 1:
            if nums[idx + 1] - nums[idx] <= thresh:
                count += 1
                idx += 1
            idx += 1
        return count
    nums.sort()
    print(nums)
    minn, maxx = 0, nums[-1] - nums[0]
    
    while minn < maxx:
        mid = (minn + maxx) // 2
        if count_valid_pairs(mid) >= pairs:
            maxx = mid
        else:
            minn = mid + 1
    return minn
nums = [10,1,2,7,1,3]
p = 2
print(find_the_max_min(nums, p))