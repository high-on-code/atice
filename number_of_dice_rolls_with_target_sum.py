def find_number_of_dice_rolls_with_target_sum(count, faces, target):
    cache = [[0] * (target + 1) for _ in range(count)]

    for face in range(1, min(faces + 1, target + 1)):
        cache[0][face] = 1
    # print("*" * 80)
    # print("ironman cache", cache)
    for current_dice_count in range(1, count):
        for current_target in range(1, target + 1):
            for one_less_target in range(
                current_target - 1, max(current_target - faces - 1, -1), -1
            ):
                cache[current_dice_count][current_target] += cache[
                    current_dice_count - 1
                ][one_less_target]
    return cache[count - 1][target] % ((10 ** 9) + 7)


n = 1
k = 6
target = 3
n = 2
k = 6
target = 7
n = 30
k = 30
target = 500
print(find_number_of_dice_rolls_with_target_sum(n, k, target))
