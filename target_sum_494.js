var findTargetSumWays = function(nums, target) {
    const cache = new Map();
    findTargetSumWaysRecur(0, 0, target, cache, nums)
    const mapKey = [0, 0]
    console.log(cache.get(mapKey))
    console.log(cache)
    return cache.get(convertKey(0, 0));
};

const findTargetSumWaysRecur = (currSum, index, target, cache, nums) => {
    if (index == nums.length) {
        if (currSum == target) {
            return 1
        } else {
            return 0;
        }
    }
    const mapKey = convertKey(currSum, index);
    console.log(mapKey)
    if(cache.has(mapKey)) {
        return cache.get(mapKey)
    }
    let res = findTargetSumWaysRecur(currSum + nums[index], index + 1, target, cache, nums) + findTargetSumWaysRecur(currSum - nums[index], index + 1, target, cache, nums);
    cache.set(mapKey, res)
    return res
}

const convertKey = (val1, val2) => {
    return `${val1.toString()}-${val2.toString()}`
}

let nums = [1,1,1,1,1]
let target = 3
// let nums = [1]
// let target = 1
console.log(findTargetSumWays(nums, target))