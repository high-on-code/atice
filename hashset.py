# https://leetcode.com/problems/design-hashset/
class MyHashSet:
    def __init__(self):
        self.key_range = 769
        self.bucket_array = [Bucket() for _ in range(self.key_range)]

    def _hash(self, value):
        return value % self.key_range

    def add(self, value):
        bucket_index = self._hash(value)
        self.bucket_array[bucket_index].insert(value)

    def remove(self, value):
        bucket_index = self._hash(value)
        self.bucket_array[bucket_index].delete(value)

    def contains(self, value):
        bucket_index = self._hash(value)
        return self.bucket_array[bucket_index].exists(value)

class Bucket:
    def __init__(self):
        self.head = Node(0)

    def insert(self, value):
        if self.exists(value):
            return None
        new_node = Node(value, self.head.next_node)
        self.head.next_node = new_node

    def delete(self, value):
        curr = self.head
        next = self.head.next_node
        while next is not None:
            if next.value == value:
                curr.next_node = next.next_node
                return
            curr = next
            next = next.next_node

    def exists(self, value):
        curr = self.head.next_node
        while curr is not None:
            if curr.value == value:
                return True
            curr = curr.next_node
        return False


class Node:
    def __init__(self, value, next_node=None):
        self.value = value
        self.next_node = next_node
