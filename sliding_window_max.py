# https://leetcode.com/problems/sliding-window-maximum/description/
from collections import deque


def find_slinding_win_max(nums, k):
    qu = deque()
    res = []

    for idx in range(k):
        while qu and nums[idx] >= nums[qu[-1]]:
            qu.pop()
        qu.append(idx)

    res.append(nums[qu[0]])
    for idx in range(k, len(nums)):
        while qu and qu[0] <= idx - k:
            qu.popleft()
        while qu and nums[idx] >= nums[qu[-1]]:
            qu.pop()
        qu.append(idx)

        res.append(nums[qu[0]])
    return res


nums = [1, 3, -1, -3, 5, 3, 6, 7]
k = 3
nums =[1,-1]
k = 1
print(find_slinding_win_max(nums, k))
