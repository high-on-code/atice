/**
 * @param {number[]} nums
 * @return {boolean}
 */
var check = function(nums) {
    let breaks = 0;
    for (let i = 0; i < nums.length - 1; i++) {
        if (nums[i] > nums[i + 1]) {
            breaks++;
            if (breaks > 2) {
                return false;
            }
        }
    }
    if (nums[0] < nums.at(-1)) {
        breaks++;
    }
    return breaks <= 1;
};
let nums = [3,4,5,1,2];
nums = [2,1,3,4];
console.log(check(nums));