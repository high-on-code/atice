# https://leetcode.com/problems/predict-the-winner/
def predict_the_winner_recur(nums, start, end, cache):
    if start == end:
        return nums[start]
    if cache[start][end] is not None:
        return cache[start][end]
    res = max(nums[start] - predict_the_winner_recur(nums, start + 1, end, cache), nums[end] - predict_the_winner_recur(nums, start, end - 1, cache))
    cache[start][end] = res
    return res

def predict_the_winner(nums):
    numlen = len(nums)
    cache = [[None] * numlen for _ in range(numlen)]
    print(cache)
    return predict_the_winner_recur(nums, 0, numlen - 1, cache) >= 0
    
nums = [1,5,2]
nums = [1,5,233,7]
print(predict_the_winner(nums))