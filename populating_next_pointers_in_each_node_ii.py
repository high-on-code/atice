def process_child(child, prev, left_most):
    if child:
        if prev:
            prev.next = child
        else:
            left_most = child
    prev = child
    return prev, left_most


def populate_next_pointers(root):
    if not root:
        return root
    left_most = root
    while left_most:
        prev, curr = None, left_most
        left_most = None
        while curr:
            prev, left_most = process_child(curr.left, prev, left_most)
            prev, left_most = process_child(curr.right, prev, left_most)
            curr = curr.next
    return root
