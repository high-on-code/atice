
var findMaxFish = function(grid) {


function findParent(cell) {
    if (parentCell[cell] === cell) return cell;
     parentCell[cell] = findParent(parentCell[cell]);
     return parentCell[cell];
}

function combineComps(cell, neighbor) {
    const parent1 = findParent(cell);
    const parent2 = findParent(neighbor);
    if (parent1 === parent2) return;
    if (compSize[parent1] < compSize[parent2]) {
        parentCell[parent1] = parent2;
        compSize[parent2] += compSize[parent1];
        totalFish[parent2] += totalFish[parent1];
    } else {
        parentCell[parent2] = parent1;
        compSize[parent1] += compSize[parent2];
        totalFish[parent1] += totalFish[parent2];
    }
}

    const rows = grid.length;
    const cols = grid[0].length;
    const totalCells = rows * cols;
    // initialize array with index as value
    
    const parentCell = new Array(totalCells).fill(0).map((_, i) => i);
    const compSize = new Array(totalCells).fill(1);
    const totalFish = new Array(totalCells).fill(0);

    for (let row = 0; row < rows; row++) {
        for (let col = 0; col < cols; col++) {
            const cell = row * cols + col;
            totalFish[cell] = grid[row][col];
        }
    }
    const directions = [[0, 1], [0, -1], [1, 0], [-1, 0]];
    for (let row = 0; row < rows; row++) {
        for (let col = 0; col < cols; col++) {
            if (grid[row][col] === 0) {
                continue;
            }
            const cell = row * cols + col;
            for (const [dr, dc] of directions) {
                const r = row + dr;
                const c = col + dc;
                if (r < 0 || r >= rows || c < 0 || c >= cols || grid[r][c] === 0) {
                    continue;
                }
                const neighbor = r * cols + c;
                combineComps(cell, neighbor);
            }
        }
    }
    // find the largest value of totalFish
    return Math.max(...totalFish);
};

const grid = [[0,2,1,0],[4,0,0,3],[1,0,0,4],[0,3,2,0]];
console.log(findMaxFish(grid));
