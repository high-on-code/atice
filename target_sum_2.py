def get_target_sum(nums, cidx, csum, target, cache):
    total = sum(nums)
    if cidx == len(nums):
        return 1 if target == csum else 0
    elif cache[cidx][csum + total] != -float('inf'):
        # res = cache[cidx][total]
        return cache[cidx][csum + total]
    add = get_target_sum(nums, cidx + 1, csum + nums[cidx], target, cache)
    sub = get_target_sum(nums, cidx + 1, csum - nums[cidx], target, cache)
    cache[cidx][csum + total] = add + sub
    return cache[cidx][csum + total]


def find_target_sum_ways(nums, target):
    total = sum(nums)
    cache = [[-float('inf')] * ((2 * total) + 1) for _ in range(len(nums))]
    # print(cache)
    return get_target_sum(nums, 0, 0, target, cache)


nums = [1, 1, 1, 1, 1]
target = 3
print(find_target_sum_ways(nums, 3))
