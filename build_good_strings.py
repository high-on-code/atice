# https://leetcode.com/problems/count-ways-to-build-good-strings
def count_ways(low, high, zero, one):
    dp_cache = [1] + ([0] * high)
    for idx in range(1, high + 1):
        if idx >= zero:
            dp_cache[idx] += dp_cache[idx - zero]
        if idx >= one:
            dp_cache[idx] += dp_cache[idx - one]
    return sum(dp_cache[low: high + 1])

low = 3
high = 3
zero = 1
one = 1
print(count_ways(low, high, zero, one))