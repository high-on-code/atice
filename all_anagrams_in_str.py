from collections import Counter, defaultdict


def check_eq(d1, d2):
    for k, v in d1.items():
        if v > 0:
            if k not in d2 or d2[k] != v:
                return False
    for k, v in d2.items():
        if v > 0:
            if k not in d1 or d1[k] != v:
                return False
    return True


def find_all_anagrams(s1, s2):
    s1len = len(s1)
    s2len = len(s2)
    s2_ctr = Counter(s2)
    res = []
    s1_ctr = defaultdict(lambda: 0)
    for idx in range(s2len):
        s1_ctr[s1[idx]] += 1
    # print("*" * 80)
    # print("ironman s1_ctr", s1_ctr)
    for idx in range(s2len, s1len):
        if check_eq(s1_ctr, s2_ctr):
            res.append(idx - s2len)

        s1_ctr[s1[idx - s2len]] -= 1
        s1_ctr[s1[idx]] += 1
    if check_eq(s1_ctr, s2_ctr):
        res.append(idx - s2len + 1)
    return res


s = "cbaebabacd"
p = "abc"
# s = "abab"
# p = "ab"
# s = "aa"
# p = "bb"
print(find_all_anagrams(s, p))
