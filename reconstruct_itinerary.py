from collections import defaultdict


def reconstruct_itinerary_recur(graph, location, visited, res, stops):
    # print("*" * 80)
    # print("ironman res, visited", res, visited)
    if len(res) + 1 == stops:
        return res
    for idx, dest in enumerate(graph[location]):
        if not visited[location][idx]:
            visited[location][idx] = True
            res.append(dest)
            op = reconstruct_itinerary_recur(graph, dest, visited, res, stops)
            if not op:
                visited[location][idx] = False
                res.pop()
            else:
                return res


def reconstruct_itinerary(tickets):
    graph = defaultdict(list)
    for to, fro in tickets:
        graph[to].append(fro)
    for to, dest in graph.items():
        dest.sort()
    stops = len(tickets) + 1
    visited = {key: [False] * len(val) for key, val in graph.items()}
    res = ["JFK"]
    reconstruct_itinerary_recur(graph, "JFK", visited, res, stops)
    return res
    # print("*" * 80)
    # print("ironman dest", graph)


tickets = [["MUC", "LHR"], ["JFK", "MUC"], ["SFO", "SJC"], ["LHR", "SFO"]]
print(reconstruct_itinerary(tickets))
