from collections import defaultdict


def get_target_pos_helper(val_list, st, ed, tar_pos):
    mid = (st + ed) // 2


def get_target_pos(val_list, tar_pos):
    return get_target_pos_helper(val_list, 0, len(val_list), tar_pos)


def find_if_subsequnce(source, target):
    char_idx = defaultdict(list)
    for idx, val in enumerate(target):
        char_idx[val].append(idx)
    tar_pos = 0
    for val in source:
        if val not in char_idx:
            return False
        elif tar_pos > char_idx[val][-1]:
            return False
        else:
            tar_pos = get_target_pos(char_idx[val], tar_pos)
