def is_graph_bipartite(graph):
    colors = {}
    for idx in range(len(graph)):
        if idx not in colors:
            stack = [idx]
            colors[idx] = 0
            while stack:
                idx = stack.pop()
                for nei in graph[idx]:
                    if nei not in colors:
                        colors[nei] = colors[idx] ^ 1
                        stack.append(nei)
                    elif colors[nei] == colors[idx]:
                        return False
    return True
