from collections import defaultdict


def find_max_prod_of_word_lengths(words):
    hashmap = defaultdict(lambda: 0)
    for aword in words:
        bitmask = 0
        for ch in aword:
            bitmask |= 1 << ord(ch) - ord("a")
        if hashmap[bitmask] < len(aword):
            hashmap[bitmask] = len(aword)
    bitmaps = list(hashmap.keys())
    max_prod = 0
    for idx in range(len(bitmaps)):
        bitmap1 = bitmaps[idx]
        for idx2 in range(idx + 1, len(bitmaps)):
            bitmap2 = bitmaps[idx2]
            if bitmap1 & bitmap2 == 0:
                max_prod = max(hashmap[bitmap1] * hashmap[bitmap2], max_prod)
    return max_prod


words = ["abcw", "baz", "foo", "bar", "xtfn", "abcdef"]
words = ["a", "ab", "abc", "d", "cd", "bcd", "abcd"]
print(find_max_prod_of_word_lengths(words))
