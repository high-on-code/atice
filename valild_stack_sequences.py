# https://leetcode.com/problems/validate-stack-sequences/solution/
def find_if_stack_seq_is_valid(pushed, popped):
    stack = []
    pop_idx = 0
    for val in pushed:
        if val != popped[pop_idx]:
            stack.append(val)
        else:
            pop_idx += 1
            while stack and pop_idx < len(popped) and popped[pop_idx] == stack[-1]:
                stack.pop()
                pop_idx += 1
    return not stack


pushed = [1, 2, 3, 4, 5]
popped = [4, 5, 3, 2, 1]
pushed = [1, 2, 3, 4, 5]
popped = [4, 3, 5, 1, 2]
pushed = [1, 0]
popped = [1, 0]
print(find_if_stack_seq_is_valid(pushed, popped))
