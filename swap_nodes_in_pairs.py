# https://leetcode.com/problems/swap-nodes-in-pairs/
def swap_nodes_in_pairs(head):
    if not head or not head.next:
        return head
    
    p1 = head
    p2 = head.next
    while True:
        p1.val, p2.val = p2.val, p1.val
        if p2.next and p2.next.next:
            p1 = p1.next.next
            p2 = p1.next
        else:
            break
    return head