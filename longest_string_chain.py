from collections import defaultdict


def longest_string_chain_recur(word, word_set, word_res):
    if word in word_res:
        return word_res[word]
    curr_size = 0

    for idx in range(len(word)):
        new_word = word[:idx] + word[idx + 1 :]
        if new_word in word_set:
            curr_size = max(
                curr_size,
                longest_string_chain_recur(new_word, word_set, word_res),
            )
    word_res[word] = 1 + curr_size
    return word_res[word]


def longest_string_chain(words):
    word_set = set(words)
    word_res = defaultdict(lambda: 1)
    res = 0
    for aword in words:
        res = max(longest_string_chain_recur(aword, word_set, word_res), res)
    return res


words = ["a", "b", "ba", "bca", "bda", "bdca"]
# words = ["a", "ab"]
words = ['a']
print(longest_string_chain(words))
