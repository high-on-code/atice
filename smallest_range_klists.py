import sys
def change_to_ranges(nums):
    for idx, alist in enumerate(nums):
        nums[idx] = [alist[0], alist[-1]]
        print(alist)
    return nums
def find_smallest_range(nums):
    nums = change_to_ranges(nums)
    print("nums", nums)
    klists = nums
    num_lists = len(klists)
    rgmax, rgmin = -sys.maxsize, sys.maxsize
    for alist in klists:
        st, ed = alist[0], alist[-1]
        rgmax = max(rgmax, ed)
        rgmin = min(rgmin, st)
    continue_iteration = True
    while rgmin < rgmax and continue_iteration:
        new_rgmin = rgmin + 1
        for  alist in klists:
            if not alist[0] <= new_rgmin <= alist[1]:
                continue_iteration = False
                break
        else:
            rgmin = new_rgmin
    continue_iteration = True
    while rgmin < rgmax and continue_iteration:
        new_rgmax = rgmax - 1
        for  alist in klists:
            if not alist[0] <= new_rgmax <= alist[1]:
                continue_iteration = False
                break
        else:
            rgmax = new_rgmax
    return rgmin, rgmax
nums = [[4,10,15,24,26],[0,9,12,20],[5,18,22,30]]
print(find_smallest_range(nums))