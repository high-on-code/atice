var eventualSafeNodes = function(graph) {
    const nodeCount = graph.length;
    // create an adjacency list
    const adjList = Array.from({ length: nodeCount }, () => []);
    // create array indegree to store indegree of each node
    const inDegree = Array.from({ length: nodeCount }, () => 0);
    // create reverse graph for [[1,2],[2,3],[5],[0],[5],[],[]]
    for (let i = 0; i < nodeCount; i++) {

        for (let j = 0; j < graph[i].length; j++) {
            adjList[graph[i][j]].push(i);
            inDegree[i]++;
        }
    }
    // initialze queue to store nodes with 0 inDegree
    const queue = [];
    // iterate through inDegree array to find nodes with 0 inDegree
    for (let i = 0; i < nodeCount; i++) {
        if (inDegree[i] === 0) {
            queue.push(i);
        }
    }
    const safeNodes = [];
    while (queue.length) {
        const node = queue.shift();
        safeNodes.push(node);
        for (let i = 0; i < adjList[node].length; i++) {
            inDegree[adjList[node][i]]--;
            if (inDegree[adjList[node][i]] === 0) {
                queue.push(adjList[node][i]);
            }
        }
    }
    // sort safeNodes array
    safeNodes.sort((a, b) => a - b);
    return safeNodes;
};
let graph = [[1,2],[2,3],[5],[0],[5],[],[]];
console.log(eventualSafeNodes(graph));