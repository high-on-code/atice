from collections import deque


def find_all_paths_from_source_to_target(graph):
    if not graph:
        return None
    target = len(graph) - 1
    qu = deque()
    qu.append([0])
    paths = []
    while qu:
        cpath = qu.popleft()
        cnode = cpath[-1]
        for nei in graph[cnode]:
            new_path = cpath + [nei]
            if nei == target:
                paths.append(new_path)
            else:
                qu.append(new_path)

    return paths


graph = [[1, 2], [3], [3], []]
print(find_all_paths_from_source_to_target(graph))
