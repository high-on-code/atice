def find_k_weakest_rows(matrix, k):
    row_list = []
    for rid, row in enumerate(matrix):
        row_list.append((row.count(1), rid))
    # print("*" * 80)
    # print("ironman row_list", row_list)
    row_list.sort()
    return [j for (i, j) in row_list][:k]


mat = (
    [
        [1, 1, 0, 0, 0],
        [1, 1, 1, 1, 0],
        [1, 0, 0, 0, 0],
        [1, 1, 0, 0, 0],
        [1, 1, 1, 1, 1],
    ],
)
k = 3
print(find_k_weakest_rows(mat, k))
