def find_dice_rolls_with_target_sum_recur(num_dice, faces, target, mem):
    if num_dice == 0 and target == 0:
        return 1
    elif num_dice == 0:
        return 0
    if (num_dice, target) in mem:
        return mem[(num_dice, target)]
    res = 0
    for val in range(1, faces + 1):
        if target - val < 0:
            break
        res += find_dice_rolls_with_target_sum_recur(
            num_dice - 1, faces, target - val, mem
        )
    mem[(num_dice, target)] = res
    return res


def find_dice_rolls_with_target_sum(num_dice, faces, target):
    mem = {}
    return find_dice_rolls_with_target_sum_recur(num_dice, faces, target, mem) % (
        (10 ** 9) + 7
    )


n = 1
k = 6
target = 3
n = 2
k = 6
target = 7

print(find_dice_rolls_with_target_sum(n, k, target))
