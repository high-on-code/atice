# https://leetcode.com/problems/candy/
def count(num):
    return num * (num + 1) // 2


def distribute_candies(ratings):
    ratings_len = len(ratings)
    if ratings_len <= 1:
        return ratings_len
    candies = 0
    up = 0
    down = 0
    old_slope = 0
    for idx in range(1, ratings_len):
        diff = ratings[idx] - ratings[idx - 1]
        if diff > 0:
            new_slope = 1
        elif diff < 0:
            new_slope = -1
        else:
            new_slope = 0
        if (old_slope > 0 and new_slope == 0) or (old_slope < 0 and new_slope >= 0):
            candies += count(up) + count(down) + max(up, down)
            up, down = 0, 0
        if new_slope > 0:
            up += 1
        elif new_slope < 0:
            down += 1
        else:
            candies += 1
        old_slope = new_slope
    candies += count(up) + count(down) + max(up, down) + 1
    return candies


ratings = [1, 0, 2]
ratings = [1, 3, 2, 2, 1]
ratings = [5, 4, 3, 3, 2, 1]
print(distribute_candies(ratings))
