def find_contiguos_array(nums):
    num_len = len(nums)
    cache = [-2] * (num_len * 2 + 1)
    cache[num_len] = -1
    count = 0
    max_len = 0
    for idx, val in enumerate(nums):
        if val == 1:
            count += 1
        else:
            count -= 1
        translated_idx = count + num_len
        if cache[translated_idx] >= -1:
            max_len = max(idx - cache[translated_idx], max_len)
        else:
            cache[translated_idx] = idx
    return max_len


nums = [0, 1]
print(find_contiguos_array(nums))
