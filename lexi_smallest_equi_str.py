def get_ch_idx(ch):
    return ord(ch) - ord("a")


def dfs(src, adj_mat, visited, components, min_char):
    visited[src] = 1
    components.append(src)
    min_char = min(min_char, src)
    for idx in range(26):
        if adj_mat[src][idx] == 1 and visited[idx] == 0:
            min_char = min(dfs(idx, adj_mat, visited, components, min_char), min_char)
    return min_char


def find_lex_smallest_equ_str(str1, str2, base_str):
    adj_mat = []
    for idx in range(26):

        adj_mat.append([0] * 26)
    # print(adj_mat)
    for c1, c2 in zip(str1, str2):
        st1_idx = get_ch_idx(c1)
        st2_idx = get_ch_idx(c2)
        adj_mat[st1_idx][st2_idx] = 1
        adj_mat[st2_idx][st1_idx] = 1
    mapping_char = [idx for idx in range(26)]
    visited = [0] * 26

    # print(mapping_char)
    # print(visited)
    for idx in range(26):
        if visited[idx] == 0:
            components = []
            min_char = 27
            min_char = dfs(idx, adj_mat, visited, components, min_char)
            for idx2 in components:
                mapping_char[idx2] = min_char
    # print("iron man mapping_char", mapping_char)
    ans = []
    for ch in base_str:
        ans.append(chr(ord("a") + mapping_char[get_ch_idx(ch)]))
    return "".join(ans)


s1 = "parker"
s2 = "morris"
baseStr = "parser"
print(find_lex_smallest_equ_str(s1, s2, baseStr))
