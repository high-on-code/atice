import bisect
def marr_arr_increasing_recur(cache, idx, prev, arr1, arr2):
    if idx == len(arr1):
        return 0
    if (idx, prev) in cache:
        return cache[(idx, prev)]
    cost = float('inf')
    if arr1[idx] > prev:
        cost = marr_arr_increasing_recur(cache, idx + 1, arr1[idx], arr1, arr2)
    idx2 = bisect.bisect_right(arr2, prev)
    if idx2 < len(arr2):
        cost = min(cost, 1 + marr_arr_increasing_recur(cache, idx + 1, arr2[idx2], arr1, arr2))
    cache[(idx, prev)] = cost
    return cost


def make_array_increasing(arr1, arr2):
    cache = {}
    arr2.sort()
    return marr_arr_increasing_recur(cache, 0, -1, arr1, arr2)  

arr1 = [1,5,3,6,7]
arr2 = [1,3,2,4]
arr1 = [1,5,3,6,7]
arr2 = [4,3,1]
arr1 = [1,5,3,6,7]
arr2 = [1,6,3,3]

print(make_array_increasing(arr1, arr2))