def check_permutation_in_string(s1, s2):
    if len(s1) > len(s2):
        return False
    s1_freq = [0] * 26
    for ch in s1:
        s1_freq[ord(ch) - ord("a")] += 1
    s2_freq = [0] * 26
    s1_len = len(s1)
    for ch in s2[:s1_len]:
        s2_freq[ord(ch) - ord("a")] += 1
    s2_len = len(s2)
    for idx in range(s2_len - s1_len):
        if s1_freq == s2_freq:
            return True
        s2_freq[ord(s2[idx]) - ord("a")] -= 1
        s2_freq[ord(s2[idx + len(s1)]) - ord("a")] += 1
    if s1_freq == s2_freq:
        return True
    return False


# s1 = "ab"
# s2 = "eidbaooo"
s1 = "ab"
s2 = "eidboaoo"
# "ab"
# "eidbaooo"
print(check_permutation_in_string(s1, s2))
