from collections import deque


def level_traverse(root):
    if not root:
        return None
    qu = deque()
    qu.append(root)
    res = []
    while qu:
        wall_len = len(qu)
        curr_wall = []
        for _ in range(wall_len):
            node = qu.popleft()
            curr_wall.append(node.val)
            if node.left:
                qu.append(node.left)
            if node.right:
                qu.append(node.right)
        res.append(curr_wall)
    return res
