INFI = float("inf")


def find_unsorted_subarray(nums):
    min_val = INFI
    max_val = -INFI
    lflag = False
    numlen = len(nums)
    for idx in range(1, numlen):
        val = nums[idx]
        if nums[idx - 1] > nums[idx]:
            lflag = True
        if lflag and val < min_val:
            min_val = val
    rflag = False
    numlen = len(nums)
    for idx in range(numlen - 1, 0, -1):
        val = nums[idx - 1]
        if nums[idx - 1] > nums[idx]:
            rflag = True
        if rflag and val > max_val:
            max_val = val
    left = right = 0
    if lflag:
        left = 0
        for idx, val in enumerate(nums):
            if val > min_val:
                left = idx
                break
    if rflag:
        right = len(nums)
        for idx in range(len(nums) - 1, -1, -1):
            val = nums[idx]
            if val < max_val:
                right = idx
                break
    return right - left + 1 if (lflag or rflag) else 0


nums = [2, 6, 4, 8, 10, 9, 15]
# nums = [1, 2, 3, 4]
nums = [2,1]
print(find_unsorted_subarray(nums))
