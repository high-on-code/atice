# https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal
from collections import deque


def trav_zigzag(root):
    deq = deque()
    rev = True
    deq.append(root)
    deq.append(None)
    res = []
    row = deque()
    while deq:
        cn = deq.popleft()

        if not cn:
            rev = not rev
            res.append(row)
            row = deque()
        if rev:
            row.appendleft(cn.val)
        else:
            row.append(cn.val)
        if cn.left:
            deq.append(cn.left)
        if cn.right:
            deq.append(cn.right)
    return res
