# https://leetcode.com/problems/minimum-domino-rotations-for-equal-row/
from collections import defaultdict


def find_min_domino_rotations(tops, bottoms):
    top_count_dict = defaultdict(lambda: 0)
    bottom_count_dict = defaultdict(lambda: 0)

    lenn = len(tops)
    double_occurence = 0
    for idx in range(lenn):
        top_value = tops[idx]
        bottom_value = bottoms[idx]
        if top_value == bottom_value:
            double_occurence += 1
        top_count_dict[tops[idx]] += 1
        bottom_count_dict[bottoms[idx]] += 1
    for top_val, freq in top_count_dict.items():
        bott_freq = bottom_count_dict[top_val]
        if bott_freq + freq - double_occurence >= lenn:
            return lenn - max(freq, bott_freq)
    return -1


tops = [2, 1, 2, 4, 2, 2]
bottoms = [5, 2, 6, 2, 3, 2]
# tops = [3, 5, 1, 2, 3]
# bottoms = [3, 6, 3, 3, 4]
# tops = [1, 2, 1, 1, 1, 2, 2, 2]
# bottoms = [2, 1, 2, 2, 2, 2, 2, 2]
print(find_min_domino_rotations(tops, bottoms))
