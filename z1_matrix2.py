from collections import deque


def find_nearest_0_helper(mat, rid, cid, dist_mat, visited):
    if mat[rid][cid] == 0:
        return 0
    qu = deque()
    qu.append((rid, cid))
    visited.add((rid, cid))
    distance = 0
    while qu:
        wall_len = len(qu)
        distance += 1

        while wall_len:
            ridx, cidx = qu.popleft()
            wall_len -= 1
            dists = ((0, 1), (0, -1), (1, 0), (-1, 0))
            for dx, dy in dists:
                nx, ny = dx + ridx, dy + cidx
                if 0 <= nx < len(mat) and 0 <= ny < len(mat[0]):
                    if mat[nx][ny] == 0:
                        dist_mat[rid][cid] = distance
                        return
                    elif (nx, ny) not in visited:
                        qu.append((nx, ny))
                        visited.add((nx, ny))


def find_nearest_0(mat):
    # (Better) (Notice)the matrix itselft holding distance so no dist variable
    dist_mat = [[float('inf')] * len(mat[0]) for _ in range(len(mat))]
    qu = deque()
    for rid, row in enumerate(mat):
        for cid, val in enumerate(row):
            if val == 0:
                qu.append((rid, cid))
                dist_mat[rid][cid] = 0
    distance = 0
    while qu:
        # wall_len = len(qu)
        distance += 1
        # (Better) wall_len is also not necessary as we are storing distance
        # while wall_len:
        rid, cid = qu.popleft()
        dists = ((0, 1), (0, -1), (1, 0), (-1, 0))
        for dx, dy in dists:
            nx, ny = dx + rid, dy + cid
            if 0 <= nx < len(mat) and 0 <= ny < len(mat[0]):
                # (Better)No need for visited because of this condition
                if dist_mat[nx][ny] > (dist_mat[rid][cid] + 1):
                    dist_mat[nx][ny] = dist_mat[rid][cid] + 1
                    qu.append((nx, ny))
    return dist_mat


# mat = [[0, 0, 0], [0, 1, 0], [0, 0, 0]]
# mat = [[0, 0, 0], [0, 1, 0], [1, 1, 1]]
mat = [[0], [0], [0], [0], [0]]
mat = [
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1],
    [0, 0, 0],
]
# mat = [[1, 1, 1], [1, 1, 1], [0, 0, 0]]

dist_mat = find_nearest_0(mat)
print(dist_mat)
