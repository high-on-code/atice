def make_string_great(strn):
    res = [strn[0]]
    bad = False
    for idx in range(1, len(strn)):
        ch = strn[idx]
        if ch.isupper():
            if res and res[-1] == ch.lower():
                bad = True
                res.pop()
                continue
        else:
            if res and res[-1] == ch.upper():
                bad = True
                res.pop()
                continue
        res.append(ch)
    # if ch.isupper():
    #     if res and res[-1] == ch.lower():
    #         bad = True
    #         res.pop()
    # elif res and res[-1] == ch.upper():
    #     bad = True
    #     res.pop()

    # else:
    #     res.append(ch)
    # print(idx)
    # print(ch)
    # print(pch)
    return "".join(res) if bad else ""


# s = "leEeetcode"
s = "abBAcC"
print(make_string_great(s))
