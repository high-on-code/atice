# https://leetcode.com/problems/kth-missing-positive-number/
def findKthPositive(arr, k):
    cm = 0
    cnum = 1
    arr_len = len(arr)
    arr_idx = 0
    while cm < k:
        if cnum != arr[arr_idx]:
            cm += 1
        else:
            arr_idx += 1
        cnum += 1
    return cnum - 1


arr = [2, 3, 4, 7, 11]
k = 5
print(findKthPositive(arr, k))
