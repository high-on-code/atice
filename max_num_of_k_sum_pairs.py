from collections import defaultdict


def max_num_of_k_sum_pairs(nums, k):
    compl = defaultdict(lambda: 0)
    count = 0
    for val in nums:
        kcompl = k - val
        if compl[kcompl]:
            count += 1
            compl[kcompl] -= 1
        else:
            compl[val] += 1
    return count


nums = [1, 2, 3, 4]
k = 5
print(max_num_of_k_sum_pairs(nums, k))
