def find_combinations_recur(curr, fnum, nums, lent, res):
    if lent == 0:
        # print(curr)
        return res.append(curr[:])
    for idx in range(fnum + 1, nums + 1):
        curr.append(idx)
        # curr += [idx]
        find_combinations_recur(curr, idx, nums, lent - 1, res)
        curr.pop()
    
def find_combinations(nums, lent):
    res = []
    find_combinations_recur([], 0, nums, lent, res)
    return res

n = 4
k = 2
print(find_combinations(n, k))