letters = {
    "2": "abc",
    "3": "def",
    "4": "ghi",
    "5": "jkl",
    "6": "mno",
    "7": "pqrs",
    "8": "tuv",
    "9": "wxyz",
}


def find_letter_comb_of_phone_number_recur(digits, res, idx, path):
    if idx == len(digits):
        res.add("".join(path))
        return
    dig_letters = letters[digits[idx]]
    for a_letter in dig_letters:
        path.append(a_letter)
        find_letter_comb_of_phone_number_recur(digits, res, idx + 1, path)
        path.pop()


def find_letter_comb_of_phone_number(digits):
    if not digits:
        return []
    res = set()
    find_letter_comb_of_phone_number_recur(digits, res, 0, [])
    return res


digits = "23"
print(find_letter_comb_of_phone_number(digits))
