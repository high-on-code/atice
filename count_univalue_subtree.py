def count_univalue_subtrees(root):
    if not root.left and not root.right:
        return True, 1
    if root.left:
        lres, lcount = count_univalue_subtrees(root.left)
    else:
        lres, lcount = True, 0
    if root.right:
        rres, rcount = count_univalue_subtrees(root.right)
    else:
        rres, rcount = True, 0
    if lres and rres:
        if root.left and root.right:
            if root.val == root.left.val == root.right.val:
                return True, rcount + lcount + 1
            else:
                return False, rcount + lcount
        else:
            child = (root.left or root.right)
            if child.val == root.val:
                return True, rcount + lcount + 1
            else:
                return False, rcount + lcount
    else:
        return False, rcount + lcount
