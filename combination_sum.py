def find_combi_for_sum_recur(res, combi, rem, cursor, candidates):
    if rem < 0:
        return
    if rem == 0:
        res.append(combi)
    for idx in range(cursor, len(candidates)):
        curr_ele = candidates[idx]
        find_combi_for_sum_recur(
            res, combi + [curr_ele], rem - curr_ele, idx, candidates
        )


def find_combi_for_sum(candidates, target):
    res = []
    find_combi_for_sum_recur(res, [], target, 0, candidates)
    return res


candidates = [2, 3, 6, 7]
target = 7
print(find_combi_for_sum(candidates, target))
