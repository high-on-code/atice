class WordDictionary:

    def __init__(self):
        self.sett = set()

    def addWord(self, word: str) -> None:
        self.sett.add(word)
    
    def match_word(self, word):
        for matt in self.sett:
            if len(matt) != len(word):
                continue
            for ch1, ch2 in zip(matt, word):
                if ch2 == '.':
                    continue
                if ch1 != ch2:
                    break
            else:
                return True
        return False

    def search(self, word: str) -> bool:
        if '.' in word:
            return self.match_word(word)
        return word in self.sett


# Your WordDictionary object will be instantiated and called as such:
# obj = WordDictionary()
# obj.addWord(word)
# param_2 = obj.search(word)
