from collections import defaultdict


class UnionFind:
    def __init__(self, args):
        self.par = [idx for idx in range(args)]
        self.rank = [0] * args

    def find(self, x):
        if self.par[x] != x:
            self.par[x] = self.find(self.par[x])
        return self.par[x]

    def union(self, x, y):
        xset = self.find(x)
        yset = self.find(y)
        if xset == yset:
            return
        if self.rank[xset] < self.rank[yset]:
            self.par[xset] = yset
        elif self.rank[xset] > self.rank[yset]:
            self.par[yset] = xset
        else:
            self.par[xset] = yset
            self.rank[yset] += 1


def find_num_good_paths(vals, edges):
    graph = defaultdict(list)
    for u, v in edges:
        graph[u].append(v)
        graph[v].append(u)
    val_nodes = defaultdict(list)
    for ed, val in enumerate(vals):
        val_nodes[val].append(ed)
    print("iron man val_nodes", val_nodes)
    node_count = len(vals)
    uf = UnionFind(node_count)
    good_paths = 0

    for val in sorted(val_nodes.keys()):
        # print("iron man val", val)
        for node in val_nodes[val]:
            if node not in graph:
                continue
            for nei in graph[node]:
                if val_nodes[nei] >= val_nodes[nei]:
                    uf.union(node, nei)
        group = {}
        for node in val_nodes[val]:
            group[uf.find(node)] = group.get(uf.find(node), 0) + 1
        for key in group.keys():
            size = group[key]
            good_paths += (size * (size + 1)) // 2
    return good_paths


vals = [1, 3, 2, 1, 3]
edges = [[0, 1], [0, 2], [2, 3], [2, 4]]
vals = [1, 1, 2, 2, 3]
edges = [[0, 1], [1, 2], [2, 3], [2, 4]]
print(find_num_good_paths(vals, edges))
