# https://leetcode.com/problems/range-sum-of-bs
def range_sum_bst_helper(root, low, high, res):
    if not root:
        return res
    if low <= root.val <= high:
        res += root.val
    # (Notice) by adding these conditions we are only traversive paths of tree that are relavant
    if low < root.val:
        res = range_sum_bst_helper(root.left, low, high, res)
    if high > root.val:
        res = range_sum_bst_helper(root.right, low, high, res)
    return res


def range_sum_bst(root, low, high):
    return range_sum_bst_helper(root, low, high, 0)
