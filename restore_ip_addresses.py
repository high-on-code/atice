def valid(strn, st_idx, lent):
    if lent == 1:
        return True
    if strn[st_idx] == "0":
        return False
    if int(strn[st_idx : st_idx + lent]) <= 255:
        return True
    else:
        return False


def restore_ip_addresses_recur(strn, st_idx, dots, ans):
    rem_len = len(strn) - st_idx
    rem_ints = 4 - len(dots)
    if rem_len > (rem_ints * 3) or rem_len < rem_ints:
        return
    if len(dots) == 3:
        if valid(strn, st_idx, rem_len):
            res = []
            last = 0
            for ad in dots:
                res.append(strn[last : last + ad])
                last += ad
            res.append(strn[st_idx:])
            ans.append(".".join(res))
    for curr_pos in range(1, 4):
        if curr_pos > rem_len:
            break
        dots.append(curr_pos)
        if valid(strn, st_idx, curr_pos):
            restore_ip_addresses_recur(strn, st_idx + curr_pos, dots, ans)
        dots.pop()

def restore_ip_addresses(strn):
    ans = []
    restore_ip_addresses_recur(strn, 0, [], ans)
    return ans


s = "25525511135"
print(restore_ip_addresses(s))
