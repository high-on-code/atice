from collections import deque


def find_minimum_knight_moves(x, y):
    origin_queue = deque()
    origin_queue.append((0, 0, 0))
    origin_visited_nodes = {(0, 0): 0}
    dest_queue = deque()
    dest_queue.append((x, y, 0))
    dest_visited_nodes = {(x, y): 0}
    while True:

        # origin_steps += 1
        origin_wall_len = len(origin_queue)
        for _ in range(origin_wall_len):
            cx, cy, cd = origin_queue.popleft()
            if (cx, cy) in dest_visited_nodes:
                return cd + dest_visited_nodes[(cx, cy)]
            for dx, dy in (
                (1, 2),
                (-1, 2),
                (1, -2),
                (-1, -2),
                (2, 1),
                (-2, 1),
                (2, -1),
                (-2, -1),
            ):

                nx, ny = cx + dx, cy + dy
                if (nx, ny) in origin_visited_nodes:
                    continue
                else:
                    origin_queue.append((nx, ny, cd + 1))
                    origin_visited_nodes[(nx, ny)] = cd + 1
        dest_wall_len = len(dest_queue)

        for _ in range(dest_wall_len):
            cx, cy, cd = dest_queue.popleft()
            if (cx, cy) in origin_visited_nodes:
                return cd + origin_visited_nodes[(cx, cy)]
            for dx, dy in (
                (1, 2),
                (-1, 2),
                (1, -2),
                (-1, -2),
                (2, 1),
                (-2, 1),
                (2, -1),
                (-2, -1),
            ):
                nx, ny = cx + dx, cy + dy
                if (nx, ny) in dest_visited_nodes:
                    continue
                else:
                    dest_queue.append((nx, ny, cd + 1))
                    dest_visited_nodes[(nx, ny)] = cd + 1


x, y = 2, 1
# (x, y) = 2, 2
x = 5
y = 5
# x = 0
# y = 0
print(find_minimum_knight_moves(x, y))
