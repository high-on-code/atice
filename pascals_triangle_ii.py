def pascals_triangle_helper(cache, row, target):
    if row == target + 1:
        return
    cache.append([])
    for idx in range(row + 1):
        if idx == 0 or idx == row:
            cache[-1].append(1)
        else:
            cache[-1].append(cache[row - 1][idx - 1] + cache[row - 1][idx])
    pascals_triangle_helper(cache, row + 1, target)


def pascals_triangle(row):
    cache = [[1]]
    pascals_triangle_helper(cache, 1, row)
    return cache[row]


row = 3
print(pascals_triangle(row))
