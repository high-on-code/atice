# https://leetcode.com/problems/solving-questions-with-brainpower/
def sov_qs_with_brain_power_recur(questions, idx, dp_cache):
    if idx >= len(questions):
        return 0
    elif dp_cache[idx] != -1:
        return dp_cache[idx]
    dp_cache[idx] = max(questions[idx][0] +
        sov_qs_with_brain_power_recur(questions, idx + questions[idx][1] + 1, dp_cache),
        sov_qs_with_brain_power_recur(questions, idx + 1, dp_cache)
        )
    return dp_cache[idx]
    
def solve_qs_with_brain_power(questions):
    dp_cache = [-1] * len(questions)
    return sov_qs_with_brain_power_recur(questions, 0, dp_cache)
questions = [[3,2],[4,3],[4,4],[2,5]]
print(solve_qs_with_brain_power(questions))