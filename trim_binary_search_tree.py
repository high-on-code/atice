def trim_bst(root, low, high):
    if not root:
        return None
    if root.val > high:
        return trim_bst(root.left, low, high)
    elif root.val < low:
        return trim_bst(root.right, low, high)
    root.left = trim_bst(root.left, low, high)
    root.right = trim_bst(root.right, low, high)
    return root
