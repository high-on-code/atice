def get_mid(head):
    mid_prev = None
    while head and head.next:
        mid_prev = mid_prev.next if mid_prev else head
        head = head.next.next
    mid = mid_prev.next
    mid_prev.next = None
    return mid


def merge(left, right):
    dummy_head = ListNode()
    tail = dummy_head
    while left and right:
        if left.val < right.val:
            tail.next = left
        else:
            tail.next = right
    tail.next = left if left else right
    return dummy_head.next


def sort_list(head):
    if not head or not head.next:
        return head
    mid = get_mid(head)
    left = sort_list(head)
    right = sort_list(mid)
    merge(left, right)
