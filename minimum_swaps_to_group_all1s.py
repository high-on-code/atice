def find_min_swaps(data):
    window_size = 0
    for val in data:
        window_size += val
    max_window_size = 0
    current_window_size = 0
    for idx in range(len(data)):
        current_window_size += data[idx]
        if idx >= window_size:
            current_window_size -= data[idx - window_size]
        max_window_size = max(max_window_size, current_window_size)
    return window_size - max_window_size


data = [1, 0, 1, 0, 1]
data = [0, 0, 0, 1, 0]
data = [1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1]
print(find_min_swaps(data))
