from collections import defaultdict, deque


def convert_tree_to_graph_recur(node, parent, graph, leaf_nodes):
    if parent:
        graph[node].append(parent)
        graph[parent].append(node)
    if not node.left and not node.right:
        leaf_nodes.add(node)
    if node.left:
        convert_tree_to_graph_recur(node.left, node, graph, leaf_nodes)
    if node.right:
        convert_tree_to_graph_recur(node.right, node, graph, leaf_nodes)
def covert_tree_to_graph(root):
    graph = defaultdict(list)
    leaf_nodes = set()
    convert_tree_to_graph_recur(root, None, graph, leaf_nodes)
    return graph, leaf_nodes
def find_good_leaf_nodes(root, distance):
    graph, leaf_nodes = covert_tree_to_graph(root)
    ans = 0
    for node in leaf_nodes:
        visited = set([node])
        wall = deque()
        wall.append((node))
        curr_distance = 0
        while wall and curr_distance < distance:
            wall_len = len(wall)
            for _ in range(wall_len):
                curr_node = wall.popleft()

                for nei in graph[curr_node]:
                    if nei not in visited:
                        if nei in leaf_nodes:
                            ans += 1
                        wall.append(nei)
                        visited.add(nei)

            curr_distance += 1
    return ans // 2