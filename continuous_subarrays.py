def find_combs(st_idx, ed_idx):
    window = ed_idx - st_idx + 1
    return window * (window + 1) / 2
def find_continuous_subarray2(nums):
    num_len = len(nums)
    st_idx = 0
    num_max, num_min = nums[0], nums[0]
    res = 0
    end_idx = 0
    while end_idx < num_len:
        num_max = max(num_max, nums[end_idx])
        num_min = min(num_min, nums[end_idx])

        if num_max - num_min > 2:
            res += find_combs(st_idx, end_idx - 1)
            st_idx += 1
            end_idx = st_idx
            num_max, num_min = nums[end_idx], nums[end_idx]
        else:
            end_idx += 1
    res += find_combs(st_idx, end_idx - 1)
    return res
def find_continuous_subarray(nums):
    num_len = len(nums)
    res = num_len
    for idx in range(num_len - 1):
        num_max, num_min = nums[idx], nums[idx]
        for idx2 in range(idx + 1, num_len):
            num_max = max(num_max, nums[idx2])
            num_min = min(num_min, nums[idx2])
            if num_max - num_min <= 2:
                res += 1
            else:
                break
    return res

nums = [5,4,2,4]
# nums = [1,2,3]

print(find_continuous_subarray2(nums))