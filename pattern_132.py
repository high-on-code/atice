def check_132patter(nums):
    minj = []
    minj.append(nums[0])
    for idx in range(1, len(nums)):
        minj.append(min(minj[idx - 1], nums[idx]))
    mono_rep = []
    for idx in range(len(nums) - 1, -1, -1):
        while mono_rep and mono_rep[-1] < nums[idx]:
            if minj[idx] < mono_rep[-1]:
                return True
            mono_rep.pop()
        mono_rep.append(nums[idx])
    return False

nums = [1,2,3,4]
nums = [3,1,4,2]
nums = [1,0,1,-4,-3]
print(check_132patter(nums))