from functools import lru_cache


@lru_cache(maxsize=None)
def find_ways_to_decode_recur(idx, decoded):
    if idx == len(decoded):
        return 1
    elif decoded[idx] == "0":
        return 0
    elif idx == len(decoded) - 1:
        return 1
    ans = find_ways_to_decode_recur(idx + 1, decoded)
    if int(decoded[idx : idx + 2]) <= 26:
        ans += find_ways_to_decode_recur(idx + 2, decoded)
    return ans


def find_ways_to_decode(decoded):
    return find_ways_to_decode_recur(0, decoded)


s = "12"
print(find_ways_to_decode(s))
