def tree_construct_helper(inorder_map, postorder, lefb, rib):
    if lefb > rib:
        return None
    root_val = postorder.pop()
    root = TreeNode(root_val)
    root.right = tree_construct_helper(
        inorder_map, postorder, inorder_map[root_val] + 1, rib
    )
    root.left = tree_construct_helper(
        inorder_map, postorder, lefb, inorder_map[root_val] - 1
    )
    return root


def tree_construct(inorder, postorder):
    inorder_map = {val: idx for idx, val in enumerate(inorder)}
    return tree_construct_helper(inorder_map, postorder, 0, len(postorder) - 1)


inorder = [9, 3, 15, 20, 7]
postorder = [9, 15, 7, 20, 3]
print(tree_construct(inorder, postorder))
