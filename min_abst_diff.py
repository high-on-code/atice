def find_min_absolute_diff_in_bst_recur(root, min_diff):
    if not root:
        return min_diff
    if root.left:
        min_diff = min(min_diff, abs(root.val - root.left.val))
        min_diff = min(min_diff, find_min_absolute_diff_in_bst_recur(root.left, min_diff))
        
    if root.right:
        min_diff = min(min_diff, abs(root.val - root.right.val))
        min_diff =  min(min_diff, find_min_absolute_diff_in_bst_recur(root.right, min_diff))
    
    return min_diff
def find_min_absolute_diff_in_bst(root):
    min_diff = float('inf')
    return find_min_absolute_diff_in_bst_recur(root, min_diff)