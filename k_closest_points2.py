import heapq


def find_k_closest_points(points, k):
    points_heap = [
        (-(point[0] ** 2 + point[1] ** 2), idx) for idx, point in enumerate(points[:k])
    ]
    heapq.heapify(points_heap)
    for idx in range(k, len(points)):
        x, y = points[idx]
        point_dist = -(x ** 2 + y ** 2)
        if point_dist > points_heap[0][0]:
            heapq.heappushpop(points_heap, (point_dist, idx))
    return [points[point[1]] for point in points_heap]


points = [[1, 3], [-2, 2]]
k = 1
print(find_k_closest_points(points, k))
