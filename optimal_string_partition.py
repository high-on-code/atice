# https://leetcode.com/problems/optimal-partition-of-string
def trans_ch(ch):
    return  ord(ch) - ord("a")


class Solution:
    def partitionString(self, s: str) -> int:
        cut = 1
        ch_array = [-1] * 26
        st_idx = 0
        for idx, val in enumerate(s):

            if ch_array[trans_ch(val)] - st_idx >= 0:
                cut += 1

                st_idx = idx
            ch_array[trans_ch(val)] = idx
        return cut


s = "abacaba"
print(Solution().partitionString(s))
