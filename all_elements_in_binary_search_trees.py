def inorder_bst(root):
    if not root:
        return None
    stack = []
    visited = set()
    stack.append(root)
    visited.add(root)
    while stack:
        if stack[-1].left and stack[-1].left not in visited:
            node = stack[-1].left
            stack.append(node)
            visited.add(node)
        else:
            node = stack.pop()
            yield node.val
            if node.right:
                stack.append(node.right)


def get_all_elements(root1, root2):
    tree1_gen = inorder_bst(root1)
    tree2_gen = inorder_bst(root2)
    result_list = []
    tree1_list = list(tree1_gen)
    # print(tree1_list)
    tree2_list = list(tree2_gen)
    # print(tree2_list)
    if not root1:
        return tree2_list
    if not root2:
        return tree1_list
    idx1, idx2 = 0, 0
    while idx1 < len(tree1_list) and idx2 < len(tree2_list):
        if tree1_list[idx1] < tree2_list[idx2]:
            result_list.append(tree1_list[idx1])
            idx1 += 1
        else:
            result_list.append(tree2_list[idx2])
            idx2 += 1
    if idx1 < len(tree1_list):
        return result_list + tree1_list[idx1:]
    else:
        return result_list + tree2_list[idx2:]


"""
    try:
        tree1_val = next(tree1_gen)
        tree2_val = next(tree2_gen)
        while True:
            if tree1_val < tree2_val:
                result_list.append(tree1_val)
                tree1_val = next(tree1_gen)
            else:
                result_list.append(tree2_val)
                tree2_val = next(tree2_gen)
    except StopIteration:
        if not (root1 and root2):
            left_tree, left_val = (
                (tree1_gen, tree1_val) if root1 else (tree2_gen, next(tree2_gen))
            )
        else:
            left_tree, left_val = (
                (tree1_gen, tree1_val)
                if tree1_val != result_list[-1]
                else (tree2_gen, tree2_val)
            )

    result_list.append(left_val)

    try:
        while True:
            result_list.append(next(left_tree))
    except StopIteration:
        pass
    return result_list
"""
