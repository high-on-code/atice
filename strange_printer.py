def calculate_strate_printer_rec(strn, le, ri, cache):
    if cache[le][ri] > -1:
        return cache[le][ri]
    cache[le][ri] = len(strn)
    mi = -1
    for idx in range(le, ri):
        if strn[idx] != strn[ri] and mi == -1:
            mi = idx
        if mi != -1:
            cache[le][ri] = min(cache[le][ri], 1 + calculate_strate_printer_rec(strn, mi, idx, cache) + calculate_strate_printer_rec(strn, idx + 1, ri, cache))
    if mi == -1:
        cache[le][ri] = 0
    return cache[le][ri]
def calculate_strate_printer(strn):
    slen = len(strn)
    cache = [[-1] * slen for _ in range(slen)]
    return calculate_strate_printer_rec(strn, 0, slen - 1, cache) + 1


s = "aaabbb"
s = "aba"
print(calculate_strate_printer(s))