def assign_cookies(greed, size):
    greed.sort()
    size.sort()
    size_idx = 0
    sat = 0
    for idx, val in enumerate(greed):
        while size_idx < len(size) and val > size[size_idx]:
            size_idx += 1
        if size_idx < len(size):
            sat += 1
            size_idx += 1
        else:
            break
    return  sat

g = [1,2,3]
s = [1,1]
g = [1,2]
s = [1,2,3]
print(assign_cookies(g, s))