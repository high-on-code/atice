def find_num_ways_to_form_target_str(words, target):
    tlen = len(target)
    wct = len(words)
    cache = [[-1 for _ in range(target + 1)] for _ in range(len(words) + 1)]
    ch_count = [[0]*wct for _ in range(26)]