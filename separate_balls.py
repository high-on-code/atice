def find_widx(sidx, balls):
    ballen = len(balls)
    while sidx < ballen:
        if balls[sidx] == '1':
            return sidx
        sidx += 1
    return None

def find_bidx(sidx, balls):
    while sidx >= 0:
        if balls[sidx] == '0':
            return sidx
        sidx -= 1
    return None
def separate_balls(balls):
    widx = None
    for idx, val in enumerate(balls):
        if val == '1':
            widx = idx
            break
    else:
        return 0
    bidx = None
    for idx in range(len(balls) - 1, -1, -1):
        if balls[idx] == '0':
            bidx = idx
            break
    else:
        return 0
    balls = list(balls)
    res = 0
    while widx < bidx:
        # balls[widx], balls[bidx] = balls[bidx], balls[widx]
        res += 1
        widx = find_widx(widx + 1, balls)
        if not widx:
            break
        bidx = find_bidx(bidx - 1, balls)
        if not bidx:
            break
    return res

s = "101"
s = "100"
print(separate_balls(s))