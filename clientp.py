import socket
​HOST = "127.0.0.1" 
PORT = 65432 
​sum_of_bytes = [1, 2, 3, 4, 5, 6, 7, 8]
​with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    s.sendall(bytes(sum_of_bytes))
    data = s.recv(4)
    received_sum = int.from_bytes(data, 'big')
    print(f"Received Sum: {received_sum}")