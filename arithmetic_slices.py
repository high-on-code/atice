# https://leetcode.com/problems/arithmetic-slices-ii-subsequence
def find_artithmetic_subsequence(arr):
    arrlen = len(arr)
    cache = [None] * arrlen
    ans = 0
    for idx in range(arrlen):
        cache[idx] = {}
        for idx2 in range(idx):
            v1 = arr[idx]
            v2 = arr[idx2]
            diff = v1 - v2
            if not (-(2 ** 31) <= diff <= ((2 ** 31) - 1)):
                continue
            as1 = cache[idx].get(diff, 0)
            as2 = cache[idx2].get(diff, 0)
            cache[idx][diff] = as1 + as2 + 1
            ans += as2
    return ans % ((2 ** 31) - 1)


nums = [2, 4, 6, 8, 10]
nums = [7, 7, 7, 7, 7]
print(find_artithmetic_subsequence(nums))
