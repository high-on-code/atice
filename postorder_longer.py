# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def postorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        stack = []
        res = []
        # stack.push(root)
        while stack or root:
            # print('root', root)
            # print('stack', stack)
            # print('res', res)
            if root:
                temp = root.left
                root.left = None
                stack.append(root)
                root = temp
            else:
                temp2 = stack.pop()
                if temp2.right:
                    temp = temp2.right
                    temp2.right = None
                    stack.append(temp2)
                    root = temp
                else:
                    res.append(temp2.val)
        return res
