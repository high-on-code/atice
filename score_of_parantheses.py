# https://leetcode.com/problems/score-of-parentheses/
def score_of_parans(para):
    stack = []
    res = 0
    para_len = len(para)
    idx = 0
    while idx < para_len:
        if para[idx] == ")":
            shell = 1
            depth = 0
            while idx < para_len and para[idx] == ")":
                if depth == 0:
                    shell = 1
                else:
                    shell *= 2
                depth += 1
                idx += 1
            res += shell
        idx += 1
    return res


s = "()"
s = "(())"
s = "()()"
print(score_of_parans(s))
