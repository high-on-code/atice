from collections import defaultdict

def dfs(edge, graph, visited, order, has_cycle):
    visited[edge] = 1
    for nei in graph[edge]:
        if visited[nei] == 0:
            dfs(nei, graph, visited, order, has_cycle)
            if has_cycle[0]:
                return
            elif visited[nei] == 1:
                has_cycle[0] = True
                return
    visited[edge] = 2
    order.append(edge)

def top_sort(row_conditions, k):
    graph = defaultdict(list)
    order = []
    visited = [0] * (k + 1)
    has_cycle = [False]
    for [to, fro] in row_conditions:
        graph[to].append(fro)
    for edge in range(1, k + 1):
        if visited[edge] == 0:
            dfs(edge, graph, visited, order, has_cycle)
    
def build_matrix_with_conditions(k, row_conditions, col_conditions):
    row_order = top_sort(row_conditions, k)