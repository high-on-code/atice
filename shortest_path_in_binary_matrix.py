from collections import deque


def find_shortest_path_in_binary_matrix(grid):
    if grid[0][0] == 1 or grid[-1][-1] == 1:
        return -1
    right = len(grid[0]) - 1
    bottom = len(grid) - 1
    qu = deque()
    qu.append((0, 0))
    dist = 0
    visited = set(qu)

    while qu:
        wall_size = len(qu)
        dist += 1

        for _ in range(wall_size):
            crid, ccid = qu.popleft()
            # (Notice) for case [[0]] to work this is important
            if (crid, ccid) == (bottom, right):
                return dist
            # visited.add((crid, ccid))
            for nrid, ncid in (
                (crid - 1, ccid - 1),
                (crid - 1, ccid),
                (crid - 1, ccid + 1),
                (crid, ccid + 1),
                (crid + 1, ccid + 1),
                (crid + 1, ccid),
                (crid, ccid - 1),
                (crid + 1, ccid - 1),
                (crid, ccid - 1),
            ):
                # print("*" * 80)
                # print("ironman nrid, ncid", nrid, ncid)

                if (
                    0 <= nrid < len(grid)
                    and 0 <= ncid < len(grid[0])
                    and (nrid, ncid) not in visited
                    and grid[nrid][ncid] == 0
                ):
                    # if (nrid, ncid) == (bottom, right):
                    #     return dist + 1
                    qu.append((nrid, ncid))
                    visited.add((nrid, ncid))

    return -1


grid = [[0, 1], [1, 0]]
# grid = [[0, 0, 0], [1, 1, 0], [1, 1, 0]]
# grid = [[0]]
print(find_shortest_path_in_binary_matrix(grid))
