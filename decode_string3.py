def decode_string_helper_helper(strn, start_idx, res):
    num = 0
    while strn[start_idx].isnumeric():
        num = (num * 10) + int(strn[start_idx])
        start_idx += 1
    if strn[start_idx] == "[":
        temp_str = ""
        start_idx += 1
        while strn[start_idx].isalpha():
            temp_str += strn[start_idx]
            start_idx += 1
        if strn[start_idx] == "]":
            res += (num * temp_str)
            # start_idx += 1
            return res, start_idx
        elif strn[start_idx].isnumeric():
            res, start_idx = decode_string_helper_helper(strn, start_idx, res)
            return (num * (temp_str + res)), start_idx + 1
        else:
            return res + decode_string_helper(strn, start_idx, res)


def decode_string_helper(strn, start_idx, res):
    strn_len = len(strn)
    # temp_str = ""
    num = 0
    while strn[start_idx].isnumeric():
        num = (num * 10) + int(strn[start_idx])
        start_idx += 1
    if strn[start_idx] == "[":
        temp_str = ""
        start_idx += 1
        while strn[start_idx].isalpha():
            temp_str += strn[start_idx]
            start_idx += 1
    return res


def decode_string(strn):
    return decode_string_helper(strn, 0, "")


s = "3[a]2[bc]"
# s = "3[a2[c]]"
# s = "2[abc]3[cd]ef"
s = "2[2[y]pq]ef"
print(decode_string(s))
