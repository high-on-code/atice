def find_complements(num_list, list_idx, summ, cache):
    if list_idx == len(num_list):
        res = cache.get(summ, 0)
        return res
    count = 0
    for val in num_list[list_idx]:
        count += find_complements(num_list, list_idx + 1, summ - val, cache)
    return count


def find_four_sum_recur(num_list, list_idx, summ, cache):
    if list_idx == len(num_list) // 2:
        cache[summ] = cache.get(summ, 0) + 1
        return
    for val in num_list[list_idx]:
        find_four_sum_recur(num_list, list_idx + 1, summ + val, cache)


def find_four_sum(*num_list):
    cache = {}
    find_four_sum_recur(num_list, 0, 0, cache)
    return find_complements(num_list, len(num_list) // 2, 0, cache)


nums1 = [1, 2]
nums2 = [-2, -1]
nums3 = [-1, 2]
nums4 = [0, 2]
print(find_four_sum(nums1, nums2, nums3, nums4))
