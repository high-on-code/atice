# https://leetcode.com/problems/removing-stars-from-a-string/
def remove_stars_from_string(st):
    # empty stack
    stack = []
    # iterate over st
    for ch in st:
        if ch == '*' and stack:
            # pop the top element of stack
            stack.pop()
        else:
            stack.append(ch)
            
s = "leet**cod*e"
print(remove_stars_from_string(s))