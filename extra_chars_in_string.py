# https://leetcode.com/problems/extra-characters-in-a-string/?envType=daily-question&envId=2023-09-02
def find_extra_chars_in_string_recur(stn, dicto, idx, cache):
    if idx == len(stn):
        return 0
    if cache[idx] > -1:
        return cache[idx]
    ans = 1 + find_extra_chars_in_string_recur(stn, dicto, idx + 1, cache)
    for end in range(idx + 1, len(stn) + 1):
        if stn[idx:end] in dicto:
            ans = min(ans, find_extra_chars_in_string_recur(stn, dicto, end, cache))
    cache[idx] = ans
    return ans
def find_extra_chars_in_string(stn, dicto):
    cache = [-1] * len(stn)
    return find_extra_chars_in_string_recur(stn, dicto, 0, cache)

s = "leetscode"
dictionary = ["leet","code","leetcode"]
s = "sayhelloworld"
dictionary = ["hello","world"]
print(find_extra_chars_in_string(s, dictionary))