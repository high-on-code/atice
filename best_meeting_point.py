def find_best_meeting_point(graph):
    rows = len(graph)
    cols = len(graph[0])
    xs = []
    ys = []
    for rid, arow in enumerate(graph):
        for cid, val in enumerate(arow):
            if val:
                xs.append(rid)
    for cid in range(cols):
        for rid in range(rows):
            if graph[rid][cid]:
                ys.append(cid)
    xc, yc = xs[int(len(xs) / 2)], ys[int(len(ys) / 2)]
    xd = sum([abs(r - xc) for r in xs])
    yd = sum([abs(r - yc) for r in ys])
    return xd + yd


graph = [[1, 0, 0, 0, 1], [0, 0, 0, 0, 0], [0, 0, 1, 0, 0]]
print(find_best_meeting_point(graph))
