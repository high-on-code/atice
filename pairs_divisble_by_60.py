from collections import defaultdict


def pairs_divisible(nums):
    cache = defaultdict(lambda: 0)
    res = 0
    for val in nums:
        res += cache[60 - (val % 60)]
        cache[val % 60] += 1
    return res


nums = [30, 20, 150, 100, 40]
print(pairs_divisible(nums))
