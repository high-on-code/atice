from socket import NI_NUMERICSERV


def find_min_operations(nums, target):
    le = 0
    new_target = sum(nums) - target
    if new_target == 0:
        return len(nums)
    curr_sum = 0
    max_len = 0
    for ri, val in enumerate(nums):
        curr_sum += val

        while curr_sum > new_target and le < len(nums):
            curr_sum -= nums[le]
            le += 1
        if curr_sum == new_target:
            max_len = max(max_len, ri - le + 1)
    if max_len > 0:
        return len(nums) - max_len
    else:
        return -1


nums = [1, 1, 4, 2, 3]
x = 5
nums = [
    8828,
    9581,
    49,
    9818,
    9974,
    9869,
    9991,
    10000,
    10000,
    10000,
    9999,
    9993,
    9904,
    8819,
    1231,
    6309,
]
x = 134365
print(find_min_operations(nums, x))
