def convert_sorted_array_to_bst_recur(nums, le, ri):
    if le > ri:
        return None
    mid = (le + ri) // 2
    root = TreeNode(nums[mid])
    root.left = convert_sorted_array_to_bst_recur(nums, le, mid - 1)
    root.right = convert_sorted_array_to_bst_recur(nums, mid + 1, ri)
    return root


def convert_sorted_array_to_bst(nums):
    return convert_sorted_array_to_bst_recur(nums, 0, len(nums) - 1)
