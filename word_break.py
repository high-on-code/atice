def word_break(strn, words):
    strn_len = len(strn)
    cache = [False] * strn_len

    for idx in range(strn_len):
        for a_word in words:
            word_len = len(a_word)
            if idx + 1 >= word_len and (idx + 1 == word_len or cache[idx - word_len]):
                if strn[idx - word_len + 1 : idx + 1] == a_word:
                    cache[idx] = True
                    break
    return cache[-1]


strn = "leetcode"
words = ["leet", "code"]
print(word_break(strn, words))
