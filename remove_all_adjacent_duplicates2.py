def remove_all_adjacent_duplicates(strn, count):
    stack = []
    for idx, val in enumerate(strn):
        if idx > 0 and stack and val == stack[- 1][0]:

            stack[-1][1] += 1
            if stack[-1][1] == count:
                stack.pop()

        else:
            stack.append([val, 1])
    res = []
    for val, reps in stack:
        for _ in range(reps):
            res.append(val)
    return "".join(res)


s = "deeedbbcccbdaa"
k = 3
print(remove_all_adjacent_duplicates(s, k))
