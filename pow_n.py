def pow_n(x, n):
    if n == 0:
        return 1
    elif n < 0:
        return 1 / pow_n(x, -n)
    elif n % 2:
        return x * pow_n(x, n-1)
    return pow_n(x*x, n/2)

x = 2.00000
n = 10
x = 2.10000
n = 3
print(pow_n(x, n))