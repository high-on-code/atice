def string_to_integer(strn):
    sign, result = 1, 0
    idx = 0
    strn_len = len(strn)
    while idx < strn_len and strn[idx] == " ":
        idx += 1

    if idx < strn_len and strn[idx] in "-+":
        sign = -1 if strn[idx] == "-" else 1
        idx += 1

    INT_MAX = (2 ** 31) - 1
    INT_MIN = -(2 ** 31)
    INT_NOT_MAX = INT_MAX // 10
    while idx < strn_len and ord("0") <= ord(strn[idx]) <= ord("9"):
        dig = int(strn[idx])
        if result < INT_NOT_MAX:
            result = (result * 10) + int(strn[idx])
        elif result > INT_NOT_MAX:
            return INT_MAX if sign == 1 else INT_MIN
        else:
            if dig <= 7:
                result = result * 10 + dig
            else:
                return INT_MAX if sign == 1 else INT_MIN
        idx += 1
    return sign * result


strn = "-91283472332"
strn = "21474836460"
strn = "-2147483647"
print(string_to_integer(strn))
