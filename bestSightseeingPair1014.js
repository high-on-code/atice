var maxScoreSightseeingPair = function (values) {
    let res = 0;
    const maxLeftVal = new Array(values.length);
    maxLeftVal[0] = values[0];
    for(let i = 1; i < values.length; i++) {
        const pairVal = maxLeftVal[i - 1] + values[i] - i;
        res = Math.max(res, pairVal);
        maxLeftVal[i] = Math.max(maxLeftVal[i - 1], values[i] + i)
    }
    return res;
};
let values = [8,1,5,2,6];
values = [1,2]
console.log(maxScoreSightseeingPair(values))