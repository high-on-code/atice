def partition(nums, st, ed):
    pivot = nums[ed]
    pivot_idx = st
    for idx in range(st, ed):
        if nums[idx] < pivot:
            nums[pivot_idx], nums[idx] = nums[idx], nums[pivot_idx]
            pivot_idx += 1
    nums[ed], nums[pivot_idx] = nums[pivot_idx], nums[ed]
    return pivot_idx


def select(nums, st, ed, arr_mid):
    if st == ed:
        return nums[st]
    pivot_index = partition(nums, st, ed)
    if pivot_index == arr_mid:
        print("*" * 80)
        print("ironman nums", nums)
        return nums[pivot_index]
    elif pivot_index < arr_mid:
        return select(nums, pivot_index + 1, ed, arr_mid)
    else:
        return select(nums, st, pivot_index - 1, arr_mid)


def find_min_moves_to_equal_arr(nums):
    median = select(nums, 0, len(nums) - 1, len(nums) // 2)


nums = [1, 10, 2, 9]
print(find_min_moves_to_equal_arr(nums))
